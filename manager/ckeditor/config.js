/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
config.toolbar = [
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'basicstyles', 'cleanup', 'clipboard', 'mode', 'document', 'doctools', 'find', 'selection', 'spellchecker', 'Link', 'Unlink', 'Image', 'Table' ], items: [ 'Bold', 'Italic', 'Underline', '-', 'PasteText', '-', 'Print', 'Scayt', '-', 'Link', 'Unlink', '-', 'Image', 'Table', '-', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'FontSize', 'Youtube' ] },
];

	
	   config.filebrowserBrowseUrl = 'ckeditor/plugins/kcfinder/browse.php?type=files';
	   config.filebrowserImageBrowseUrl = 'ckeditor/plugins/kcfinder/browse.php?type=images';
	   config.filebrowserFlashBrowseUrl = 'ckeditor/plugins/kcfinder/browse.php?type=flash';
	   config.filebrowserUploadUrl = 'ckeditor/plugins/kcfinder/upload.php?type=files';
	   config.filebrowserImageUploadUrl = 'ckeditor/plugins/kcfinder/upload.php?type=images';
	   config.filebrowserFlashUploadUrl = 'ckeditor/plugins/kcfinder/upload.php?type=flash';
	   config.entities = false;
	   config.basicEntities = false;
	   config.htmlEncodeOutput = false;
	   config.entities_latin = false;
	   config.entities_greek = false;
	   config.extraPlugins = 'youtube';
	   config.youtube_responsive = true;
	   config.youtube_related = false;
	

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	
	// config.enterMode = CKEDITOR.ENTER_BR; // inserts <br />


};
