<?php

function sucesso(){
	$cat = $_GET['link'];
	$aux = '';
	if(!empty($_GET['id_evento'])){
		$aux = $_GET['id_evento'];
	}
	echo "<script>
			window.location.href = '?link=".$cat.$aux."&sucesso';
		  </script>";
}

function error(){
	echo "<script>
			$(function(){ 
				$('.alert.alert-danger').slideDown('slow').delay(5000).slideUp('slow');
			});
		  </script>";
}

function operacao(){
	echo "<script>window.location.href = document.referrer;</script>";
}

function permissao($modulo){
	if($_SESSION['id_usuario'] != 1){
		$select = Connection::getInstance()->prepare("SELECT id FROM usuarios WHERE id = '".$_SESSION['id_usuario']."' AND FIND_IN_SET(:modulo, permissoes)");
		$select->bindParam(":modulo", $modulo);
		$select->execute();
		$modulos = $select->rowCount();
		if($modulos == 0){
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
}
	
function contador(){
	$ip = $_SERVER["REMOTE_ADDR"];

	$select = Connection::getInstance()->prepare("SELECT * FROM acessos WHERE date(data) = CURDATE() AND ip = :ip");
	$select->bindValue(":ip", $ip);
	$select->execute();
	$conta = $select->rowCount();
	
	if($conta == 0){
    	$add = Connection::getInstance()->prepare("INSERT INTO acessos (data, ip) VALUES (NOW(), :ip)");
    	$add->bindValue(":ip", $ip);
    	$add->execute();
	}

	$tempmins = 5; 
	$ip = $_SERVER["REMOTE_ADDR"];
	$select = Connection::getInstance()->prepare("SELECT * FROM acessos_online WHERE ip = :ip");
	$select->bindParam(":ip", $ip);
	$select->execute();
	$conta = $select->rowCount();
	if($conta > 0){
	    $update =  Connection::getInstance()->prepare('UPDATE acessos_online SET time = "'.time().'" WHERE ip = :ip');
	    $update->bindParam(":ip", $ip);
		$update->execute();
	} 
	else{
	    $update =  Connection::getInstance()->prepare('INSERT INTO acessos_online (ip,time) VALUES (:ip,"'.time().'")');
	    $update->bindParam(":ip", $ip);
		$update->execute();
	}
	$del =  Connection::getInstance()->prepare('DELETE FROM acessos_online WHERE time<'.(time()-($tempmins*60)));
	$del->execute();
}

function dataextenso(){
	$data = date('D');
    $mes = date('M');
    $dia = date('d');
    $ano = date('Y');
    
    $semana = array(
        'Sun' => 'Domingo', 
        'Mon' => 'Segunda-Feira',
        'Tue' => 'Terça-Feira',
        'Wed' => 'Quarta-Feira',
        'Thu' => 'Quinta-Feira',
        'Fri' => 'Sexta-Feira',
        'Sat' => 'Sábado'
    );
    
    $mes_extenso = array(
        'Jan' => 'Janeiro',
        'Feb' => 'Fevereiro',
        'Mar' => 'Marco',
        'Apr' => 'Abril',
        'May' => 'Maio',
        'Jun' => 'Junho',
        'Jul' => 'Julho',
        'Aug' => 'Agosto',
        'Nov' => 'Novembro',
        'Sep' => 'Setembro',
        'Oct' => 'Outubro',
        'Dec' => 'Dezembro'
    );
	$data = $semana["$data"] . ", {$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
	return $data;
}

function mes_extenso($mes2){
	switch ($mes2){
		case '01':
			$mes = "janeiro";
		break;

		case '02':
			$mes = "fevereiro";
		break;

		case '03':
			$mes = "março";
		break;

		case '04':
			$mes = "abril";
		break;

		case '05':
			$mes = "maio";
		break;

		case '06':
			$mes = "junho";
		break;

		case '07':
			$mes = "julho";
		break;

		case '08':
			$mes = "agosto";
		break;

		case '09':
			$mes = "setembro";
		break;

		case '10':
			$mes = "outubro";
		break;

		case '11':
			$mes = "novembro";
		break;

		case '12':
			$mes = "dezembro";
		break;
	}
	return $mes;
}

function limpaUrl($str){

    $str = retiraAcentos($str);
    $str = strtolower(utf8_decode($str)); $i=1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
    while($i>0) $str = str_replace('--','-',$str,$i);
    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
    return $str;
}

function retiraAcentos($string){
	$array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç" , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", " ", "-" );
	$array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c" , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "_", "_" );
	return str_replace($array1, $array2,$string);
}
function retiraAcentos2($string){
	$string = ereg_replace("[^a-zA-Z0-9_.]", "", strtr($string, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));

	return $string;
}

function formata_valor($pValor){
	$pValor = str_replace(".", "", $pValor);
	$pValor = str_replace(",", ".", $pValor);
	return $pValor;
}

function formato_real($valor) {
	$valor = number_format($valor, 2, ',', '.');
	return $valor;
}


function formatDate($data, $formato = 'd/m/y'){
	return date($formato, strtotime($data));
}

function formatHour($data, $formato = 'H:i'){
	$hour = date($formato, strtotime($data));
	$hour = str_replace(':', 'h', $hour);
	return $hour;
}

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

function limita_caracteres($texto, $limite, $quebra = false){
   $tamanho = strlen($texto);
   if($tamanho <= $limite){ //Verifica se o tamanho do texto é menor ou igual ao limite
      $novo_texto = $texto;
   }else{ // Se o tamanho do texto for maior que o limite
      if($quebra == true){ // Verifica a opção de quebrar o texto
         $novo_texto = trim(substr($texto, 0, $limite))."...";
      }else{ // Se não, corta $texto na última palavra antes do limite
         $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
         $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada
      }
   }
   return $novo_texto; // Retorna o valor formatado
}

function limitar($string, $tamanho, $encode = 'UTF-8') {
    if( strlen($string) > $tamanho )
        $string = mb_substr($string, 0, $tamanho - 3, $encode) . '...';
    else
        $string = mb_substr($string, 0, $tamanho, $encode);

    return $string;
}

function Redimensionar($imagem, $largura, $pasta, $file){
	$name = date("G").'_'.date("i").'_'.date("s").'_'.$file;

	if ($imagem['type']=="image/jpeg"){
		$img = imagecreatefromjpeg($imagem['tmp_name']);
	}else if ($imagem['type']=="image/gif"){
		$img = imagecreatefromgif($imagem['tmp_name']);
	}else if ($imagem['type']=="image/png"){
		$img = imagecreatefrompng($imagem['tmp_name']);

	}
	$x   = imagesx($img);
	$y   = imagesy($img);
	$autura = ($largura * $y)/$x;

	$nova = imagecreatetruecolor($largura, $autura);
	imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura, $autura, $x, $y);

	if ($imagem['type']=="image/jpeg"){
		$local="$pasta/$name";
		imagejpeg($nova, $local);
	}else if ($imagem['type']=="image/gif"){
		$local="$pasta/$name";
		imagegif($nova, $local);
	}else if ($imagem['type']=="image/png"){
		$local="$pasta/$name";
		imagepng($nova, $local);
	}

	imagedestroy($img);
	imagedestroy($nova);

	return $local;
}
function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

function setValueAjax($id,$table){
	require_once('Connection.class.php');

	// Verifica se esta setado	
	$select = Connection::getInstance()->prepare("SELECT destaque FROM ".$table." WHERE id = :id");
    $select->bindValue(":id", $id);
    $select->execute();
    $result = $select->fetch();

    $value = 0;
    // Seta o numero
    if($result['destaque'] == 0){
    	$value = 1;
    }

	$acao = Connection::getInstance()->prepare("UPDATE ".$table." SET destaque = :value WHERE id = :id");
    $acao->bindValue(":value", $value);
    $acao->bindValue(":id", $id);
    
    if($acao->execute()){
        echo json_encode($value);
    }
}

if(!empty($_GET['ajax'])){
    setValueAjax($_GET['id'],$_GET['table']);
}

if (!empty($_FILES) && !empty($_FILES['file']) && !empty($_FILES['file']['name'])) {
	upload($_FILES['file']);
}

function upload($file, $table = 'summernote', $column = 'image', $id = null){
	if (!$file['error']) {
		require_once("Upload.class.php");
        $handle = new upload($file);

        if($handle->uploaded){
            // $handle->image_resize         = false;
            // $handle->image_x              = 1920;
            // $handle->image_y              = 840;
            $handle->image_ratio_y        = false;
            $dir = "uploads/".$table."/";
            $dir_back = '../../';
            if(!empty($id)){
            	$dir .= $id."/";
            	$dir_back = '../';
            }
            if(!is_dir($dir_back.$dir)) {
                mkdir($dir_back.$dir, 0777, true);
            }
            $handle->process($dir_back.$dir);
          
            if($handle->processed) {
                $arquivo = $dir.$handle->file_dst_name;
                $handle->clean();

                require_once('Resize.class.php');
                $image = new SimpleImage();
                $image->load($dir_back.$arquivo);
                // $image->resizeToWidth(1280);
                $image->save($dir_back.$arquivo);

                if($table != 'summernote' && !empty($id)){
	                $acao = Connection::getInstance()->prepare("UPDATE ".$table." SET ".$column." = :arquivo WHERE id = :id");
	                            
	                $acao->bindValue(":id", $id);
	                $acao->bindValue(":arquivo", $arquivo);
	                if($acao->execute()){
	                	return true;
	                }else{
						echo "Ooops! Não foi possivel salvar o caminho no Banco de dados.";
	                }
                }else{
                	echo $arquivo;
                }

            }else{
                echo 'Ooops! Não foi possivel processar o upload.';
            }
        }else{
        	echo 'Ooops! Não foi possivel fazer o upload.';
        }
	}else{
      	echo 'Ooops! Seu upload desencadeou o seguinte erro: '.$_FILES['file']['error'];
    }
}






/**
 * This file is part of the array_column library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Ben Ramsey (http://benramsey.com)
 * @license http://opensource.org/licenses/MIT MIT
 */
if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();
        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }
        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }
        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;
        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }
        $resultArray = array();
        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;
            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }
            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }
            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }
        }
        return $resultArray;
    }
}




function MarcaDagua($imagem){

//return $imagem;

    $lowerFileName = strtolower($imagem); 
    if(substr_count($lowerFileName, '.jpg')>0 || substr_count($lowerFileName, '.jpeg')>0){
        $a = imagecreatefromjpeg($imagem);    
    }else if(substr_count($lowerFileName, '.png')>0){
        $a = imagecreatefrompng($imagem); 
    }else if(substr_count($lowerFileName, '.gif')>0){
        $a = imagecreatefromgif($imagem); 
    }
    
   
   // $a = imagecreatefromjpeg($imagem);
    $b = imagecreatefrompng('../img/logo_marcar.png');
  
    
    // Obtem a largura_nova da imagem
    $larguraImagem = imagesx( $a );
    // Obtém a altura da imagem
    $alturaImagem = imagesy( $a );

    // Obtem a largura_nova da imagem
    $larguraLogo = imagesx( $b );
    // Obtém a altura da imagem
    $alturaLogo = imagesy( $b );


    

    define("WIDTH", $larguraImagem);
    define("HEIGHT", $alturaImagem);



    $dest_image = imagecreatetruecolor(WIDTH, HEIGHT);

    //make sure the transparency information is saved
    imagesavealpha($dest_image, true);

    //create a fully transparent background (127 means fully transparent)
    $trans_background = imagecolorallocatealpha($dest_image, 0, 0, 0, 127);

    //fill the image with a transparent background
    imagefill($dest_image, 0, 0, $trans_background);

    // Centralizar logo
    //$espacamentohorizontal = ($larguraImagem/2) - ($larguraLogo/2); 
    //$espacamentoVertical = ($alturaImagem/2) - ($alturaLogo/2); 

    // Canto direito inferior
    $espacamentohorizontal = ($larguraImagem -$larguraLogo); 
    $espacamentoVertical = ($alturaImagem - $alturaLogo); 

    

    
    //copy each png file on top of the destination (result) png
    imagecopy($dest_image, $a, 0, 0, 0, 0, WIDTH, HEIGHT);
    imagecopy($dest_image, $b, $espacamentohorizontal, $espacamentoVertical, 0, 0, $larguraLogo, $alturaLogo);
   // imagecopy($dest_image, $c, 0, 0, 0, 0, WIDTH, HEIGHT);

    //send the appropriate headers and output the image in the browser
    //header('Content-Type: image/png');
    imagepng($dest_image, $imagem);
    //imagepng($dest_image, "../img/nova.png");

    //return imagepng($dest_image);

   // imagepng($dest_image, "nova.jpg" );  //salvar imagem

    //destroy all the image resources to free up memory
    imagedestroy($a);
    imagedestroy($b);
  //  imagedestroy($c);
    imagedestroy($dest_image);

    return true;

}






function verificaServicoAtivo($TABLE, $IDPLANOSERVICO){

    //verifica se tem itens ativos ainda para remover o serviço de ativo e ocultar a coluna no site
    $servicos_ativos = Connection::getInstance()->query("SELECT * FROM {$TABLE} WHERE ativo = 1")->fetchAll(PDO::FETCH_ASSOC);
    $conta_servicos = count($servicos_ativos);

    if($conta_servicos < 1){
        $novaacao = Connection::getInstance()->prepare("UPDATE planos_servicos SET ativo = 0 WHERE id_plano_servico = $IDPLANOSERVICO");
        $novaacao->execute();
    }else{
        $novaacao = Connection::getInstance()->prepare("UPDATE planos_servicos SET ativo = 1 WHERE id_plano_servico = $IDPLANOSERVICO");
        $novaacao->execute();
    }
	return true;
}


function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}


?>
