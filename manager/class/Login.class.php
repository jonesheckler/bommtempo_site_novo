<?php
require_once(dirname(__FILE__).'/Connection.class.php');
 
Class Login{
 
   /*function __construct(){
      $objConnection = Connection::getInstance();
   }*/
 
   function verificarLogado(){
      if(!isset($_SESSION["logado"])){
         header("Location: login.php");
         exit();
      }
   }
 
   function Logar($usuario,$senha){
      $sql = "SELECT * FROM usuarios WHERE usuario = :usuario AND ativo = 1";
      $resultados = Connection::getInstance()->prepare($sql);
      $resultados->bindValue(":usuario", $usuario);
      $resultados->execute();
      $num_rows = $resultados->rowCount();
      $resultados->execute();

      if($num_rows == 1){
         $resultados->execute();
         $d_usuario = $resultados->fetch();
         $senha = md5($senha);
         if($d_usuario["senha"] == $senha){
            $_SESSION["id_usuario"] = $d_usuario["id"];
            $_SESSION["nome_usuario"] = $d_usuario["nome"];
            $_SESSION["logado"] = "sim";
            $update = Connection::getInstance()->prepare("UPDATE usuarios SET ultimo_acesso = CURTIME() WHERE id = '".$_SESSION['id_usuario']."'");
            $update->execute();
            header("Location: index.php");
         }else{
            $Erro = "Usuário ou senha inválidos";
            return $Erro;
         }
      }else{
         $Erro = "Usuário ou senha inválidos";
         return $Erro;
      };
   }

   function Logar2($usuario,$senha,$painel){
      if($painel != 'distribuidores' && $painel != 'saloes'){
          $Erro = "Usuário ou senha inválidos";
         return $Erro;
         exit();
      }
      $sql = "SELECT * FROM ".$painel." WHERE usuario = :usuario AND ativo = 1";
      $resultados = Connection::getInstance()->prepare($sql);
      $resultados->bindValue(":usuario", $usuario);
      $resultados->execute();
      $num_rows = $resultados->rowCount();
      $resultados->execute();

      if($num_rows == 1){
         $resultados->execute();
         $d_usuario = $resultados->fetch();
         $senha = md5($senha);
         if($d_usuario["senha"] == $senha){
            $_SESSION["id_usuario"] = $d_usuario["id"];
            $_SESSION["nome_usuario"] = $d_usuario["nome"];
            $_SESSION["painel"] = $painel;
            $_SESSION["logado"] = "sim";
            header("Location: index.php");
         }else{
            $Erro = "Usuário ou senha inválidos";
            return $Erro;
         }
      }else{
         $Erro = "Usuário ou senha inválidos";
         return $Erro;
      };
   }
 
   function getIdUsuario(){
      return $_SESSION["id_usuario"];
   }
 
   function deslogar(){
      if($_SESSION['painel'] != ''){
         $painel = $_SESSION['painel'];

         session_destroy();
         header("Location: login.php?painel=".$painel);
      }else{
         session_destroy();
         header("Location: login.php");
      }
   }
 
}
?>