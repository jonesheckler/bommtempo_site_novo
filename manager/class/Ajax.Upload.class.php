<?php
    require_once('Login.class.php');
    require_once('functions.php');
    $objLogin = new Login();
    $objLogin->verificarLogado();

    require_once("Upload.class.php");

    $files = array();
    foreach ($_FILES['arquivo'] as $k => $l) {
      foreach ($l as $i => $v) {
      if (!array_key_exists($i, $files))
        $files[$i] = array();
        $files[$i][$k] = $v;
      }
    }

    foreach ($files as $file) {
      $handle = new upload($file);
      if($handle->uploaded){
        /*$handle->image_resize         = true;
        $handle->image_x              = 850;
        $handle->image_y              = 400;
        $handle->image_ratio_y        = true;*/
        $handle->process('../../uploads/'.$_POST['folder'].'/');

        /*$handle->image_resize         = true;
        $handle->image_x              = 400;
        $handle->image_y              = 400;
        $handle->image_ratio_y        = true;*/
        $handle->process('../../uploads/'.$_POST['folder'].'/thumb/');
        if($handle->processed) {
          $arquivo = 'uploads/'.$_POST['folder'].'/'.$handle->file_dst_name;
          $arquivo_thumb = 'uploads/'.$_POST['folder'].'/thumb/'.$handle->file_dst_name;
          $handle->clean();

          require_once('Resize.class.php');

            $image = new SimpleImage();
            $image->load('../../'.$arquivo);
            $image->resizeToWidth(1024);
            $image->save('../../'.$arquivo);

            $image = new SimpleImage();
            $image->load('../../'.$arquivo_thumb);
            $image->resizeToWidth(400);
            $image->save('../../'.$arquivo_thumb);

            $acao = Connection::getInstance()->prepare("INSERT INTO ".$_POST['tabela']." (id_pai, foto, thumb) VALUES ('".$_POST['id']."', :arquivo, :arquivo_thumb)");
            $acao->bindValue(":arquivo", $arquivo);
            $acao->bindValue(":arquivo_thumb", $arquivo_thumb);
            $acao->execute();
         
        }
      }
       echo json_encode(array("success" => true, "message" => "Success!"));
    }
    
?>