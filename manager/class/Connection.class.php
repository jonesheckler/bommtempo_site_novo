<?php
session_start();

$base_master = "dev";


if($base_master == "prod"){
      $base_url = 'https://www.bommtempo.com.br';
      date_default_timezone_set('America/Sao_Paulo');

      class Connection{
         public static $instance;

         /**
          * @return PDO
         */
         public static function getInstance() { 
            if (!isset(self::$instance)) { 
               self::$instance = new PDO('mysql:host=mysql.bommtempo.com.br;dbname=bommtempo08', 'bommtempo08', 'B0mMT3mp0', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); 
               self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
               self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
               self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING); 
            } 
               return self::$instance; 
         }
      }
}
else if($base_master == "dev"){
   $base_url = 'http://bommtempo.localhost/';
      date_default_timezone_set('America/Sao_Paulo');

      class Connection{
         public static $instance;

         /**
          * @return PDO
         */
         public static function getInstance() { 
            if (!isset(self::$instance)) { 
               self::$instance = new PDO('mysql:host=localhost;dbname=bommtempo', 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); 
               self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
               self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
               self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING); 
            } 
               return self::$instance; 
         }
      }
}else if($base_master == "cloud"){
   $base_url = 'http://phpstack-314900-964650.cloudwaysapps.com/';
      date_default_timezone_set('America/Sao_Paulo');

      class Connection{
         public static $instance;

         /**
          * @return PDO
         */
         public static function getInstance() { 
            if (!isset(self::$instance)) { 
               self::$instance = new PDO('mysql:host=localhost;dbname=bbkceabjha', 'bbkceabjha', '8wURFB39gd', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); 
               self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
               self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
               self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING); 
            } 
               return self::$instance; 
         }
      }
}
?>
