<?php

    if($_POST){
        extract($_POST);

        $ativo = ($ativo == 1) ? 1 : 0;
        if(!empty($_GET['acao'])){
            if($_GET['acao'] == 'editar'){
                $id = $_GET['id'];
                $acao = Connection::getInstance()->prepare("UPDATE home_banners SET nome_1 = :nome_1, nome_2 = :nome_2, link = :link, ativo = :ativo, ordem = :ordem, alinhamento_1 = :alinhamento_1, alinhamento_2 = :alinhamento_2 WHERE id = :id");
                $acao->bindValue(":id", $id);
            }else if($_GET['acao'] == 'adicionar'){
                $acao = Connection::getInstance()->prepare("INSERT INTO home_banners (nome_1, nome_2, link,  ativo, ordem,alinhamento_1, alinhamento_2) VALUES (:nome_1, :nome_2, :link, :ativo, :ordem, :alinhamento_1, :alinhamento_2)");
            }
        }

        $acao->bindValue(":nome_1", $nome_1);
        $acao->bindValue(":nome_2", $nome_2);
        $acao->bindValue(":ativo", $ativo);
        $acao->bindValue(":link", $link);
        $acao->bindValue(":ordem", $ordem);
        $acao->bindValue(":alinhamento_1", $alinhamento_1);
        $acao->bindValue(":alinhamento_2", $alinhamento_2);
        

        $acao->execute();

        if($_GET['acao'] == 'adicionar'){
            $id = Connection::getInstance()->lastInsertId();
        }
        if(isset($_FILES["banner"]["tmp_name"])){
            require_once("class/Upload.class.php");
            $handle = new upload($_FILES["banner"], "pt_BR");
            if($handle->uploaded){
                $handle->allowed = array("image/*");
                $handle->file_new_name_body   = bin2hex(openssl_random_pseudo_bytes(10));
                $handle->image_resize         = true;
                $handle->image_x              = 1920;
                $handle->image_ratio_y        = true;
                $handle->image_no_enlarging   = true;

                $folder = "uploads/home_banners/".$id."/";

                $handle->process("../".$folder);
                if($handle->processed){
                    $acao = Connection::getInstance()->prepare("UPDATE home_banners SET banner = :banner WHERE id = :id");
                        
                    $acao->bindValue(":id", $id);
                    $acao->bindValue(":banner", $folder.$handle->file_dst_name);
                    $acao->execute();
                }else{
                    echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
                }
            }
        }

        if(isset($_FILES["banner_mobile"]["tmp_name"])){
            require_once("class/Upload.class.php");
            $handle = new upload($_FILES["banner_mobile"], "pt_BR");
            if($handle->uploaded){
                $handle->allowed = array("image/*");
                $handle->file_new_name_body   = bin2hex(openssl_random_pseudo_bytes(10));
                $handle->image_resize         = true;
                $handle->image_x              = 1920;
                $handle->image_ratio_y        = true;
                $handle->image_no_enlarging   = true;

                $folder = "uploads/home_banners/".$id."/";

                $handle->process("../".$folder);
                if($handle->processed){
                    $acao = Connection::getInstance()->prepare("UPDATE home_banners SET banner_mobile = :banner_mobile WHERE id = :id");
                        
                    $acao->bindValue(":id", $id);
                    $acao->bindValue(":banner_mobile", $folder.$handle->file_dst_name);
                    $acao->execute();
                }else{
                    echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
                }
            }
        }
        sucesso();            

    }  
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Home</h1>
    </div>
</div>
<div class="row">
<?php if(!empty($_GET['acao']) && ($_GET['acao'] == 'adicionar' || $_GET['acao'] == 'editar')){ 

    if(isset($_GET['excluir_imagem'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT banner FROM home_banners WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $imagem = $select->fetch();
        @unlink('../'.$imagem['banner']);

        $update = Connection::getInstance()->prepare("UPDATE home_banners SET banner = '' WHERE id = :id");
        $update->bindValue(":id", $id);
        $update->execute();
        operacao();
    }

    if(isset($_GET['excluir_banner_mobile'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT banner_mobile FROM home_banners WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $imagem = $select->fetch();
        @unlink('../'.$imagem['banner_mobile']);

        $update = Connection::getInstance()->prepare("UPDATE home_banners SET banner_mobile = '' WHERE id = :id");
        $update->bindValue(":id", $id);
        $update->execute();
        operacao();
    }

    if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT * FROM home_banners WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $select = $select->fetch();
        
        $ativo = ($select["ativo"] == 1) ? 'checked="checked"' : '';
    }else{
        $ativo ='checked="checked"';
    }

?>
    <div class="col-lg-12">
        <div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>  
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastro de Banners
            </div>   
            <div class="panel-body">
                <form method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Imagem *</label>
                        <?php if(!empty($select["banner"])){?>
                            <img src="../<?= $select["banner"];?>" class="img-responsive">
                            <a href="?link=<?= $_GET['link'] ?>&id=<?= $_GET['id'] ?>&acao=<?= $_GET['acao'] ?>&excluir_imagem"><button type="button" class="btn btn-sm btn-danger">Excluir Imagem</button></a>
                        <?php }else{ ?>
                            <input type="file" class="form-control" name="banner" autofocus required>
                        <?php } ?>
                        <p class="help-block">* Tamanho sugerido 1920x1447 pixels.</p>
                    </div>
                    <div class="form-group">
                        <label>Imagem Mobile</label>
                        <?php if(!empty($select["banner_mobile"])){?>
                            <img src="../<?= $select["banner_mobile"];?>" class="img-responsive">
                            <a href="?link=<?= $_GET['link'] ?>&id=<?= $_GET['id'] ?>&acao=<?= $_GET['acao'] ?>&excluir_banner_mobile"><button type="button" class="btn btn-sm btn-danger">Excluir Imagem Mobile</button></a>
                        <?php }else{ ?>
                            <input type="file" class="form-control" name="banner_mobile" autofocus required>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Título 1</label>
                        <input class="form-control" name="nome_1" value="<?= !empty($select["nome_1"]) ? $select['nome_1'] : '' ?>">
                    </div>
                    <div class="form-group">
                        <label>Alinhamento título 1</label>
                        <select class="form-control" name="alinhamento_1">
                            <option value="left" <?= !empty($select["alinhamento_1"]) && $select["alinhamento_1"] == "left" ? 'selected' : '' ?> >Esquerda</option>
                            <option value="center" <?= !empty($select["alinhamento_1"]) && $select["alinhamento_1"] == "center" ? 'selected' : '' ?> >Centro</option>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Título 2 </label>
                        <input class="form-control" name="nome_2" value="<?= !empty($select["nome_2"]) ? $select['nome_2'] : '' ?>" >
                        
                    </div>
                    <div class="form-group">
                        <label>Alinhamento título 2</label>
                        <select class="form-control" name="alinhamento_2">
                            <option value="left" <?= !empty($select["alinhamento_2"]) && $select["alinhamento_2"] == "left" ? 'selected' : '' ?> >Esquerda</option>
                            <option value="center" <?= !empty($select["alinhamento_2"]) && $select["alinhamento_2"] == "center" ? 'selected' : '' ?> >Centro</option>
                            
                        </select>
                    </div>
                    <!--
                    <div class="form-group">
                        <label>Link </label>
                        <input type="url" class="form-control" name="link" value="<?= !empty($select["link"]) ? $select['link'] : '' ?>" >
                    </div>-->
                    <div class="form-group">
                        <label>Ordenação</label>
                        <input class="form-control" type="number" min="0" name="ordem" value="<?= !empty($select["ordem"]) ? $select['ordem'] : '0' ?>">
                        
                    </div>
                    
                    <!-- <div class="form-group">
                        <label>Link</label>
                        <input class="form-control" name="slug" value="<?= !empty($select["slug"]) ? $select['slug'] : '' ?>">
                    </div>
                    <hr>
                    <div class="form-group ">
                        <label>Status</label>
                        <div class="checkbox"><label class="nopadding"><input type="checkbox" name="ativo" class="flat" value="1" <?= $ativo?>> Ativo</label></div>
                    </div> -->

                    <div class="form-group ">
                        <label>Ativo</label><br>
                        <label class="switch ">
                        <input  type="checkbox" value="1" <?=$select["ativo"] ? "checked" : ""?> name="ativo" class="form-control  ">
                        <span class="slider"></span>
                        </label>
                        
                    </div>

                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <a href="?link=<?= $_GET['link'] ?>" class="btn btn-default">Voltar</a>
                    <input type="hidden" name="id" id="id" value="<?= $_GET['id']?>">
                </form>
            </div>                    
        </div>
    </div>

<?php }else{ 

    if(!empty($_GET['acao']) && ($_GET['acao'] == 'excluir')){
        $id = $_GET['id'];

        $del = Connection::getInstance()->prepare("DELETE FROM home_banners WHERE id = :id");
        $del->bindValue(":id", $id);
        $del->execute();

        sucesso();
    }

?>
<div class="col-lg-12">
    <?php if(isset($_GET['sucesso'])):?>
        <div class="alert alert-success">Operação realizada com sucesso.</div>
    <?php endif; ?>

    <?php
    $select = Connection::getInstance()->query("SELECT * FROM home_banners ORDER BY ordem asc")->fetchAll();
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                   <h4>Lista de Banners</h4>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-6">
                   
                    <a href="?link=<?= $_GET['link']?>&acao=adicionar"><button type="button" class="btn btn-md btn-success pull-right">Adicionar</button></a>
                   
                </div>
                
            </div>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                    <thead>
                        <tr>
                            <th class="hidden">id</th>
                            <th width="80">Ordem</th>
                            <th>Título</th>
                            <th width="200">Imagem</th>
                            
                           <!--  <th>URL</th> -->
                             <th width="80">Ativo</th> 
                            <th width="150">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // select esta acima

                        foreach ($select as $key => $s) {
                            $ativo = ($s["ativo"] == 1) ? 'Sim' : 'Não';
                    ?>
                        <tr class="gradeA">
                            <td class="text-center"><?= $s["ordem"]?></td>
                            <td class="text-center"><?= $s['nome_1'] ?> <b><?= $s['nome_2'] ?></b></td>
                            <td class="text-center">
                                <a href="<?= $s["banner"] ? '../'.$s["banner"] : 'dist/css/images/sem_imagem.png' ?>" class="fancybox">
                                    <img class="img-responsive" src="<?= $s["banner"] ? '../'.$s["banner"] : 'dist/css/images/sem_imagem.png' ?>" style="margin: auto;" >
                                </a>
                            </td>
                            
                            <td class="text-center"><?= $ativo?></td> 
                            <td class="text-center">

                                 <a href="?link=<?=$_GET["link"]?>&acao=editar&id=<?= $s['id']?>" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="javascript:confirma('?link=<?= $_GET["link"] ?>&acao=excluir&id=<?= $s['id']?>')"  class="btn  btn-danger "><i class="fa fa-trash fa-fw"></i></a>
    
                                <!--
                                <a href="?link=< ?= $_GET['link']?>&acao=editar&id=< ?= $s['id']?>">
                                    <button type="button" class="btn btn-sm btn-primary">Editar</button>
                                </a> 
                                <a href="javascript:confirma('?link=< ?= $_GET['link']?>&acao=excluir&id=< ?= $s['id']?>')">
                                    <button type="button" class="btn btn-sm btn-danger">Excluir</button>
                                </a> -->
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>
</div>