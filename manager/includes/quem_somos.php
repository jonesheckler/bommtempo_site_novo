<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">SOBRE</h1>
    </div>
</div>
<?php


if(strtoupper($_SERVER["REQUEST_METHOD"]) === "POST"){
    if(isset($_POST['dados'])){
        // salva o formulario com conteudo da página quem somos
        // salva o formulario com conteudo da página quem somos
        // salva o formulario com conteudo da página quem somos
        extract($_POST);

        $acao = Connection::getInstance()->prepare("UPDATE quem_somos SET titulo = :titulo, descricao = :descricao WHERE quem_somos_id = 1");

        $acao->bindValue(":titulo", $titulo);
        $acao->bindValue(":descricao", $descricao);


        $acao->execute();

        if(isset($_FILES["arquivo"]["tmp_name"])){
            require_once("class/Upload.class.php");
            $handle = new upload($_FILES["arquivo"], "pt_BR");
            if($handle->uploaded){
                $handle->allowed = array("image/*");
                $handle->file_new_name_body   = bin2hex(openssl_random_pseudo_bytes(10));
                $handle->image_resize         = true;
                $handle->image_x              = 1920;
                $handle->image_ratio_y        = true;
                $handle->image_no_enlarging   = true;

                $folder = "uploads/quem_somos/";

                $handle->process("../".$folder);
                if($handle->processed){
                    $acao = Connection::getInstance()->prepare("UPDATE quem_somos SET imagem = :imagem WHERE quem_somos_id = 1");
                        
                    $acao->bindValue(":imagem", $folder.$handle->file_dst_name);
                    $acao->execute();
                }else{
                    echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
                }
            }
        }
        sucesso();
    }else{
        // salva as imagens da galeria
        // salva as imagens da galeria
        // salva as imagens da galeria
        // salva as imagens da galeria
        $folder = "uploads/quem_somos/";


        //Tratamento da array $_FILES da galeria
        if(isset($_FILES["arquivos"]["tmp_name"])){
            $organized_files      = array();
            $type_keys  = array_keys($_FILES["arquivos"]); // name, type, tmp_name, error
        
            $keys = array_keys($_FILES["arquivos"]['tmp_name']);
            foreach($keys as $gb){
                foreach($type_keys as $type){
                    $organized_files[$gb][$type] = $_FILES["arquivos"][$type][$gb];
                }
            }
            $to_be_deleted_if_error = [];
            $alerts = [];


            Connection::getInstance()->beginTransaction();
            $insert = Connection::getInstance()->prepare("INSERT INTO quem_somos_galeria (imagem) VALUES (:imagem)");
            try{

                require_once("class/Upload.class.php");
                foreach($organized_files as $key => $file){
                    $handle = new upload($file, "pt_BR");
                    if($handle->uploaded){

                        //gera imagem principal
                        $name = bin2hex(openssl_random_pseudo_bytes(10));
                        $handle->allowed = array("image/*");
                        $handle->file_new_name_body   = $name;
                        $handle->image_resize         = true;
                        $handle->image_x              = 1280;
                        $handle->image_ratio_y        = true;
                        $handle->image_no_enlarging   = true;
                        $handle->process("../".$folder);
                        if($handle->processed){
                            $to_be_deleted_if_error[] = $folder.$handle->file_dst_name;
                            $to_insert["imagem"] = $folder.$handle->file_dst_name;
                        }else{
                            $alerts[] = ["<b>Falha ao fazer o upload da imagem <u>". $handle->file_src_name  ."</u></b>, ".$handle->error, "danger"];
                            $to_insert["imagem"] = "";
                        }

                        $insert->execute($to_insert);
                    }
                }

                Connection::getInstance()->commit();

                echo "<div class='alert alert-success'>Arquivos enviados com sucesso</div>";

                if(!empty($alerts))  echo "<div class'alert alert-danger'>".implode('<br>', $alerts)."</div>";
                

            }catch(Exception $e){
                Connection::getInstance()->rollBack();
                echo $e->getMessage();
                $prefix = '../';
                foreach($to_be_deleted_if_error as $f){
                    if(!is_dir($prefix.$f) && file_exists($prefix.$f)){
                        @unlink($prefix.$f);
                    }
                }
                echo "<div class='alert alert-success'>Não foi possível fazer o upload das imagens</div>";
            }
            

        }

    }
    

}

if(isset($_GET['excluir_gal_img'])){
    $deletar_id = $_GET['excluir_gal_img'];
    $select = Connection::getInstance()->prepare("SELECT imagem FROM quem_somos_galeria WHERE quem_somos_galeria_id = :id");
    $select->bindValue(":id", $deletar_id);
    $select->execute();
    $imagem = $select->fetch();
    @unlink('../'.$imagem['imagem']);

    $update = Connection::getInstance()->prepare("DELETE FROM quem_somos_galeria WHERE quem_somos_galeria_id = :id");
    $update->bindValue(":id", $deletar_id);
    $update->execute();
    operacao();
}

if(isset($_GET['excluir_imagem'])){
    $imagem = Connection::getInstance()->query("SELECT imagem FROM quem_somos LIMIT 1")->fetchColumn();
    unlink('../'.$imagem);

    $update = Connection::getInstance()->query("UPDATE quem_somos SET imagem = '' LIMIT 1");
    operacao();
}
?>



<div class="row">
<?php if(isset($_GET['sucesso'])):?>
    <div class="col-md-12"><div class="alert alert-success">Operação realizada com sucesso.</div></div>
<?php endif; ?>
    <div class="col-md-12">
        <?php 
            $select = Connection::getInstance()->query("SELECT * FROM quem_somos LIMIT 1")->fetch();  
        ?>
    <div class="panel panel-default">
        <div class="panel-heading">
        <i class="fa fa-home"></i>
        Dados
        </div>
        <div class="panel-body">
        <form method="post" role="form" enctype="multipart/form-data" id="form-quem-somos">
        <input type="hidden" name="dados" value="dados" >
        <div class="row">
            <div class="col-md-6 hidden form-group">
                    <label>Imagem</label>
                    <?php if(!empty($select["imagem"])){?>
                        <img src="../<?= $select["imagem"];?>" class="img-responsive">
                        <a href="?link=<?= $_GET['link'] ?>&excluir_imagem"><button type="button" class="btn btn-sm btn-danger">Excluir Imagem</button></a>
                    <?php }else{ ?>
                        <input type="file" class="form-control" name="arquivo">
                    <?php } ?>
                    <p class="help-block"></p>
                </div>
                <div class="col-md-6 form-group hidden">
                    <label>Titulo *</label>
                    <input class="form-control" name="titulo" value="<?= !empty($select["titulo"]) ? $select['titulo'] : '' ?>" required>
                </div>
        </div>
            
            <div class="form-group">
                <label>Descrição *</label>
                <textarea class="form-control" name="descricao" id="descricao"><?= !empty($select["descricao"]) ? $select['descricao'] : '' ?></textarea>
                <script>CKEDITOR.replace('descricao', {height: '200px'});</script>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
            <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']?>">
        </form>
        <script>
        $("#form-quem-somos").submit( function(e) {
            var messageLength = CKEDITOR.instances['descricao'].getData().trim().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Por favor, coloque uma descrição.' );
                e.preventDefault();
                return;
            }
        });
        </script>
    </div>
    </div>
    </div>



    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
            <i class="fa fa-image"></i>
            Galeria
            </div>
            <div class="panel-body">
                <form action="" method="post" enctype="multipart/form-data" onsubmit="flashMsg('Fazendo upload dos arquivos, aguarde...', 'info', 600000)">
                    <div class="form-group">
                        <input type="file" name="arquivos[]" id="arquivos" multiple required class="form-control">
                        <small>Você pode selecionar mais de um arquivo<br>
                        Tamanho sugerido 680x380px<br>
                        Se o upload não estiver indo tente enviar menos arquivos
                        </small>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
                <div class="row mt-2">
                    <br><br>
                    <?php

                    $results = Connection::getInstance()->query("SELECT * FROM quem_somos_galeria")->fetchAll();
                    foreach($results as $r){
                    ?>
                    <div class="col-xl-3 col-md-4 col-sm-6 wrap_delete_file">
                        <div class="img-thumbnail " style="width:100%;position:relative">
                            <a tabindex="-1" href="index.php?link=<?=$_GET['link']?>&excluir_gal_img=<?=$r['quem_somos_galeria_id']?>" style="position:absolute;right:.3rem;top:.3rem;" href='#' class='btn-sm p-0 btn btn-danger'>
                                <i class='fas fa-times fa-fw'></i>
                            </a>
                            <div style="background-image:url(../<?=$r["imagem"] ? $r["imagem"] : $r["imagem"]?>); background-size: cover; background-position: center; height:200px" class="m-1"></div>
                        </div>
                       
                        
                    </div>
                    <?php
                    }
                    ?>
                </div>
                    
            </div>
        </div>
    </div>
</div>