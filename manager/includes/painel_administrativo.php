<?php
$TABLE = "painel_administrativo";
$PRIMARYKEY = "id";

error_reporting(E_ALL);
ini_set('display_errors', 1);

if($_POST){
    extract($_POST);

    $update = Connection::getInstance()->prepare("UPDATE painel_administrativo SET titulo = :titulo, subtitulo = :subtitulo, cor_primaria = :cor_primaria, cor_secundaria = :cor_secundaria WHERE id = '1'");
    $update->bindValue(":titulo", $titulo);
    $update->bindValue(":subtitulo", $subtitulo);
    $update->bindValue(":cor_primaria", $cor_primaria);
    $update->bindValue(":cor_secundaria", $cor_secundaria);

    if($update->execute()){
        sucesso();
    }else{
        error();
    }

    require_once("class/Upload.class.php");
    $folder = "uploads/".$TABLE."/imagem_login/";
    if(isset($_FILES["imagem_login"]["tmp_name"])){
        $handle = new upload($_FILES["imagem_login"], "pt_BR");
        if($handle->uploaded){
            $handle->file_new_name_body = limpaUrl($handle->file_src_name);
            $handle->allowed = array("image/*");
            $handle->image_resize         = true;
            $handle->image_x = 1280;
            $handle->image_ratio_y        = true;
            $handle->image_no_enlarging   = true;
            $handle->process(__DIR__."/../../".$folder);
            if($handle->processed){
                $acao = Connection::getInstance()->prepare("UPDATE {$TABLE} SET imagem_login = ? WHERE {$PRIMARYKEY} = 1");
                $acao->execute(array($folder.$handle->file_dst_name)); 
            }else{
                echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
            }
        }
    }
    require_once("class/Upload.class.php");
    $folder = "uploads/".$TABLE."/imagem_logo/";
    if(isset($_FILES["imagem_logo"]["tmp_name"])){
        $handle = new upload($_FILES["imagem_logo"], "pt_BR");
        if($handle->uploaded){
            $handle->file_new_name_body = limpaUrl($handle->file_src_name);
            $handle->allowed = array("image/*");
            $handle->image_resize         = true;
            $handle->image_x = 1280;
            $handle->image_ratio_y        = true;
            $handle->image_no_enlarging   = true;
            $handle->process(__DIR__."/../../".$folder);
            if($handle->processed){
                $acao = Connection::getInstance()->prepare("UPDATE {$TABLE} SET imagem_logo = ? WHERE {$PRIMARYKEY} = 1");
                $acao->execute(array($folder.$handle->file_dst_name)); 
            }else{
                echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
            }
        }
    }

}

else
{
    if(isset($_GET["excluir_imagem_login"])){
        $select = Connection::getInstance()->prepare("SELECT imagem_login FROM {$TABLE} WHERE {$PRIMARYKEY} = 1");
        $select->execute();
        $imagem_login = $select->fetch();
        @unlink("../" . $imagem_login["imagem_login"]);
        
        $update = Connection::getInstance()->prepare("UPDATE {$TABLE} SET imagem_login = ''  WHERE {$PRIMARYKEY} = 1");
        $update->execute();
        operacao();
    }
    if(isset($_GET["excluir_imagem_logo"])){
        $select = Connection::getInstance()->prepare("SELECT imagem_logo FROM {$TABLE} WHERE {$PRIMARYKEY} = 1");
        $select->execute();
        $imagem_logo = $select->fetch();
        @unlink("../" . $imagem_logo["imagem_logo"]);
        
        $update = Connection::getInstance()->prepare("UPDATE {$TABLE} SET imagem_logo = ''  WHERE {$PRIMARYKEY} = 1");
        $update->execute();
        operacao();
    }
}



$body = Connection::getInstance()->prepare("SELECT * FROM painel_administrativo");
$body->execute();
$body = $body->fetch();

?>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Empresa<br><small>Geral</small></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>




<div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>
<?php if(isset($_GET['sucesso'])):?>
    <div class="alert alert-success">Operação realizada com sucesso.</div>
<?php endif; ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body card-body">
                <form method="post" class="row" role="form" enctype="multipart/form-data">

                    <div class="form-group col-md-6">
                        <label>Imagem do Login</label><br><small>Tamanho recomendado de 1920px x 1080px</small>
                        <?php if(!empty($body["imagem_login"])){ ?>
                            <img src="../<?= $body["imagem_login"]; ?>" class="img-responsive">
                            <a href="?link=<?= $_GET["link"] ?>&excluir_imagem_login"  class="btn  btn-danger"><i class="fa fa-fw fa-trash"></i></a>
                        <?php }else{ ?>
                            <input type="file" class="form-control " name="imagem_login" >
                        <?php } ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Logo</label><br><small>Tamanho recomendado de 100px x 100px</small>
                        <?php if(!empty($body["imagem_logo"])){ ?>
                            <img src="../<?= $body["imagem_logo"]; ?>" class="img-responsive">
                            <a href="?link=<?= $_GET["link"] ?>&excluir_imagem_logo"  class="btn  btn-danger"><i class="fa fa-fw fa-trash"></i></a>
                        <?php }else{ ?>
                            <input type="file" class="form-control " name="imagem_logo" >
                        <?php } ?>
                    </div>

             <!--        <div class="form-group col-xs-12 col-12" >
                        <label>Texto</label>
                        <textarea  class="form-control tem-ckeditor" rows="6" name="texto" id="texto"><?=$body["texto"]?></textarea>
                    </div>

                         -->
                    <div class="form-group col-md-6">
                        <label>Título</label>
                        <input class="form-control  form-control" type="text" name="titulo" maxlength="25" value="<?=$body["titulo"]?>">
                        <small>Máximo 25 caracteres</small>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Subtítulo</label>
                        <input class="form-control  form-control" type="text" name="subtitulo" maxlength="25" value="<?=$body["subtitulo"]?>">
                        <small>Máximo 25 caracteres</small>
                    </div>

                    <div class="form-group col-xs-6 col-6">
                        <label>Cor do Filtro</label>
                        <input class="form-control  form-control" type="color" name="cor_primaria" value="<?=$body["cor_primaria"]?>">
                    </div>

                    <div class="form-group col-xs-6 col-6 hidden">
                        <label>Cor Secundaria</label>
                        <input class="form-control  form-control" type="color" name="cor_secundaria" value="<?=$body["cor_secundaria"]?>">
                    </div>

                    <div class="form-group col-xs-12 col-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->