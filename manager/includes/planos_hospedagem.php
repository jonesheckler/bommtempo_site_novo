<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
$COLUMNS = array (
  0 => 'plano',
  1 => 'email',
  2 => 'banda_mensal',
  3 => 'espaco_disco',
  4 => 'banco_dados',
  5 => 'ftp',
  6 => 'subdominios',
  7 => 'anti_spam',  
  8 => 'webmail',
  9 => 'painel_admin',
  10 => 'suporte_tecnico',
  11 => 'valor',
  12 => 'label_botao',
  13 => 'ordem',
  14 => 'ativo',
);
$TABLE = "planos_hospedagem";
$PRIMARYKEY = "id_plano_hospedagem";
$IDPLANOSERVICO = 3;
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header font-weight-light">Planos
        <small class="font-weight-light"><br>Hospedagem</small>
        </h1>
    </div>
</div>
<?php
    if(isset($_GET["acao"])){
        $body = array();
        if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST"){
            $body = array_intersect_key($_POST, array_flip($COLUMNS));
            $body["ativo"] = (!empty($body["ativo"]) ? $body["ativo"] : 0);
            $body["anti_spam"] = (!empty($body["anti_spam"]) ? $body["anti_spam"] : 0);
            $body["webmail"] = (!empty($body["webmail"]) ? $body["webmail"] : 0);
            $body["painel_admin"] = (!empty($body["painel_admin"]) ? $body["painel_admin"] : 0);
            $body["valor"] = (!empty($body["valor"]) ? $body["valor"] : 0.00);
        }
        foreach($COLUMNS as $c){
            if(!isset($body[$c])) $body[$c] = "";
        }
if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST"){
    if($_GET["acao"] == "adicionar"){
        
        $acao = Connection::getInstance()->prepare("INSERT INTO {$TABLE} (plano, email, banda_mensal, espaco_disco, banco_dados, ftp, subdominios, anti_spam, webmail, painel_admin, suporte_tecnico, valor, label_botao, ordem, ativo) VALUES (:plano, :email, :banda_mensal, :espaco_disco, :banco_dados, :ftp, :subdominios, :anti_spam, :webmail, :painel_admin, :suporte_tecnico, :valor, :label_botao, :ordem, :ativo)");
        try{
            $acao->execute($body);
            $body[$PRIMARYKEY] = Connection::getInstance()->lastInsertId();
            echo '<div class="alert alert-success">Cadastro do(a) <a class="btn btn-secondary btn-sm" href="index.php?link='.$_GET["link"].'&acao=editar&'.$PRIMARYKEY.'='.$body[$PRIMARYKEY].'">'.$body["plano"].'</a> realizado com sucesso</div>';
            //verifica se tem itens ativos ainda para remover o serviço de ativo e ocultar a coluna no site
            verificaServicoAtivo($TABLE, $IDPLANOSERVICO);
        }catch(Exception $e){
            var_dump($e);
            echo '<div class="alert alert-danger">Não foi possível adicionar o(a) <span title="'. str_replace("\"", "'", $e->getMessage()) .'">'.$body["plano"].'</span></div>';
            $inser_error = true;
        }
    }else{
        $acao = Connection::getInstance()->prepare("UPDATE {$TABLE} SET plano = :plano, email = :email, banda_mensal = :banda_mensal, espaco_disco = :espaco_disco, banco_dados = :banco_dados, ftp = :ftp, subdominios = :subdominios, anti_spam = :anti_spam, webmail = :webmail, painel_admin = :painel_admin, suporte_tecnico = :suporte_tecnico,  valor = :valor, label_botao = :label_botao, ordem = :ordem, ativo = :ativo  WHERE {$PRIMARYKEY} = :{$PRIMARYKEY}");
        try{
            $body[$PRIMARYKEY] = $_GET[$PRIMARYKEY];
            $acao->execute($body);
            echo "<div class='alert alert-success'><b>{$body['plano']}</b> editado com sucesso</div>";
            //verifica se tem itens ativos ainda para remover o serviço de ativo e ocultar a coluna no site
            verificaServicoAtivo($TABLE, $IDPLANOSERVICO);
        }catch(Exception $e){
            echo '<div class="alert alert-danger">Não foi possível editar o(a) <span title="'. str_replace("\"", "'", $e->getMessage()) .'">'.$body["plano"].'</span></div>';
        }
    }


}

if($_GET["acao"] == "adicionar"){
    //Limpa o $body se esta adicionando e se foi salvo no banco
    if(!isset($insert_error)){
        foreach($COLUMNS as $c) $body[$c] = "";
        $body["ativo"] = "1";
        $body["anti_spam"] = "1";
        $body["webmail"] = "1";
        $body["painel_admin"] = "1";
    }
    $panelHeading = "Adicionar <b>Plano</b>";
}else{
    $body = Connection::getInstance()->prepare("SELECT * FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
    $body->execute(array($_GET[$PRIMARYKEY]));
    $body = $body->fetch();
    if(!$body){
        die("<script>alert('Registro não encontrado'); window.location.href = 'index.php?link={$_GET["link"]}'</script>");
    }
    $panelHeading = "Editando <b>".$body["plano"]."</b>";
}
?>
<div class="row">
    <div class="col-lg-12">
    <div class="panel card panel-default">
        <div class="panel-heading card-header"><?=$panelHeading?>
        <a class="btn btn-secondary btn-sm pull-right float-right" href="index.php?link=<?=$_GET['link']?>">voltar</a>
</div>
<div class="panel-body card-body">
                <form method="post" class="row" role="form" enctype="multipart/form-data">

                
                <div class="form-group col-md-6">
                    <label>Plano<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="plano" value="<?=$body["plano"]?>">
                </div>
                
                <div class="form-group col-md-6">
                    <label>E-mail<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="email" value="<?=$body["email"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>Banda Mensal<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="banda_mensal" value="<?=$body["banda_mensal"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>Espaço em disco<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="espaco_disco" value="<?=$body["espaco_disco"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>Banco de Dados<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="number" min="0"  name="banco_dados" value="<?=$body["banco_dados"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>FTP<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="number" min="0"  name="ftp" value="<?=$body["ftp"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>Subdomínios<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="number" min="0"  name="subdominios" value="<?=$body["subdominios"]?>">
                </div>

                <div class="form-group col-md-12">
                    <label>Anti-spam</label><br>
                    <label class="switch ">
                    <input  type="checkbox" value="1" <?=$body["anti_spam"] ? "checked" : ""?> name="anti_spam" class="form-control  ">
                    <span class="slider"></span>
                    </label>
                </div>

                <div class="form-group col-md-12">
                    <label>Webmail</label><br>
                    <label class="switch ">
                    <input  type="checkbox" value="1" <?=$body["webmail"] ? "checked" : ""?> name="webmail" class="form-control  ">
                    <span class="slider"></span>
                    </label>
                </div>

                <div class="form-group col-md-12">
                    <label>Painel Admin</label><br>
                    <label class="switch ">
                    <input  type="checkbox" value="1" <?=$body["painel_admin"] ? "checked" : ""?> name="painel_admin" class="form-control  ">
                    <span class="slider"></span>
                    </label>
                </div>

                <div class="form-group col-md-6">
                    <label>Suporte Técnico<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="suporte_tecnico" value="<?=$body["suporte_tecnico"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>Valor<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  decimal" type="text" name="valor" value="<?=$body["valor"]?>">
                </div>

                <div class="form-group col-md-6">
                    <label>Label Botão<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="label_botao" value="<?=$body["label_botao"]?>">
                </div>

                <div class="form-group col-md-6">
                        <label>Ordenação</label>
                        <input class="form-control" type="number" min="0" name="ordem" value="<?= !empty($body["ordem"]) ? $body['ordem'] : '0' ?>">
                    </div>

                <div class="form-group col-xs-12 col-12">
                    <label>Ativo</label><br>
                    <label class="switch ">
                    <input  type="checkbox" value="1" <?=$body["ativo"] ? "checked" : ""?> name="ativo" class="form-control  ">
                    <span class="slider"></span>
                    </label>
                </div>

                    <div class="row"></div>
                    <button type="submit" class="btn btn-primary"><?=$_GET["acao"] === "adicionar" ? "Adicionar" : "Salvar"?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
}else{
if(!empty($_GET["excluir"])){
    $del = Connection::getInstance()->prepare("DELETE FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
    $del->execute(array($_GET["excluir"]));
    //verifica se tem itens ativos ainda para remover o serviço de ativo e ocultar a coluna no site
    verificaServicoAtivo($TABLE, $IDPLANOSERVICO);
}

$results = Connection::getInstance()->query("SELECT * FROM {$TABLE} ORDER BY ordem")->fetchAll();

$conta_results = count($results);
?>
    <div class="panel card panel-default">
        <div class="panel-heading card-header">
                Listagem de planos de Hospedagem
                <a href="?link=<?= $_GET["link"]?>&acao=adicionar" class="btn btn-sm btn-success pull-right float-right">Adicionar</a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body card-body">
        <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                <thead>
                    <tr>
                        <th width="20" class="hidden">#</th>
                        <th width="100">Plano</th>
                        <th >Suporte Técnico</th>
                        <th >Banda Mensal</th>
                        <th width="100">Valor</th>
                        <th width="60">Ordem</th>
                        <th width="60">Ativo</th>
                        <th width="100">Ações</th>
                    </tr>
                </thead>
            <tbody>
            <?php
                foreach ($results as $t) {
            ?>
                <tr class="gradeA">
                    
                    <td >
                       <?=$t["plano"]?>
                    </td>
                    
                    <td >
                       <?=$t["suporte_tecnico"]?>
                    </td>
                    <td >
                       <?=$t["banda_mensal"]?>
                    </td>
                  
                    <td >
                        R$ <?=number_format($t['valor'], 2, ',', '.')?>
                    </td>

                    <td >
                        <?=$t["ordem"]?>
                    </td>
                    
                    <td >
                        <span style="display:none;"><?=$t["ativo"]?></span>                        <?=($t["ativo"] ? "Sim" : "Não");?>
                    </td>
                    
                    <td>
                        <a href="?link=<?=$_GET["link"]?>&acao=editar&<?=$PRIMARYKEY ?>=<?=$t[$PRIMARYKEY]?>" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                  <a href="javascript:confirma('?link=<?= $_GET["link"] ?>&excluir=<?=$t[$PRIMARYKEY]?>')"  class="btn  btn-danger "><i class="fa fa-trash fa-fw"></i></a>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->

<?php
}

?>

<script>
$('.decimal').mask('#0.00', {reverse: true});

</script>