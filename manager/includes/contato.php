<?php
 //error_reporting(E_ALL);
 //ini_set('display_errors', 1);
    if($_POST){
        extract($_POST);
        $acao = Connection::getInstance()->prepare("UPDATE contato SET  rua = :rua, numero = :numero, bairro = :bairro, complemento = :complemento, cidade = :cidade, uf = :uf, mapa = :mapa, rua2 = :rua2, numero2 = :numero2, bairro2 = :bairro2, complemento2 = :complemento2, cidade2 = :cidade2, uf2 = :uf2, mapa2 = :mapa2, horario = :horario, horario2 = :horario2, cep = :cep, facebook = :facebook, instagram = :instagram, youtube = :youtube WHERE id = 1");
        
       // $acao->bindValue(":latitude", $latitude);
       // $acao->bindValue(":longitude", $longitude);
        $acao->bindValue(":rua", $rua);
        $acao->bindValue(":numero", $numero);
        $acao->bindValue(":bairro", $bairro);
        $acao->bindValue(":cidade", $cidade);
        $acao->bindValue(":uf", $uf);
        $acao->bindValue(":complemento", $complemento);
        $acao->bindValue(":mapa", $mapa);
        $acao->bindValue(":rua2", $rua2);
        $acao->bindValue(":numero2", $numero2);
        $acao->bindValue(":bairro2", $bairro2);
        $acao->bindValue(":cidade2", $cidade2);
        $acao->bindValue(":uf2", $uf2);
        $acao->bindValue(":complemento2", $complemento2);
        $acao->bindValue(":mapa2", $mapa2);
        $acao->bindValue(":horario", $horario);
        $acao->bindValue(":horario2", $horario2);
        $acao->bindValue(":cep", $cep);
        //$acao->bindValue(":telefone", $telefone);
        //$acao->bindValue(":telefone_res", $telefone_res);
        $acao->bindValue(":facebook", $facebook);
        $acao->bindValue(":youtube", $youtube);
        $acao->bindValue(":instagram", $instagram);

        if($acao->execute()){
            sucesso();
        }else{
            error();
        }
    }

?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Contato</h1>
    </div>
</div>
<div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>
<?php if(isset($_GET['sucesso'])):?>
    <div class="alert alert-success">Operação realizada com sucesso.</div>
<?php endif; ?>
<div class="row">
<?php 
    $select = Connection::getInstance()->prepare("SELECT * FROM contato WHERE id = 1");
    $select->execute();
    $select = $select->fetch();  
?>
    <form method="post" role="form" enctype="multipart/form-data">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Informações de contato - Feliz
                </div>   
                <div class="panel-body">
                    
                        <!--
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Latitude</label>
                                <input type="text" class="form-control" name="latitude" value="<?php echo stripslashes($select["latitude"])?>"/>
                                </div>
                            <div class="col-md-6">
                                <label>Longitude</label>
                                <input type="text" class="form-control" name="longitude" value="<?php echo stripslashes($select["longitude"])?>"/>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label>Rua</label>
                            <input type="text" class="form-control" name="rua" id="rua" value="<?php echo stripslashes($select["rua"])?>">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Número</label>
                                <input type="number" class="form-control" name="numero" id="numero" value="<?php echo stripslashes($select["numero"])?>"/>
                            </div>
                            <div class="col-md-6">
                                <label>Bairro</label>
                                <input type="text" class="form-control" name="bairro" id="bairro" value="<?php echo stripslashes($select["bairro"])?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Complemento</label>
                            <input type="text" class="form-control" name="complemento" id="complemento" value="<?php echo stripslashes($select["complemento"])?>">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8">
                                <label>Cidade</label>
                                <input type="text" class="form-control" name="cidade" id="cidade" value="<?php echo stripslashes($select["cidade"])?>"/>
                            </div>
                            <div class="col-md-4">
                                <label>UF</label>
                                <input type="text" class="form-control" name="uf" id="uf" value="<?php echo stripslashes($select["uf"])?>" maxlength="2"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>URL Mapa</label>
                            <input type="text" class="form-control" name="mapa" id="mapa" value="<?php echo stripslashes($select["mapa"])?>">
                        </div>
                       
                        <div class="form-group">
                            <label>Horário de Atendimento *</label>
                            <textarea class="form-control" name="horario" id="horario"><?= !empty($select["horario"]) ? $select['horario'] : '' ?></textarea>
                        </div>
                        <!--
                        <div class="form-group ">
                            <label>CEP</label>
                            <input type="text" class="form-control cep" name="cep" value="<?php echo stripslashes($select["cep"])?>" />
                        </div> 
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Telefone Princial</label>
                                <input type="text" class="form-control telefone" name="telefone"value="<?php echo stripslashes($select["telefone"])?>" />
                            </div>
                            <div class="col-md-6">
                                <label>Telefone Secundário</label>
                                <input type="text" class="form-control telefone" name="telefone_res" value="<?php echo stripslashes($select["telefone_res"])?>">
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label>Facebook</label>
                            <input type="text" class="form-control" name="facebook" value="<?php echo stripslashes($select["facebook"])?>">
                        </div>
                        <div class="form-group">
                            <label>Instagram</label>
                            <input type="text" class="form-control" name="instagram" value="<?php echo stripslashes($select["instagram"])?>">
                        </div>
                        <div class="form-group hidden">
                            <label>Youtube</label>
                            <input type="text" class="form-control" name="youtube" value="<?php echo stripslashes($select["youtube"])?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']?>">
                    
                </div>
            </div>
        </div>



        <div class="col-lg-6">
            <div class="panel panel-default">
                    <div class="panel-heading">
                        Informações de contato - São Seb. do Caí
                    </div>   
                <div class="panel-body">
                <div class="form-group">
                            <label>Rua</label>
                            <input type="text" class="form-control" name="rua2" id="rua2" value="<?php echo stripslashes($select["rua2"])?>">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Número</label>
                                <input type="number" class="form-control" name="numero2" id="numero2" value="<?php echo stripslashes($select["numero2"])?>"/>
                            </div>
                            <div class="col-md-6">
                                <label>Bairro</label>
                                <input type="text" class="form-control" name="bairro2" id="bairro2" value="<?php echo stripslashes($select["bairro2"])?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Complemento</label>
                            <input type="text" class="form-control" name="complemento2" id="complemento2" value="<?php echo stripslashes($select["complemento2"])?>">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8">
                                <label>Cidade</label>
                                <input type="text" class="form-control" name="cidade2" id="cidade2" value="<?php echo stripslashes($select["cidade2"])?>"/>
                            </div>
                            <div class="col-md-4">
                                <label>UF</label>
                                <input type="text" class="form-control" name="uf2" id="uf2" value="<?php echo stripslashes($select["uf2"])?>" maxlength="2"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>URL Mapa</label>
                            <input type="text" class="form-control" name="mapa2" id="mapa2" value="<?php echo stripslashes($select["mapa2"])?>">
                        </div>

                        <div class="form-group">
                            <label>Horário de Atendimento *</label>
                            <textarea class="form-control" name="horario2" id="horario"><?= !empty($select["horario2"]) ? $select['horario2'] : '' ?></textarea>
                        </div>
                        

                </div>

            </div>
        </div>   
    </form>
</div>
<!-- /.row -->