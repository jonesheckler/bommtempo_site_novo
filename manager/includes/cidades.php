<?php
 //error_reporting(E_ALL);
 //ini_set('display_errors', 1);
$COLUMNS = array (
  0 => 'titulo_principal',
  1 => 'ordem',
  2 => 'ativo',
);
$TABLE = "cidades";
$PRIMARYKEY = "id_cidade";


?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header font-weight-light">Cidades
        <small class="font-weight-light"><br>Cadastro de Cidades</small>
        </h1>
    </div>
</div>
<?php
    if(isset($_GET["acao"])){
        $body = array();
        if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST"){
            $body = array_intersect_key($_POST, array_flip($COLUMNS));
            $body["ativo"] = (!empty($body["ativo"]) ? $body["ativo"] : 0);
        }
        foreach($COLUMNS as $c){
            if(!isset($body[$c])) $body[$c] = "";
        }
if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST"){
    if($_GET["acao"] == "adicionar"){
       // var_dump($_POST);exit();
        $acao = Connection::getInstance()->prepare("INSERT INTO {$TABLE} (titulo_principal, ordem, ativo) VALUES (:titulo_principal, :ordem, :ativo)");

        try{
            $acao->execute($body);
            $body[$PRIMARYKEY] = Connection::getInstance()->lastInsertId();
            echo '<div class="alert alert-success">Cadastro do(a) <a class="btn btn-secondary btn-sm" href="index.php?link='.$_GET["link"].'&acao=editar&'.$PRIMARYKEY.'='.$body[$PRIMARYKEY].'">'.$body["titulo_principal"].'</a> realizado com sucesso</div>';
            
        }catch(Exception $e){
            //var_dump($e);exit();
            echo '<div class="alert alert-danger">Não foi possível adicionar o(a) <span title="'. str_replace("\"", "'", $e->getMessage()) .'">'.$body["titulo_principal"].'</span></div>';
            $inser_error = true;
        }
    }else{
        $acao = Connection::getInstance()->prepare("UPDATE {$TABLE} SET titulo_principal = :titulo_principal, ordem = :ordem, ativo = :ativo  WHERE {$PRIMARYKEY} = :{$PRIMARYKEY}");
        try{
            $body[$PRIMARYKEY] = $_GET[$PRIMARYKEY];
            $acao->execute($body);
            echo "<div class='alert alert-success'><b>{$body['titulo_principal']}</b> editado com sucesso</div>";

        }catch(Exception $e){
            var_dump($e);exit();
            echo '<div class="alert alert-danger">Não foi possível editar o(a) <span title="'. str_replace("\"", "'", $e->getMessage()) .'">'.$body["titulo_principal"].'</span></div>';
        }


    }


}

if($_GET["acao"] == "adicionar"){
    //Limpa o $body se esta adicionando e se foi salvo no banco
    if(!isset($insert_error)){
        foreach($COLUMNS as $c) $body[$c] = "";
        $body["ativo"] = "1";
    }
    $panelHeading = "Adicionar <b>Plano</b>";
}else{
    $body = Connection::getInstance()->prepare("SELECT * FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
    $body->execute(array($_GET[$PRIMARYKEY]));
    $body = $body->fetch();
    if(!$body){
        die("<script>alert('Registro não encontrado'); window.location.href = 'index.php?link={$_GET["link"]}'</script>");
    }
    $panelHeading = "Editando <b>".$body["titulo_principal"]."</b>";
}
?>
<div class="row">
    <div class="col-lg-12">
    <div class="panel card panel-default">
        <div class="panel-heading card-header"><?=$panelHeading?>
        <a class="btn btn-secondary btn-sm pull-right float-right" href="index.php?link=<?=$_GET['link']?>">voltar</a>
</div>
<div class="panel-body card-body">
                <form method="post" class="row" role="form" enctype="multipart/form-data">

         
                <div class="form-group col-xs-12 col-12">
                    <label>Cidade<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="titulo_principal" value="<?=$body["titulo_principal"]?>">
                </div>

                <div class="form-group col-md-6">
                        <label>Ordenação</label>
                        <input class="form-control" type="number" min="0" name="ordem" value="<?= !empty($body["ordem"]) ? $body['ordem'] : '0' ?>">
                    </div>

                <div class="form-group col-xs-12 col-12">
                    <label>Ativo</label><br>
                    <label class="switch ">
                    <input  type="checkbox" value="1" <?=$body["ativo"] ? "checked" : ""?> name="ativo" class="form-control  ">
                    <span class="slider"></span>
                    </label>
                </div>

                    <div class="row"></div>
                    <button type="submit" class="btn btn-primary"><?=$_GET["acao"] === "adicionar" ? "Adicionar" : "Salvar"?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
}else{
if(!empty($_GET["excluir"])){
    $del = Connection::getInstance()->prepare("DELETE FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
    $del->execute(array($_GET["excluir"]));
}

$results = Connection::getInstance()->query("SELECT * FROM {$TABLE}  ORDER BY ordem")->fetchAll();

$conta_results = count($results);
?>
    <div class="panel card panel-default">
        <div class="panel-heading card-header">
                Listagem de cidades
                <a href="?link=<?= $_GET["link"]?>&acao=adicionar" class="btn btn-sm btn-success pull-right float-right">Adicionar</a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body card-body">
                <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                <thead>
                    <tr>
                        <th width="60">Ordem</th>
                        <th width="">Cidade</th>
                        <th width="100">Ativo</th>
                        <th width="100">Ações</th>
                    </tr>
                </thead>
            <tbody>
            <?php
                foreach ($results as $t) {
            ?>
                <tr class="gradeA">
                    <td class="hidden"></td>
                    <td >
                        <?=$t["ordem"]?>
                    </td>
                    <td >
                       <?=$t["titulo_principal"]?>
                    </td>
                    <td >
                        <span style="display:none;"><?=$t["ativo"]?></span>                        <?=($t["ativo"] ? "Sim" : "Não");?>
                    </td>
                    
                    <td>
                        <a href="?link=<?=$_GET["link"]?>&acao=editar&<?=$PRIMARYKEY ?>=<?=$t[$PRIMARYKEY]?>" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                  <a href="javascript:confirma('?link=<?= $_GET["link"] ?>&excluir=<?=$t[$PRIMARYKEY]?>')"  class="btn  btn-danger "><i class="fa fa-trash fa-fw"></i></a>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->

<?php
}

?>

<script>
$('.decimal').mask('#0.00', {reverse: true});


</script>