<?php
 //error_reporting(E_ALL);
 //ini_set('display_errors', 1);
    if($_POST){
        extract($_POST);
//var_dump($_POST);
        $ativo = ($ativo == 1) ? 1 : 0;
        if(!empty($_GET['acao'])){
            if($_GET['acao'] == 'editar'){
                $id = $_GET['id'];
                $acao = Connection::getInstance()->prepare("UPDATE sobre SET titulo = :titulo, descricao = :descricao,  ativo = :ativo, ordem = :ordem WHERE id = :id");
                $acao->bindValue(":id", $id);
            }else if($_GET['acao'] == 'adicionar'){
                $acao = Connection::getInstance()->prepare("INSERT INTO sobre (titulo, descricao, ativo, ordem) VALUES (:titulo, :descricao, :ativo, :ordem)");
            }
        }

        $acao->bindValue(":titulo", $titulo);
        $acao->bindValue(":descricao", $descricao);
        $acao->bindValue(":ativo", $ativo);
        $acao->bindValue(":ordem", $ordem);

        $acao->execute();

        if($_GET['acao'] == 'adicionar'){
            $id = Connection::getInstance()->lastInsertId();
        }
        if(isset($_FILES["imagem"]["tmp_name"])){
            require_once("class/Upload.class.php");
            $handle = new upload($_FILES["imagem"], "pt_BR");
            if($handle->uploaded){
                $handle->allowed = array("image/*");
                $handle->file_new_name_body   = bin2hex(openssl_random_pseudo_bytes(10));
                $handle->image_resize         = true;
                $handle->image_x              = 1920;
                $handle->image_ratio_y        = true;
                $handle->image_no_enlarging   = true;

                $folder = "uploads/sobre/".$id."/";

                $handle->process("../".$folder);
                if($handle->processed){
                    $acao = Connection::getInstance()->prepare("UPDATE sobre SET imagem = :imagem WHERE id = :id");
                        
                    $acao->bindValue(":id", $id);
                    $acao->bindValue(":imagem", $folder.$handle->file_dst_name);
                    $acao->execute();
                }else{
                    echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
                }
            }
        }
        sucesso();            

    }  
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Home</h1>
    </div>
</div>
<div class="row">
<?php if(!empty($_GET['acao']) && ($_GET['acao'] == 'adicionar' || $_GET['acao'] == 'editar')){ 

    if(isset($_GET['excluir_imagem'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT imagem FROM sobre WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $imagem = $select->fetch();
        @unlink('../'.$imagem['imagem']);

        $update = Connection::getInstance()->prepare("UPDATE sobre SET imagem = '' WHERE id = :id");
        $update->bindValue(":id", $id);
        $update->execute();
        operacao();
    }

    if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT * FROM sobre WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $select = $select->fetch();
        
        $ativo = ($select["ativo"] == 1) ? 'checked="checked"' : '';
    }else{
        $ativo ='checked="checked"';
    }

?>
    <div class="col-lg-12">
        <div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>  
        <div class="panel panel-default">
            <div class="panel-heading">
                Sobre a Bomm Tempo
            </div>   
            <div class="panel-body">
                <form method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Imagem </label>
                        <?php if(!empty($select["imagem"])){?>
                            <img src="../<?= $select["imagem"];?>" class="img-responsive">
                            <a href="?link=<?= $_GET['link'] ?>&id=<?= $_GET['id'] ?>&acao=<?= $_GET['acao'] ?>&excluir_imagem"><button type="button" class="btn btn-sm btn-danger">Excluir Imagem</button></a>
                        <?php }else{ ?>
                            <input type="file" class="form-control" name="imagem" autofocus >
                        <?php } ?>
                        <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                        <label>Titulo</label>
                        <input class="form-control" name="titulo" value="<?= !empty($select["titulo"]) ? $select['titulo'] : '' ?>">
                    </div>

                    <div class="form-group">
                        <label>Descrição *</label>
                        <textarea class="form-control" name="descricao" id="descricao"><?= !empty($select["descricao"]) ? $select['descricao'] : '' ?></textarea>
                        <script>CKEDITOR.replace('descricao', {height: '200px'});</script>
                    </div>
                    
                    <div class="form-group">
                        <label>Ordenação</label>
                        <input class="form-control" type="number" min="0" name="ordem" value="<?= !empty($select["ordem"]) ? $select['ordem'] : '0' ?>">
                        
                    </div>
                    
                   
                    <div class="form-group ">
                        <label>Ativo</label><br>
                        <label class="switch ">
                        <input  type="checkbox" value="1" <?=$select["ativo"] ? "checked" : ""?> name="ativo" class="form-control  ">
                        <span class="slider"></span>
                        </label>
                        
                    </div>

                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <a href="?link=<?= $_GET['link'] ?>" class="btn btn-default">Voltar</a>
                    <input type="hidden" name="id" id="id" value="<?= $_GET['id']?>">
                </form>
            </div>                    
        </div>
    </div>

<?php }else{ 

    if(!empty($_GET['acao']) && ($_GET['acao'] == 'excluir')){
        $id = $_GET['id'];

        $del = Connection::getInstance()->prepare("DELETE FROM sobre WHERE id = :id");
        $del->bindValue(":id", $id);
        $del->execute();

        sucesso();
    }

?>
<div class="col-lg-12">
    <?php if(isset($_GET['sucesso'])):?>
        <div class="alert alert-success">Operação realizada com sucesso.</div>
    <?php endif; ?>

    <?php
    $select = Connection::getInstance()->query("SELECT * FROM sobre ORDER BY ordem asc")->fetchAll();
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                   <h4>Listagem </h4>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-6">
                   
                    <!--
                    <a href="?link=<?= $_GET['link']?>&acao=adicionar"><button type="button" class="btn btn-md btn-success pull-right">Adicionar</button></a>
-->
                </div>
                
            </div>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                    <thead>
                        <tr>
                            <th class="hidden">id</th>
                            <th width="80">Ordem</th>
                            <th>Título</th>
                            <th width="200">Imagem</th>
                            
                           <!--  <th>URL</th> -->
                             <th width="80">Ativo</th> 
                            <th width="150">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // select esta acima

                        foreach ($select as $key => $s) {
                            $ativo = ($s["ativo"] == 1) ? 'Sim' : 'Não';
                    ?>
                        <tr class="gradeA">
                            <td class="text-center"><?= $s["ordem"]?></td>
                            <td class="text-center"><?= $s['titulo'] ?> </td>
                            <td class="text-center">
                                <a href="<?= $s["imagem"] ? '../'.$s["imagem"] : 'dist/css/images/sem_imagem.png' ?>" class="fancybox">
                                    <img class="img-responsive" src="<?= $s["imagem"] ? '../'.$s["imagem"] : 'dist/css/images/sem_imagem.png' ?>" style="margin: auto;" >
                                </a>
                            </td>
                            
                            <td class="text-center"><?= $ativo?></td> 
                            <td class="text-center">
                                <a href="?link=<?=$_GET["link"]?>&acao=editar&id=<?= $s['id']?>" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="javascript:confirma('?link=<?= $_GET["link"] ?>&acao=excluir&id=<?= $s['id']?>')"  class="btn  btn-danger "><i class="fa fa-trash fa-fw"></i></a>
    
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>
</div>