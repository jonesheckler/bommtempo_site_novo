
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Mensagens via Site</h1>
    </div>
</div>
<div class="row">
<?php

    if(!empty($_GET['acao']) && ($_GET['acao'] == 'excluir')){
        $id = $_GET['id'];

        $del = Connection::getInstance()->prepare("DELETE FROM mensagens_site WHERE id = :id");
        $del->bindValue(":id", $id);
        $del->execute();

        sucesso();
    }

?>
<div class="col-lg-12">
    <?php if(isset($_GET['sucesso'])):?>
        <div class="alert alert-success">Operação realizada com sucesso.</div>
    <?php endif; ?>

    <?php
    $select = Connection::getInstance()->query("SELECT * FROM mensagens_site ORDER BY id DESC")->fetchAll();
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                   <h4>Lista de Mensagens</h4>
                </div>
                
            </div>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                    <thead>
                        <tr>
                            <th width="100">Enviado Em</th>
                            <th width="150">Nome</th>
                            <th width="150">E-mail</th>
                            <th width="100">Telefone</th>
                            <th width="120">Motivo</th>
                            <th width="120">Cidade</th>
                            <th width="80">Método</th>
                            <th>Mensagem</th>
                            <th width="100">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // select esta acima

                        foreach ($select as $key => $s) {
                            $enviado_em = date('d/m/Y H:i', strtotime($s['enviado_em']));
                    ?>
                        <tr class="gradeA">
                            <td class="text-center"><span class="hidden"><?=$s['enviado_em']?></span><?= ($enviado_em)?></td>
                            <td class="text-center"><?= $s["nome"]?></td>
                            <td class="text-center"><?= $s["email"]?></td>
                            <td class="text-center"><?= $s["telefone"]?></td>
                            <td class="text-center"><?= $s["motivo"]?></td>
                            <td class="text-center"><?= $s["cidade"]?></td>
                            <td class="text-center"><?= $s["metodo"]?></td>
                            <td class="text-left"><?= $s["mensagem"]?></td>
                            <td class="text-center">
                                <a href="javascript:confirma('?link=<?= $_GET['link']?>&acao=excluir&id=<?= $s['id']?>')">
                                    <button type="button" class="btn btn-sm btn-danger">Excluir</button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>