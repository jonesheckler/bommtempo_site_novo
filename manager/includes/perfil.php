<?php
    if($_POST){
        extract($_POST);

        $troca_senha = '';
        if($senha != '' && $senha2 != ''){
            $troca_senha = ", senha = :senha";
        }
        $update = Connection::getInstance()->prepare("UPDATE usuarios SET nome = :nome, email = :email ".$troca_senha." WHERE id = '".$_SESSION["id_usuario"]."'");
        $update->bindValue(":nome", $nome);
        $update->bindValue(":email", $email);
        if($senha != '' && $senha2 != ''){
            $senha = md5($senha);
            $update->bindValue(":senha", $senha);
        }
        if($update->execute()){
            sucesso();
        }else{
            error();
        }
    }

    $usuario = Connection::getInstance()->prepare("SELECT * FROM usuarios WHERE id = '".$_SESSION["id_usuario"]."'");
    $usuario->execute();
    $usuario = $usuario->fetch();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Perfil</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>
<?php if(isset($_GET['sucesso'])):?>
    <div class="alert alert-success">Operação realizada com sucesso.</div>
<?php endif; ?>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Altere os dados de seu cadastro
            </div>   
            <div class="panel-body">
                <form method="post" id="perfil" role="form">
                    <div class="form-group">
                        <label>Nome</label>
                        <input class="form-control" name="nome" id="nome" value="<?php echo $usuario["nome"]?>" autofocus>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input class="form-control" name="email" id="email" value="<?php echo $usuario["email"]?>">
                    </div>
                    <div class="form-group">
                        <label>Usuário *</label>
                        <input class="form-control" name="usuario" id="usuario" readonly="readonly" value="<?php echo $usuario["usuario"]?>">
                        <p class="help-block">* Para alterar o usuário utilize o painel de configurações, caso tenha permissão.</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Senha *</label>
                        <input class="form-control" type="password" name="senha" id="senha">
                    </div>
                    <div class="form-group">
                        <label>Confirmar Senha *</label>
                        <input class="form-control" type="password" name="senha2" id="senha2">
                        <p class="help-block">* Caso não queira alterar a senha, deixe os campos em branco.</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>                    
        </div>
    </div>
</div>
<!-- /.row -->