<?php
    $visitas = Connection::getInstance()->prepare("SELECT * FROM acessos");
    $visitas->execute();
    $visitas = $visitas->rowCount();

    $online = Connection::getInstance()->query("SELECT * FROM acessos_online WHERE time >= ".(time()-(5*60))."");
    $online->execute();
    $online = $online->rowCount();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php if(!isset($_SESSION['painel'])){ echo 'Informações Gerais';}else{echo 'Seja bem-vindo(a)';}?></h1>
    </div>
</div>
<?php if(!isset($_SESSION['painel'])){?>
<div class="row">
    <div class="col-lg-3 col-md-3">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?= $visitas ?></div>
                        <div>Visitas</div>
                    </div>
                </div>
            </div>                       
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?= $online ?></div>
                        <div>Visitantes Online</div>
                    </div>
                </div>
            </div>                        
        </div>
    </div>
</div>
<?php } ?>