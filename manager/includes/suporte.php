<?php
$TABLE = "suporte";
$PRIMARYKEY = "id";


    if($_POST){
        extract($_POST);

        $ativo = ($ativo == 1) ? 1 : 0;
        if(!empty($_GET['acao'])){
            if($_GET['acao'] == 'editar'){
                $id = $_GET['id'];
                $acao = Connection::getInstance()->prepare("UPDATE suporte SET categoria = :categoria, pergunta = :pergunta, resposta = :resposta, link = :link, ativo = :ativo, ordem = :ordem WHERE id = :id");
                $acao->bindValue(":id", $id);
            }else if($_GET['acao'] == 'adicionar'){
                $acao = Connection::getInstance()->prepare("INSERT INTO suporte (categoria, pergunta, resposta, link,  ativo, ordem) VALUES (:categoria, :pergunta, :resposta, :link, :ativo, :ordem)");
            }
        }
        
        $acao->bindValue(":categoria", $categoria);
        $acao->bindValue(":pergunta", $pergunta);
        $acao->bindValue(":resposta", $resposta);
        $acao->bindValue(":ativo", $ativo);
        $acao->bindValue(":link", $link);
        $acao->bindValue(":ordem", $ordem);

        $acao->execute();

        if($_GET['acao'] == 'adicionar'){
            $id = Connection::getInstance()->lastInsertId();
        }
        if(isset($_FILES["documento"]["tmp_name"])){
            require_once("class/Upload.class.php");
            $handle = new upload($_FILES["documento"], "pt_BR");
            if($handle->uploaded){
                $handle->allowed = array("image/*");
                $handle->file_new_name_body   = bin2hex(openssl_random_pseudo_bytes(10));
                $handle->image_resize         = true;
                $handle->image_x              = 1920;
                $handle->image_ratio_y        = true;
                $handle->image_no_enlarging   = true;

                $folder = "uploads/documento/".$id."/";

                $handle->process("../".$folder);
                if($handle->processed){
                    $acao = Connection::getInstance()->prepare("UPDATE suporte SET documento = :documento WHERE id = :id");
                        
                    $acao->bindValue(":id", $id);
                    $acao->bindValue(":documento", $folder.$handle->file_dst_name);
                    $acao->execute();
                }else{
                    echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
                }
            }
        }
        sucesso();            

    }  
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Home</h1>
    </div>
</div>
<div class="row">
<?php if(!empty($_GET['acao']) && ($_GET['acao'] == 'adicionar' || $_GET['acao'] == 'editar')){ 

    if(isset($_GET['excluir_imagem'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT documento FROM suporte WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $imagem = $select->fetch();
        @unlink('../'.$imagem['documento']);

        $update = Connection::getInstance()->prepare("UPDATE suporte SET documento = '' WHERE id = :id");
        $update->bindValue(":id", $id);
        $update->execute();
        operacao();
    }

    if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $select = Connection::getInstance()->prepare("SELECT * FROM suporte WHERE id = :id");
        $select->bindValue(":id", $id);
        $select->execute();
        $select = $select->fetch();
        
        $ativo = ($select["ativo"] == 1) ? 'checked="checked"' : '';
    }else{
        $ativo ='checked="checked"';
    }

?>
    <div class="col-lg-12">
        <div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>  
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastro de Perguntas
            </div>   
            <div class="panel-body">
                <form method="post" role="form" enctype="multipart/form-data">

                    <div class="form-group " >
                    <label>Categoria<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>

                        <select required  class="form-control  " name="categoria" id="categoria">
                            <option value="0">Selecione</option>
                            <option <?=($select["categoria"] == $select['categoria'] ? "selected" : "") ?> value="sobre-minha-internet">Sobre minha internet</option>
                            <option <?=($select["categoria"] == $select['categoria'] ? "selected" : "") ?> value="sobre-meu-pagamento">Sobre meu pagamento</option>
                            <option <?=($select["categoria"] == $select['categoria'] ? "selected" : "") ?> value="sobre-meu-contrato">Sobre meu contrato</option>
                            <option <?=($select["categoria"] == $select['categoria'] ? "selected" : "") ?> value="sobre-a-bommtempo">Sobre a Bommtempo</option>
                        </select>
                    </div>
                        
                    <div class="form-group">
                        <label>Documento </label>
                        <?php if(!empty($select["documento"])){?>
                            <img src="../<?= $select["documento"];?>" class="img-responsive">
                            <a href="?link=<?= $_GET['link'] ?>&id=<?= $_GET['id'] ?>&acao=<?= $_GET['acao'] ?>&excluir_imagem"><button type="button" class="btn btn-sm btn-danger">Excluir Imagem</button></a>
                        <?php }else{ ?>
                            <input type="file" class="form-control" name="documento" autofocus >
                        <?php } ?>
                        <p class="help-block">* PDF.</p>
                    </div>
                    <div class="form-group">
                        <label>Pergunta</label>
                        <input class="form-control" name="pergunta" value="<?= !empty($select["pergunta"]) ? $select['pergunta'] : '' ?>">
                    </div>
                    <div class="form-group">
                        <label>Resposta</label>
                        <textarea class="form-control" name="resposta" id="descricao"><?= !empty($select["resposta"]) ? $select['resposta'] : '' ?></textarea>
                        <script>CKEDITOR.replace('resposta', {height: '200px'});</script>
                    </div>
                   
                    <!--
                    <div class="form-group">
                        <label>Link </label>
                        <input type="url" class="form-control" name="link" value="<?= !empty($select["link"]) ? $select['link'] : '' ?>" >
                    </div>-->
                    <div class="form-group">
                        <label>Ordenação</label>
                        <input class="form-control" type="number" min="0" name="ordem" value="<?= !empty($select["ordem"]) ? $select['ordem'] : '0' ?>">
                        
                    </div>
                    
                    <!-- <div class="form-group">
                        <label>Link</label>
                        <input class="form-control" name="slug" value="<?= !empty($select["slug"]) ? $select['slug'] : '' ?>">
                    </div>
                    <hr>
                    <div class="form-group ">
                        <label>Status</label>
                        <div class="checkbox"><label class="nopadding"><input type="checkbox" name="ativo" class="flat" value="1" <?= $ativo?>> Ativo</label></div>
                    </div> -->

                    <div class="form-group ">
                        <label>Ativo</label><br>
                        <label class="switch ">
                        <input  type="checkbox" value="1" <?=$select["ativo"] ? "checked" : ""?> name="ativo" class="form-control  ">
                        <span class="slider"></span>
                        </label>
                        
                    </div>

                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <a href="?link=<?= $_GET['link'] ?>" class="btn btn-default">Voltar</a>
                    <input type="hidden" name="id" id="id" value="<?= $_GET['id']?>">
                </form>
            </div>                    
        </div>
    </div>

<?php }else{ 



    if(!empty($_GET['acao']) && ($_GET['acao'] == 'excluir')){
        $id = $_GET['id'];

        $del = Connection::getInstance()->prepare("DELETE FROM home_banners WHERE id = :id");
        $del->bindValue(":id", $id);
        $del->execute();

        sucesso();
    }

?>
<div class="col-lg-12">
    <?php if(isset($_GET['sucesso'])):?>
        <div class="alert alert-success">Operação realizada com sucesso.</div>
    <?php endif; ?>

    <?php
    $select = Connection::getInstance()->query("SELECT * FROM suporte ORDER BY ordem asc")->fetchAll();
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                   <h4>Lista de Perguntas e Respostas</h4>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-6">
                   
                    <a href="?link=<?= $_GET['link']?>&acao=adicionar"><button type="button" class="btn btn-md btn-success pull-right">Adicionar</button></a>
                   
                </div>
                
            </div>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                    <thead>
                        <tr>
                            <th width="60">Categoria</th>
                            <th width="90">Ordem</th>
                            <th width="60">Pergunta</th>
                            <th width="90">Resposta</th>
                    
                            <th width="100">Ativo</th>
                            <th width="100">Ações</th>

                           
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // select esta acima

                        foreach ($select as $key => $s) {
                            $ativo = ($s["ativo"] == 1) ? 'Sim' : 'Não';
                    ?>
                        <tr class="gradeA">
                            <td class="text-center"><?= $s["categoria"]?></td>
                            <td class="text-center"><?= $s["ordem"]?></td>
                            <td class="text-center"><?= $s['pergunta'] ?> </td>
                            <td class="text-center"><?php $resposta = strip_tags($s['resposta']); echo substr($resposta, 0, 150)?></td>
                            
                            <td class="text-center"><?= $ativo?></td> 
                            <td class="text-center">

                                 <a href="?link=<?=$_GET["link"]?>&acao=editar&id=<?= $s['id']?>" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                <a href="javascript:confirma('?link=<?= $_GET["link"] ?>&acao=excluir&id=<?= $s['id']?>')"  class="btn  btn-danger "><i class="fa fa-trash fa-fw"></i></a>
    
                                <!--
                                <a href="?link=< ?= $_GET['link']?>&acao=editar&id=< ?= $s['id']?>">
                                    <button type="button" class="btn btn-sm btn-primary">Editar</button>
                                </a> 
                                <a href="javascript:confirma('?link=< ?= $_GET['link']?>&acao=excluir&id=< ?= $s['id']?>')">
                                    <button type="button" class="btn btn-sm btn-danger">Excluir</button>
                                </a> -->
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>
</div>