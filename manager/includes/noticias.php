
<?php
$COLUMNS = array (
  0 => 'slug',
  1 => 'titulo',
  2 => 'data',
  3 => 'autor',
  4 => 'texto',
  5 => 'ativo',
  6 => 'resumo',
);
$TABLE = "noticias";
$PRIMARYKEY = "id_noticias";
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header font-weight-light">Notícias
        <small class="font-weight-light"><br>Notícias</small>
        </h1>
    </div>
</div>
<?php
    if(isset($_GET["acao"])){
$body = array();
if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST"){
   $body = array_intersect_key($_POST, array_flip($COLUMNS));
   $body["slug"] = limpaUrl($body["titulo"]);

    $body["data"] = (!empty($body["data"]) ? $body["data"] : "00/00/0000");
    $body["data"] = explode("/", $body["data"]);
    $body["data"] = $body["data"][2]."-".$body["data"][1]."-".$body["data"][0];
$body["ativo"] = (!empty($body["ativo"]) ? $body["ativo"] : 0);
}
foreach($COLUMNS as $c){
    if(!isset($body[$c])) $body[$c] = "";
}
if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST"){
    if($_GET["acao"] == "adicionar"){
        $acao = Connection::getInstance()->prepare("INSERT INTO {$TABLE} (slug, titulo, data, autor, texto, ativo, resumo) VALUES (:slug, :titulo, :data, :autor, :texto,  :ativo, :resumo)");
        try{
            $acao->execute($body);
            $body[$PRIMARYKEY] = Connection::getInstance()->lastInsertId();
            echo '<div class="alert alert-success">Cadastro do(a) <a class="btn btn-secondary btn-sm" href="index.php?link='.$_GET["link"].'&acao=editar&'.$PRIMARYKEY.'='.$body[$PRIMARYKEY].'">'.$body["titulo"].'</a> realizado com sucesso</div>';
        }catch(Exception $e){
            echo '<div class="alert alert-danger">Não foi possível adicionar o(a) <span title="'. str_replace("\"", "'", $e->getMessage()) .'">'.$body["titulo"].'</span></div>';
            $inser_error = true;
        }
    }else{
        $acao = Connection::getInstance()->prepare("UPDATE {$TABLE} SET slug = :slug, titulo = :titulo, data = :data, autor = :autor, texto = :texto , ativo = :ativo, resumo = :resumo WHERE {$PRIMARYKEY} = :{$PRIMARYKEY}");
        try{
            $body[$PRIMARYKEY] = $_GET[$PRIMARYKEY];
            $acao->execute($body);
            echo "<div class='alert alert-success'><b>{$body['titulo']}</b> editado com sucesso</div>";
        }catch(Exception $e){
            echo '<div class="alert alert-danger">Não foi possível editar o(a) <span title="'. str_replace("\"", "'", $e->getMessage()) .'">'.$body["titulo"].'</span></div>';
        }
    }

    require_once("class/Upload.class.php");
    $folder = "uploads/".$TABLE."/".$body[$PRIMARYKEY]."/";
    if(isset($_FILES["imagem"]["tmp_name"])){
        $handle = new upload($_FILES["imagem"], "pt_BR");
        if($handle->uploaded){
            $handle->allowed = array("image/*");
            $handle->image_resize         = true;
            $handle->image_x              = 1400;
            $handle->image_ratio_y        = true;
            $handle->image_no_enlarging   = true;
            $handle->process(__DIR__."/../../".$folder);
            if($handle->processed){
                $acao = Connection::getInstance()->prepare("UPDATE {$TABLE} SET imagem = ? WHERE {$PRIMARYKEY} = ?");
                $acao->execute(array($folder.$handle->file_dst_name, $body[$PRIMARYKEY])); 
            }else{
                echo "<div class='alert alert-danger'><b>Falha ao fazer o upload do arquivo <u>". $handle->file_src_name  ."</u></b> ".$handle->error."</div>";
            }
        }
    }


}

if($_GET["acao"] == "adicionar"){
    //Limpa o $body se esta adicionando e se foi salvo no banco
    if(!isset($insert_error)){
        foreach($COLUMNS as $c) $body[$c] = "";
        $body["ativo"] = "1";
    }
    $panelHeading = "Adicionar <b>Notícia</b>";
}else{

    if(isset($_GET["excluir_imagem"])){
        $select = Connection::getInstance()->prepare("SELECT imagem FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
        $select->execute(array($_GET[$PRIMARYKEY]));
        $imagem = $select->fetch();
        @unlink("../" . $imagem["imagem"]);
    
        $update = Connection::getInstance()->prepare("UPDATE {$TABLE} SET imagem = ''  WHERE {$PRIMARYKEY} = ?");
        $update->execute(array($_GET[$PRIMARYKEY]));
        operacao();
    }
    $body = Connection::getInstance()->prepare("SELECT * FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
    $body->execute(array($_GET[$PRIMARYKEY]));
    $body = $body->fetch();
    if(!$body){
        die("<script>alert('Registro não encontrado'); window.location.href = 'index.php?link={$_GET["link"]}'</script>");
    }
    $panelHeading = "Editando <b>".$body["titulo"]."</b>";
}
?>
<div class="row">
    <div class="col-lg-12">
    <div class="panel card panel-default">
        <div class="panel-heading card-header"><?=$panelHeading?>
        <a class="btn btn-secondary btn-sm pull-right float-right" href="index.php?link=<?=$_GET['link']?>">voltar</a>
</div>
<div class="panel-body card-body">
                <form method="post" class="row" role="form" enctype="multipart/form-data">
                
                <div class="form-group col-xs-12 col-12">
                    <label>Titulo<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                    <input required class="form-control  " type="text" name="titulo" value="<?=$body["titulo"]?>">
                    
                </div>
                <div class="form-group col-xs-12 col-12">
                    <label>Resumo</label>
                    <textarea class="form-control  "  name="resumo" rows="2" maxlength="300"><?=$body["resumo"]?></textarea>
                    
                </div>
                
                
                <div class="form-group col-md-6">
                        <label>Data<u title='Esse campo é obrigatório' class='text-danger'>*</u></label>
                        <div class="input-group date datepicker-bootstrap-data">
                            <input type="text" class="form-control  " name="data" value="<?=!empty($body["data"]) ? date("d/m/Y", strtotime($body["data"])) : date("d/m/Y");?>" required>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        
                    </div>
                
                
                <div class="form-group col-md-6">
                    <label>Autor</label>
                    <input  class="form-control  " type="text" name="autor" value="<?=$body["autor"]?>">
                    
                </div>
                
                
                <div class="form-group col-xs-12 col-12" >
                    <label>Texto</label>
                    <textarea  class="form-control tem-ckeditor  " name="texto" id="texto"><?=$body["texto"]?></textarea>
                    <script>CKEDITOR.replace("texto", {height: "200px"});</script>
                    
                </div>
                
                
                <div class="form-group col-xs-12 col-12">
                <label>Imagem</label>
                    <?php if(!empty($body["imagem"])){ ?>
                        <img src="../<?= $body["imagem"]; ?>" class="img-responsive">
                        <a href="?link=<?= $_GET["link"] ?>&<?=$PRIMARYKEY?>=<?= $_GET[$PRIMARYKEY] ?>&acao=<?=$_GET["acao"] ?>&excluir_imagem"  class="btn  btn-danger"><i class="fa fa-fw fa-trash"></i></a>
                    <?php }else{ ?>
                        <input type="file"  class="form-control " name="imagem" >
                   <?php } ?>
                   
                </div>
                
                
                <div class="form-group col-xs-12 col-12">
                    <label>Ativo</label><br>
                    <label class="switch ">
                    <input  type="checkbox" value="1" <?=$body["ativo"] ? "checked" : ""?> name="ativo" class="form-control  ">
                    <span class="slider"></span>
                    </label>
                    
                </div>
                    <div class="row"></div>
                    <button type="submit" class="btn btn-primary"><?=$_GET["acao"] === "adicionar" ? "Adicionar" : "Salvar"?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
}else{
if(!empty($_GET["excluir"])){
    $del = Connection::getInstance()->prepare("DELETE FROM {$TABLE} WHERE {$PRIMARYKEY} = ?");
    $del->execute(array($_GET["excluir"]));
}

$results = Connection::getInstance()->query("SELECT * FROM {$TABLE}")->fetchAll();
$conta_results = count($results);
?>
    <div class="panel card panel-default">
        <div class="panel-heading card-header">
                Listagem de Notícias
                <a href="?link=<?= $_GET["link"]?>&acao=adicionar" class="btn btn-sm btn-success pull-right float-right">Adicionar</a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body card-body">
            <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                <thead>
                    <tr>
                        <th >Data</th>
                        <th width="60">Ativo</th>
                        <th >Titulo</th>
                        <th >Autor</th>
                        <th >Imagem</th>
                        <th width="100">Ações</th>
                    </tr>
                </thead>
            <tbody>
            <?php
                foreach ($results as $t) {
            ?>
                <tr class="gradeA">
                    <td >
                        <?=$t["data"] ? "<span style='display:none;'>".$t["data"]."</span>".date("d/m/Y", strtotime($t["data"])) : "---"?>
                    </td>
                    <td >
                        <span style="display:none;"><?=$t["ativo"]?></span>
                        <?=($t["ativo"] ? "Sim" : "Não");?>
                    </td>
                    <td >
                       <?=$t["titulo"]?>
                    </td>
                    <td >
                       <?=$t["autor"]?>
                    </td>
                    <td >
                        <?=$t["imagem"] ? "<img style='width:75px;height:auto;' src='../".$t["imagem"]."'>" : "---"?>
                    </td>
                    <td>
                        <a href="?link=<?=$_GET["link"]?>&acao=editar&<?=$PRIMARYKEY ?>=<?=$t[$PRIMARYKEY]?>" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                  <a href="javascript:confirma('?link=<?= $_GET["link"] ?>&excluir=<?=$t[$PRIMARYKEY]?>')"  class="btn  btn-danger "><i class="fa fa-trash fa-fw"></i></a>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->

<?php
}
