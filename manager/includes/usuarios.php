<?php
    if(!empty($_GET['acao'])){
        if($_GET['acao'] == 'verifica_usuarios' && (!empty($_SERVER['HTTP_X_REQUESTED_WITH']))){
            require_once('../class/Login.class.php');
            require_once('../class/functions.php');
            $objLogin = new Login();
            $objLogin->verificarLogado();

            extract($_POST);
            $verifica = Connection::getInstance()->prepare("SELECT usuario FROM usuarios WHERE usuario = :usuario AND id != :id");
            $verifica->bindValue(":usuario", $usuario);
            $verifica->bindValue(":id", $id);
            $verifica->execute();
            $verifica = $verifica->fetch();
            if($verifica['usuario'] == $usuario){
                echo 'false';
            }else{
                echo 'true';
            }
            exit();
        }
    }
    

    if($_POST){
        extract($_POST);
        $permissoes_db = '';
        if($modulo != ''){
            foreach ($modulo as $key => $p) {
                $permissoes_db .= $p.',';
            }
        }
        if(($senha != '' && $senha2 != '') || ($senha_edit != '' && $senha2_edit != '')){
            $troca_senha = ", senha = :senha";
        }

        $ativo = ($ativo == 1) ? 1 : 0;
        if($_GET['acao'] == 'editar'){
            $id = $_GET['id'];
            
            $acao = Connection::getInstance()->prepare("UPDATE usuarios SET nome = :nome, usuario = :usuario, email = :email, permissoes = :permissoes_db, ativo = :ativo ".$troca_senha." WHERE id = :id");
            $acao->bindValue(":id", $id);
        }else if($_GET['acao'] == 'adicionar'){
            $acao = Connection::getInstance()->prepare("INSERT INTO usuarios (nome, usuario, email, senha, permissoes, ultimo_acesso, ativo) VALUES (:nome, :usuario, :email, :senha, :permissoes_db, null, :ativo)");
        }
        $acao->bindValue(":nome", $nome);
        $acao->bindValue(":usuario", $usuario);
        $acao->bindValue(":email", $email);
        $acao->bindValue(":permissoes_db", $permissoes_db);
        $acao->bindValue(":ativo", $ativo);
        if(($senha != '' && $senha2 != '') || ($senha_edit != '' && $senha2_edit != '')){
            if($senha_edit != '' && $senha2_edit != ''){
                $senha = $senha_edit;
            }
            $senha = md5($senha);
            $acao->bindValue(":senha", $senha);
        }
        if($acao->execute()){
            sucesso();
        }else{
            error();
        }
    }

    if(!empty($_GET['id'])){
        $id = $_GET['id'];
        $usuario = Connection::getInstance()->prepare("SELECT * FROM usuarios WHERE id = :id");
        $usuario->bindValue(":id", $id);
        $usuario->execute();
        $usuario = $usuario->fetch();
    }

    $modulos = Connection::getInstance()->query("SELECT * FROM modulos ORDER BY id ASC");
    $modulos->execute();
    $modulos = $modulos->fetchAll();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Usuários</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
<?php if(!empty($_GET['acao']) && ($_GET['acao'] == 'adicionar' || $_GET['acao'] == 'editar')){ ?>
    <div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastro de usuário
            </div>   
            <div class="panel-body">
                <form method="post" id="perfil_cadastro" role="form">
                    <div class="form-group">
                        <label>Nome</label>                        
                        <input class="form-control" name="nome" id="nome" value="<?= (!empty($usuario['nome'])) ? $usuario["nome"]:''?>" autofocus>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input class="form-control" name="email" id="email" value="<?= (!empty($usuario['email'])) ? $usuario["email"]:''?>">
                    </div>
                    <div class="form-group">
                        <label>Usuário</label>
                        <input class="form-control" name="usuario" id="usuario" value="<?= (!empty($usuario['usuario'])) ? $usuario["usuario"]:''?>">
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Senha</label>
                        <input class="form-control" type="password" name="<?php if($_GET['acao'] == 'editar'){echo 'senha_edit';}else{echo 'senha';}?>" id="<?php if($_GET['acao'] == 'editar'){echo 'senha_edit';}else{echo 'senha';}?>">
                    </div>
                    <div class="form-group">
                        <label>Confirmar Senha</label>
                        <input class="form-control" type="password" name="<?php if($_GET['acao'] == 'editar'){echo 'senha2_edit';}else{echo 'senha2';}?>" id="<?php if($_GET['acao'] == 'editar'){echo 'senha2_edit';}else{echo 'senha2';}?>">
                        <?php if($_GET['acao'] == 'editar'){echo '<p class="help-block">* Caso não queira alterar a senha, deixe os campos em branco.</p>';}?>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Permissões</label>
                        <?php
                            if(!empty($usuario['permissoes'])){
                                $modulos_permitidos = explode(',', $usuario['permissoes']);
                            }
                            foreach($modulos as $key => $m){
                                $checked = "";
                                if(!empty($usuario['permissoes'])){
                                    $checked = (in_array($m['id'], $modulos_permitidos)) ? 'checked="checked"' : '';
                                }
                        ?>
                            <div class="checkbox">
                                <label class="nopadding">
                                <input type="checkbox" class="flat" name="modulo[]" <?php echo $checked;?> value="<?php echo $m['id']?>"> <?php echo $m['nome']?></label>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Status</label>
                        <div class="checkbox"><label class="nopadding"><input type="checkbox" name="ativo" class="flat" id="ativo" value="1" checked="checked"> Ativo</label></div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                    <a href="?link=<?= $_GET['link'] ?>" class="btn btn-default">Voltar</a>
                    <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']?>">
                </form>
            </div>                    
        </div>
    </div>
<?php }else{
    if(!empty($_GET['acao'])){
        if($_GET['acao'] == 'excluir'){
            $id = $_GET['id'];
            $usuario = Connection::getInstance()->prepare("DELETE FROM usuarios WHERE id = :id");
            $usuario->bindValue(":id", $id);
            $usuario->execute();

            sucesso();
        }
    }
?>
<div class="col-lg-12">
    <?php if(isset($_GET['sucesso'])):?>
        <div class="alert alert-success">Operação realizada com sucesso.</div>
    <?php endif; ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                   <h4>Lista de Usuários</h4>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <a href="?link=<?php echo $_GET['link']?>&acao=adicionar"><button type="button" class="btn btn-md btn-success pull-right">Adicionar</button></a>
                </div>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover tabela-lista" width="100%">
                    <thead>
                        <tr>
                            <th width="20">#</th>
                            <th>Nome</th>
                            <th>Usuário</th>
                            <th width="200">E-mail</th>
                            <th width="100">Último Acesso</th>
                            <th width="50">Status</th>
                            <th width="100">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $usuario = Connection::getInstance()->prepare("SELECT * FROM usuarios ORDER BY id DESC");
                        $usuario->execute();
                        $usuario = $usuario->fetchAll();

                        foreach ($usuario as $key => $u) {
                            if($u["ultimo_acesso"] == '0000-00-00 00:00:00' || empty($u["ultimo_acesso"])){
                                $data = '';
                            }else{
                                $acesso = explode(' ', $u["ultimo_acesso"]);
                                $data = date('d/m/Y', strtotime($acesso[0]));
                                $data = $data.' '.$acesso[1];
                            }
                            $ativo = ($u["ativo"] == 1) ? 'Ativo' : 'Inativo';
                            if($u["id"] == 1 && $_SESSION["id_usuario"] != $u["id"]){continue;}
                    ?>
                        <tr class="gradeA">
                            <td><?php echo $u["id"]?></td>
                            <td><?php echo $u["nome"]?></td>
                            <td><?php echo $u["usuario"]?></td>
                            <td><?php echo $u["email"]?></td>
                            <td><?php echo $data?></td>
                            <td><?php echo $ativo?></td>
                            <td width="150"><a href="?link=<?php echo $_GET['link']?>&acao=editar&id=<?php echo $u['id']?>"><button type="button" class="btn btn-sm btn-primary">Editar</button></a> <a href="javascript:confirma('?link=<?php echo $_GET['link']?>&acao=excluir&id=<?php echo $u['id']?>')"><button type="button" class="btn btn-sm btn-danger">Excluir</button></a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
<?php } ?>
</div>
<!-- /.row -->