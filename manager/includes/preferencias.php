<?php
    if($_POST){
        extract($_POST);

        $update = Connection::getInstance()->prepare("UPDATE configuracoes SET titulo = :titulo, descricao = :descricao, palavras = :palavras, adwords = :adwords, contato = :contato, contato_cc = :contato_cc WHERE id = '1'");
        $update->bindValue(":titulo", $titulo);
        $update->bindValue(":descricao", $descricao);
        $update->bindValue(":palavras", $palavras);
        $update->bindValue(":adwords", $adwords);
        $update->bindValue(":contato", $contato);
        $update->bindValue(":contato_cc", $contato_cc);
        if($update->execute()){
            sucesso();
        }else{
            error();
        }
    }

    $conf = Connection::getInstance()->prepare("SELECT * FROM configuracoes");
    $conf->execute();
    $conf = $conf->fetch();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Preferências</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="alert alert-danger" style="display:none;">Erro ao salvar! Tente novamente.</div>
<?php if(isset($_GET['sucesso'])):?>
    <div class="alert alert-success">Operação realizada com sucesso.</div>
<?php endif; ?>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Configurações do site
            </div>   
            <div class="panel-body">
                <form method="post" id="perfil" role="form">
                    <div class="form-group">
                        <label>Título do Site *</label>
                        <input class="form-control" name="titulo" id="titulo" maxlength="70" value="<?php echo $conf["titulo"]?>" placeholder="Nome do Site - Atuação - Cidade/UF" autofocus>
                        <p class="help-block">* Irá aparecer no topo do navegador e nos mecanismos de busca.</p>
                    </div>
                    <div class="form-group">
                        <label>Descrição *</label>
                        <input class="form-control" name="descricao" id="descricao" maxlength="155" value="<?php echo $conf["descricao"]?>" placeholder="Descreva a empresa ou finalidade do site">
                        <p class="help-block">* Para mecanismos de busca e redes sociais.</p>
                    </div>
                    <div class="form-group">
                        <label>Palavras-chave *</label>
                        <input class="form-control" name="palavras" id="palavras" maxlength="175" value="<?php echo $conf["palavras"]?>" placeholder="produtos, inovação, preço baixo, qualidade">
                        <p class="help-block">* Usadas pelos mecanismos de busca para encontrar seu site. Utilize palavras separadas por vírgula.</p>
                    </div>
                   <!-- <div class="form-group">
                        <label>Código Google AdWords*</label>
                        <textarea class="form-control" name="adwords" id="adwords" rows="5">< ?php echo $conf["adwords"]?></textarea>
                        <p class="help-block">* Cole o código gerado pelo Google AdWords.</p>
                    </div> -->
                    <hr>
                    <div class="form-group">
                        <label>E-mail de Contato *</label>
                        <input class="form-control" type="email" name="contato" id="contato" value="<?php echo $conf["contato"]?>" placeholder="contato@seusite.com.br">
                        <p class="help-block">* E-mail que será utilizado pelo site para envio de formulários. Não pode ser de domínio diferente do site.</p>
                    </div>
                    <div class="form-group">
                        <label>E-mails de Contato adicionais</label>
                        <input class="form-control" type="text" name="contato_cc" id="contato_cc" value="<?php echo $conf["contato_cc"]?>" placeholder="luis@gmail.com;paulo@hotmail.com;vanda@yahoo.com.br;">
                        <p class="help-block">* E-mails que terão os formulários enviados como cópia (CC). Podem ser de domínios diferentes e separados por ponto e vírgula</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>                    
        </div>
    </div>
</div>
<!-- /.row -->