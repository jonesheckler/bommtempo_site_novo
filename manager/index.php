<?php
    require_once('class/Login.class.php');
    require_once('class/functions.php');
    $objLogin = new Login();
    $objLogin->verificarLogado();
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Tiago F. Centa - tiago@upplay.com.br">

    <title>MW10 | Painel Administrativo</title>

    <link href="dist/css/images/logo.png" rel="shortcut icon" type="image/x-icon" />

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
    <link href="dist/css/timeline.css" rel="stylesheet">
    <link href="dist/css/jquery.filer.css" rel="stylesheet">
    <link href="dist/css/jquery.filer-dragdropbox-theme.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/css/jquery.fancybox.css" rel="stylesheet">
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"> -->
    <link 
    rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
    integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <link href="dist/iCheck/skins/flat/blue.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">

    <link href="dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="dist/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

    <link href="dist/css/jquery-ui.css" rel="stylesheet">
    <link href="dist/css/jquery-ui.theme.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- fancybox -->
    <script src="dist/js/jquery.fancybox.pack.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="bower_components/raphael/raphael-min.js"></script>
    <!-- <script src="bower_components/morrisjs/morris.min.js"></script>
    <script src="js/morris-data.js"></script> -->

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="bower_components/datatables-responsive/js/dataTables.responsive.js"></script>

    <script type="text/javascript" src="dist/js/bootstrap-colorpicker.min.js"></script>

    <!-- Custom Theme JavaScript -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
    <script src="dist/js/bootstrap-select.min.js"></script>

    <script src="dist/js/moment-with-locales.js"></script>

    <script src="dist/js/bootstrap-datetimepicker.js"></script>

    <script src="dist/js/jquery.validate.min.js"></script>

    <script src="dist/js/bootbox.min.js"></script>

    <script src="dist/js/jquery.filer.js"></script>

    <script src="dist/js/jquery.mask.js"></script>
    <script src="dist/js/jquery.validate.min.js"></script>

    <script src="dist/js/sb-admin-2.js"></script>

    <script src="dist/iCheck/icheck.min.js"></script>

    <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script> -->
    <script src="ckeditor/ckeditor.js"></script>

    <script src="dist/js/jquery-ui.min.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <ul class="nav navbar-top-links navbar-right">
                <li><a href="<?=$base_url?>" target="_blank">Ver Site</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nome_usuario']?>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php if(!isset($_SESSION['painel'])){ ?><li><a href="?link=perfil"><i class="fa fa-user fa-fw"></i> Perfil</a></li>
                        <li class="divider"></li>
                        <?php } ?>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>
        </nav>

        <div class="navbar-inverse sidebar" role="navigation">
            
                <div class="topMenu">
                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.php">
                        <img src="dist/css/images/logo.png" />
                    </a>
                    <h3>
                        Painel Administrativo
                        <small>MW10</small>
                    </h3>
                   
                </div>
            

            <div class="clearfix"></div>

            <div class="sidebar-nav navbar-collapse">
                <ul class="nav side-menu">
                    <li>
                        <a href="?link=noticias"><i class="fa fa-newspaper"></i>Notícias</a>
                    </li>
                    <?php if(permissao(6)){ ?>
                        <li>
                            <a href="?link=home_banners"><i class="fa fa-image"></i>Banners</a>
                        </li>
                    <?php }if(permissao(11)){ ?>
                    <li>
                        <a href="?link=botoes"><i class="fa fa-check-circle"></i>Botões</a>
                    </li>    
                    <?php }if(permissao(2)){ ?>
                        <li>
                            <a href="?link=sobre"><i class="fa fa-home"></i>Sobre</a>
                        </li>
                    <?php }if(permissao(3)){ ?>
                        <li>
                            <a href="?link=cidades"><i class="fa fa-map"></i>Cidades</a>
                        </li>
                    <?php }if(permissao(5)){ ?>
                        <li>
                            <a href="?link=mensagens_site"><i class="fa fa-comment"></i>Mensagens via Site</a>
                        </li>
                    <?php }if(permissao(5)){ ?>
                        <li>
                            <a href="?link=suporte"><i class="fa fa-question"></i>Suporte</a>
                        </li>
                    <?php }if(permissao(4)){ ?>
                        <li>
                            <li class="sub-menu">
                            <a href="#"><i class="fa fa-bullhorn"></i>Planos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="?link=planos_sistema_seguranca"><i class="fa fa-lock"></i>Sistema Segurança</a></li>
                                <li><a href="?link=planos_hospedagem"><i class="fa fa-server"></i>Hospedagem</a></li>
                                <li><a href="?link=planos_internet_empresa"><i class="fa fa-id-card"></i>Internet Para Sua Empresa</a></li>
                                <li><a href="?link=planos_internet_voce"><i class="fa fa-wifi"></i>Internet para Você</a></li>
                            </ul>
                        </li>
                        </li>
    
                    <?php }if(permissao(1)){ ?>
                        <li class="sub-menu">
                            <a href="#"><i class="fa fa-cogs"></i>Configurações<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="?link=preferencias"><i class="fa fa-globe-americas"></i>Geral</a></li>
                                <li><a href="?link=contato"><i class="fa fa-address-card"></i>Contato</a></li>
                                <li><a href="?link=telefones"><i class="fa fa-phone"></i>Telefones</a></li>
                                <li><a href="?link=usuarios"><i class="fa fa-users"></i>Usuários</a></li>
                                <li><a href="?link=motivos"><i class="fa fa-list-ol"></i>Motivos</a></li>
                                <li><a href="?link=painel_administrativo"><i class="fa fa-user-lock"></i>Painel Administrativo</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <div id="page-wrapper">
        <?php
            $url = (isset($_GET['link'])) ? $_GET['link'] : "home";
            
            $file = "includes/$url.php";
            if(file_exists($file)){
                include($file);
            }else{
                include("includes/404.php");;
            }
        ?>
        </div>
    </div>
</body>
</html>