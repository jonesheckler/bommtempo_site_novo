$(function() {
    $('.side-menu').metisMenu();

    $('.fundo-color').colorpicker();

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
    };

    $('.telefone').mask(SPMaskBehavior, spOptions);

    var SPMaskBehavior2 = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '+00 00 00000-0000' : '+00 00 0000-00009';
    },
    spOptions2 = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior2.apply({}, arguments), options);
      }
    };

    $('.telefone2').mask(SPMaskBehavior2, spOptions2);

    $('.price').mask("R$ 999,99");
    $('.cep').mask("99.999-999");
    $('.hour').mask("99:99:99");
    $('.data').mask("99/99/9999");

    $('.longitude').mask("#99.9999999",{
        reverse: true,
        translation: {
            '#': {
              pattern: /-|\d/,
              recursive: true
            }
          },
          onChange: function(value, e) {      
            e.target.value = value.replace(/(?!^)-/g, '').replace(/^,/, '').replace(/^-,/, '-');
          }
    });
    $('.latitude').mask("#99.9999999",{
        reverse: true,
        translation: {
            '#': {
              pattern: /-|\d/,
              recursive: true
            }
          },
          onChange: function(value, e) {      
            e.target.value = value.replace(/(?!^)-/g, '').replace(/^,/, '').replace(/^-,/, '-');
          }
    });

    $('a.fancybox').fancybox();

    // $('.summernote').summernote({
    //     callbacks: {
    //         onImageUpload: function(files) {
    //             for (i = 0; i < files.length; ++i) {
    //                 if(files[i].size < 2000000){
    //                     uploadSummernote(files[i], this);
    //                 }else{
    //                     alert('ERRO!!!\nArquivo '+files[i].name+' maior que 2mb!')
    //                 }
    //             }
    //         }
    //     }
    // });

    // $('.alert.alert-success').slideDown('slow').delay(5000).slideUp('slow');

    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    }
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    $("#login").validate({
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        },
        rules: {
            usuario: "required",
            senha: "required"
        },
        messages: {  
           
        }
    });
    $("#perfil").validate({
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        },
        rules: {
            nome: "required",
            email: "required",
            usuario: {
                required: true,
                minlength: 2
            },
            senha: {
                equalTo: "#senha2"
            },
            senha2: {
                equalTo: "#senha"
            }
        },
        messages: {  
            usuario: {
                minlength: "Campo deve possuir no mínimo 2 caracteres"
            },
            senha: {
                equalTo: "As senhas não coincidem"
            },
            senha2: {
                equalTo: "As senhas não coincidem"
            }
        }
    });
    $("#perfil_cadastro").validate({
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        },
        rules: {
            nome: "required",
            email: "required",
            usuario: {
                required: true,
                minlength: 2,
                remote: {
                    url: "includes/usuarios.php?acao=verifica_usuarios",
                    type: "post",
                    data: {
                      usuario: function() {
                        return $( "#usuario" ).val();
                      },
                      id: function() {
                        return $( "#id" ).val();
                      }
                    }
                }
            },
            senha: {
                required: true,
                equalTo: "#senha2"
            },
            senha2: {
                required: true,
                equalTo: "#senha"
            },
            senha_edit: {
                equalTo: "#senha2_edit"
            },
            senha2_edit: {
                equalTo: "#senha_edit"
            }
        },
        messages: {  
            usuario: {
                minlength: "Campo deve possuir no mínimo 2 caracteres",
                remote: "Usuário já está em uso"
            },
            senha: {
                equalTo: "As senhas não coincidem"
            },
            senha2: {
                equalTo: "As senhas não coincidem"
            },
            senha_edit: {
                equalTo: "As senhas não coincidem"
            },
            senha2_edit: {
                equalTo: "As senhas não coincidem"
            }
        }
    });
    $('.tabela-lista').DataTable({
        responsive: true,
        order: [[ 0, "desc" ]]
    });

    $('.datepicker-bootstrap').datetimepicker({
        locale: 'pt',
    });

    $('.datepicker-bootstrap-data').datetimepicker({
        locale: 'pt',
        format: 'DD/MM/YYYY'

    });
});

function confirma(href,param = null){
    var texto = "Deseja realmente executar esta ação?";
    if(param){
        texto = "Ao excluir esse registro irá desencandear na exclusão de outros registro ligados a ele.<br>"+texto;
    }

    bootbox.confirm(texto, function(result) {
        if(result === true){
            window.location = href;
        }
    });
}

function uploader(folder, id, tabela){
    $('#arquivo').filer({
        showThumbs: true,
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li>{{fi-progressBar}}</li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: true,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        uploadFile: {
            url: "class/Ajax.Upload.class.php",

            data: {'id': id, 'folder': folder, 'tabela': tabela},
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function(){},
            success: function(data, el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Imagem enviada</div>").hide().appendTo(parent).fadeIn("slow");    
                });
            },
            error: function(el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Erro ao enviar</div>").hide().appendTo(parent).fadeIn("slow");    
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: function(){document.location.href = document.location.href;}
        }
    });
}

function uploadSummernote(file, el) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url: "class/functions.php",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            baseUrl = document.location.href.match(/^.*\//)[0]
            $(el).summernote('editor.insertImage', baseUrl+'../'+url);
        }
    });
}

function changeStatus(id,table, element){
    $.ajax({
        type: 'GET',
        url: 'class/functions.php',
        data: {id: id,table: table,ajax: true}
    }).done(function(result){
        var notifi = $('.side-menu .notifi');
        var pedidos = parseInt(notifi.html());
        if (result == 1) {
            $(element).html('Entregue').removeClass("btn-danger").addClass("btn-info");   
            pedidos = pedidos - 1;
            notifi.html(pedidos);
            if(pedidos <= 0){
                notifi.hide();
            }
        }else{
            $(element).html('Pendente').removeClass("btn-info").addClass("btn-danger");

            if(pedidos <= 0 || notifi.length == 0){
                pedidos = 1;
                $('.side-menu .active').html('<i class="fa fa-cart-plus"></i>Pedidos<span class="notifi">'+pedidos+'</span>');
            }else{
                notifi.html(pedidos + 1); 
            }
        }
    });
}

function setDestaque(id, table, element){
    $.ajax({
        type: 'GET',
        url: 'class/functions.php',
        data: {id: id,table: table,ajax: true}
    }).done(function(result){
        if (result == 1) {
            $(element).removeClass("btn-dark").addClass("btn-info");    
        }else{
            $(element).removeClass("btn-info").addClass("btn-dark");
        }
    });
}