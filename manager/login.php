<?php 
    require_once(dirname(__FILE__).'/class/Login.class.php');
 
    $objConnection = Connection::getInstance();
    $objLogin = new Login();

    if(!empty($_POST["usuario"]) && !empty($_POST["senha"]) && !empty($_GET['painel'])){ 
        $logar = $objLogin->Logar2($_POST["usuario"],$_POST['senha'],$_GET['painel']);
    }

    if(!empty($_POST["usuario"]) && !empty($_POST["senha"])){ 
        $logar = $objLogin->Logar($_POST["usuario"],$_POST['senha']);
    }

    $painel = Connection::getInstance()->query("SELECT * FROM painel_administrativo")->fetch();

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MW10 | Painel Administrativo</title>

    <link href="dist/css/images/logo.png" rel="shortcut icon" type="image/x-icon" />

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif] -->

</head>

<body style="background-color: <?=$painel["cor_primaria"] ?> " > <!-- #993be0; -->

  <div class="login" style="background-image: url(<?=!empty($painel["imagem_login"]) ? '../'.$painel["imagem_login"].'' : 'dist/css/images/duo.jpg' ?>)">
    </div> 

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img src=" <?=!empty($painel["imagem_logo"]) ? '../'.$painel["imagem_logo"].'' : 'dist/css/images/logo.png' ?> " />
                        <h3 class="panel-title">Painel Administrativo</h3>
                    </div>
                    <div class="panel-body">
                        <form name="login" id="login" method="post" role="form">
                            <fieldset>
                                <?php if(isset($logar)){ ?> 
                                <div class="alert alert-warning">
                                    <?php print_r($logar); ?>
                                </div>
                                <?php } ?>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuário" name="usuario" type="usuario" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="senha" type="password" value="">
                                </div>
                                <!-- <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div -->
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/jquery.validate.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
