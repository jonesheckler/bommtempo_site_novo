$(document).ready( function(){
	if($('body').width() < 701) {
		$(window).scroll(function(){
	        if ($(window).scrollTop() > 20){$('header').addClass('active');}
	        if ($(window).scrollTop() > 20){$('header').css('top','0px');}
	        if ($(window).scrollTop() < 20 && $(window).scrollTop() > 401){$('header').css('top','-100px');}
	        if ($(window).scrollTop() < 10){$('header').removeClass('active');}
	        if ($(window).scrollTop() < 15){$('header').css('top','0px');}
	    });
	}	

    
    
    

	var modalBairros = $('.modalBairros').not('.modalCaracteristicas').not('.modalTipoImovel');
    var modalCarac = $('.modalBairros.modalCaracteristicas');
    var modalTipo = $('.modalBairros.modalTipoImovel');

	$('.abreBairros').on('click', function(){
		modalBairros.addClass('open');
	});
    
	$('.modalBairros .close').on('click', function(){
        modalBairros.removeClass('open');	
    });
    $('.modalBairros .bg-black').click(function(){
        modalBairros.removeClass('open')
    })

	$('.abreCaracteristicas').on('click', function(){
        modalBairros.removeClass('open');
        modalCarac.addClass('open');
	});
	$('.modalCaracteristicas .close').on('click', function(){
		modalCarac.removeClass('open');	
    });
    $('.modalCaracteristicas .bg-black').click(function(){
        modalCarac.removeClass('open')
    })

	$('.abreTipoImovel').on('click', function(){
        modalBairros.removeClass('open');
        modalCarac.removeClass('open');
        modalTipo.addClass('open');
	});
	$('.modalTipoImovel .close').on('click', function(){
		modalTipo.removeClass('open');	
    });
    $('.modalTipoImovel .bg-black').click(function(){
        modalTipo.removeClass('open')
    });

    
    $('.formBusca').submit(function(ev){
        var tipo_imovel = $('[name=tipo-cadastro]:checked');
        console.log('estou aqui')
        if(!tipo_imovel.length){
            alert('Você deve selecionar se o imóvel é "aluguel" ou "venda"');
            ev.preventDefault();
        }
    })

	// carousels
	$('#owl-banner').owlCarousel({
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
        transitionStyle: 'goDown',
        autoPlay: true,
        rewindSpeed : 5000
	});

	$('#owl-destaques-venda').owlCarousel({
		navigation: true,
		navigationText: ["",""],
		pagination: false,
		slideSpeed : 300,
		paginationSpeed : 400,
		items: 3,
		lazyLoad: true
	})

	$('#owl-destaques-aluguel').owlCarousel({
		navigation: true,
		navigationText: ["",""],
		pagination: false,
		slideSpeed : 300,
		paginationSpeed : 400,
		items: 3,
		lazyLoad: true
	})

	$('#owl-noticias-destaque').owlCarousel({
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		navigation: true,
		pagination: false,
		navigationText: ["",""]
	});

	

	// sliders

	$('.slider').jRange({
	    from: 0,
	    to: 4,	
	    step: 1,
	    scale: [0,1,2,3,'mais'],
	    format: '%s',
	    width: 180,
	    showLabels: false,
	    snap: true
	});
	$('.slider-area').jRange({
	    from: 0,
	    to: 500,
	    step: 1,
	    scale: [0,'500m²+'],
	    format: '%s',
	    width: 180,
	    showLabels: true,
	    isRange : true
	});
	$('.slider-valor').jRange({
	    from: 0,
	    to: 1500,
	    step: 1,
	    scale: [0, '1.5mi+'],
	    format: '%s',
	    width: 180,
	    showLabels: true,
	    isRange : true
    });
 

	// modal
	$('#tenho-interesse').on('click', function(ev) {
		ev.preventDefault();
		$('.modal').toggleClass('open');	
		console.log('ok')
    });
    
    $('#interesse').on('click', function(ev) {
		ev.preventDefault();
		$('.modal').toggleClass('open');	
		console.log('ok')
	});

	$('.modal .bg-black').on('click', function(ev) {
		ev.preventDefault();
		$('.modal').toggleClass('open');	
	});


	// flickity

	var $carousel = $('.carousel').flickity({
	  imagesLoaded: true,
	  percentPosition: false,
	  initialIndex: 1,
	  pageDots: false
	});

	var $imgs = $carousel.find('.carousel-cell img');
	// get transform property
	var docStyle = document.documentElement.style;
	var transformProp = typeof docStyle.transform == 'string' ?
	  'transform' : 'WebkitTransform';
	// get Flickity instance
	var flkty = $carousel.data('flickity');

	$carousel.on( 'scroll.flickity', function() {
	  flkty.slides.forEach( function( slide, i ) {
	    var img = $imgs[i];
	    var x = ( slide.target + flkty.x ) * -1/3;
	    img.style[ transformProp ] = 'translateX(' + x  + 'px)';
	  });
	});

	// parallax 
	$('.bannerTitulo.interna').parallax({imageSrc: '../img/banner-interna.jpg'});

    jQuery(window).trigger('resize').trigger('scroll');
    


    // integração back end
    $('.tel_mask').mask("(00) 0000-0000");
    $('.cel_mask').mask("(00) 00000-0000");

    $(document).on('click', '[data-order-by]', function(){
        var orderby = $(this).attr('data-order-by');
        $.post('./includes/imoveis_orderby.php', {orderby: orderby}).always(function(){
            window.location.reload();
        });
    });

    // tenho interesse
    var enviando_tenho_interesse = false;
    $("#tenho-interesse-formulario").submit(function(e){
        var $form = $(this);
        e.preventDefault();
        if(enviando_tenho_interesse) return;
        enviando_tenho_interesse = true;
        $("#enviando-msg").slideDown();
        $.post('./contato/tenho-interesse.php', $form.serialize(), function(data){
            if(data.result !== true && data.result !== 'true'){
                console.log(data.result);
                alert('Não foi possível enviar a mensagem, por favor, tente novamente mais tarde.');
            }else{
                alert('Mensagem enviada com sucesso, entraremos em contato o mais rápido possível!');
                $form[0].reset();
                $('.modal .bg-black').trigger('click');
            }
        })
        .fail(function(){
            
            alert("Não foi possível enviar a mensagem, por favor, tente novamente mais tarde.");
        })
        .always(function(){
            $("#enviando-msg").slideUp();
            enviando_tenho_interesse = false;
        })
    });

    //adicionar aos favoritos
    var adicionando_favoritos = false;
    $(document).on('click', '[data-favoritos]', function(e){
        e.stopPropagation();
        e.preventDefault();
        if(adicionando_favoritos) return;
        adicionando_favoritos = true;

        var $favoritos_total = $('.favoritos-total');
        var favoritos_total = parseInt($($favoritos_total[0]).text().trim(),10);

        var id = $(this).attr('data-favoritos');
        console.log(id);
        var tabela = $(this).attr('data-favoritos-tipo');
        var acao;
        if($(this).hasClass('active')){
            acao = 'del';
            $(this).removeClass('active');
            favoritos_total--;
        }else{
            acao = 'add';
            $(this).addClass('active');
            favoritos_total++;
        }

        if($(this).parent().parent().parent().parent().parent().hasClass('favoritosInterna')){
            $(this).parent().parent().hide();
        }

        $favoritos_total.text(favoritos_total);

        $.get('includes/favoritos.php', {id: id, tabela: tabela, acao: acao}).always(function(){
            adicionando_favoritos = false;
        });
    });


    $("#todosBairros").change(function(){
        var is_checked = this.checked;
        modalBairros.find('input[type=checkbox]').not(this).each(function(){
            this.checked = is_checked;
        })
    });
    // $("#todoscaracteristicas").change(function(){
    //     var is_checked = this.checked;
    //     modalCarac.find('input[type=checkbox]').not(this).each(function(){
    //         this.checked = is_checked;
    //     })
    // });


    var radioAluga = $('#tipo-cadastro1');
    var camposAluga = $('.busca-aluga');
    var areaRestrita = $('.areaRestrita');
    var radioVenda = $('#tipo-cadastro2');
    var camposVenda = $('.busca-venda');
    //mostra campos de pesquisa dependendo se é aluga ou nao
    function mostraPesquisaVendaAluga(){
        if(radioAluga[0].checked){
            camposAluga.show();
          //  areaRestrita.css('visibility', 'visible');
         //   camposVenda.hide();
            $('input[name=busca]').val('aluga');
        }else{
          //  camposAluga.hide();
          //  areaRestrita.css('visibility', 'hidden');
          //  camposVenda.show();
            $('input[name=busca]').val('venda');
        }
    }
    
    radioAluga.change(mostraPesquisaVendaAluga);
    radioVenda.change(mostraPesquisaVendaAluga);
    mostraPesquisaVendaAluga();  

})


