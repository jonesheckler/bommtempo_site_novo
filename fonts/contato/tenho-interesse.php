<?php
// error_reporting(0);
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once(__DIR__."/../admin/class/Connection.class.php");
require_once(__DIR__."/send_email.fnc.php");
header("Content-type: application/json;charset=utf-8");



$EMAILS = Connection::getInstance()->query("SELECT contato, contato_cc FROM configuracoes LIMIT 1")->fetch();
$email_principal = array($EMAILS['contato']);
$email_cc = array_filter(explode(';', $EMAILS['contato_cc']));
$send_to = array_merge($email_principal, $email_cc);
//var_dump($send_to); exit();


$SEND_EMAIL = true ; // se vai enviar email usando servidor de emails

if(strtoupper($_SERVER["REQUEST_METHOD"]) === "POST"){
    $response = [];
    if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
        $response["result"] = "EMAIL_NOT_VALID";
    }else{
        $columns = array("nome", "email", "telefone", "pagina", "mensagem", "tabela", "id_imovel");
        //remoevs all unecessary data of $_POST
        foreach ($columns as $c) {
            if(!isset($_POST[$c])){
                $_POST[$c] = '';
            }
        }
        $filter_body = array_intersect_key($_POST, array_flip($columns));
        $filter_body = array_map("trim", $filter_body);
        $filter_body = array_map("strip_tags", $filter_body);
        if(isset($filter_body["mensagem"])) $filter_body["mensagem"] = nl2br($filter_body["mensagem"]);
        $body = $filter_body;
        
        $imoveis = array();
        if($body['pagina'] == 'favoritos'){
            foreach ($_SESSION['favoritos'] as $key => $ids) {
                foreach($ids as $id){
                    $imoveis[] = [
                        'tabela' => $key,
                        'id_imovel' => $id
                    ];
                }
            }
        }else{
            $imoveis[] = [
                'tabela' => $body['tabela'],
                'id_imovel' => $body['id_imovel']
            ];
        }

        $link = "";
        $imovel = "";
        
        

        $html_msg = '    <html>
                        <head>
                            <title>'.$body['nome'].'  tem interesse no imóvel - '.$imovel.' | '.EMAIL_NAME.'</title>
                        </head>

                        <body>
                            <p>'.$body["nome"].' acaba de nos enviar uma mensagem através do nosso site. Seguem os dados de contato.</p>
                            <p> 
                            <b>Nome: </b> '.$body["nome"].'<br />
                            <b>E-mail: </b> '.$body["email"].'<br />
                            <b>Telefone: </b> '.$body["telefone"].'<br />
                            <b>Mensagem: </b> <br />'.$body["mensagem"].'<br />

                            </p>
                            <p>'.$body["nome"].' está interessado nos imoveis:</p>';
                            foreach($imoveis as $imovel){
                                if($imovel['tabela'] == "classificados"){
                                    $imovel_select = Connection::getInstance()->prepare("SELECT referencia,condominio FROM classificados WHERE id_classificados = ?");
                                    $imovel_select->execute(array($imovel['id_imovel']));
                                    $imovel_select = $imovel_select->fetch(PDO::FETCH_ASSOC);
                                    $imovel_select = "Classificados: ".$imovel_select["referencia"].', '.$imovel_select["condominio"];
                                    $link = "imoveis/classificados/".$imovel['id_imovel'];
                                }elseif($imovel["tabela"] == "imb"){
                                    $imovel_select = Connection::getInstance()->prepare("SELECT CODIGOIMOVEL, EDIFICIO FROM IMB_IMOVEL WHERE CODIGOIMOVEL = ?");
                                    $imovel_select->execute(array($imovel['id_imovel']));
                                    $imovel_select = $imovel_select->fetch(PDO::FETCH_ASSOC);
                                    $imovel_select = 'Aluguel (Viasw): Cód. '.$imovel_select["CODIGOIMOVEL"].', '.$imovel_select["EDIFICIO"];
                                    $link = "imoveis/".$imovel['id_imovel'];
                                }elseif($imovel["tabela"] == "vistasoft"){
                                    $imovel_select = 'Venda (VistaSoft): Cód. '.$imovel["id_imovel"];
                                    $link = "imoveis/venda/".$imovel['id_imovel'];
                                }

                                $html_msg .= '
                                <a href="'.$base_url.'/'.$link.'">'.$imovel_select.'</a>
                                <hr>
                                ';
                                
                            }
        $html_msg .= '
        <em>Não responda a esse email, esse e-mail é automático.</em>
                        </body>
                    </html>';
        
        $mail = send_email($body['nome'].' mostrou interesse | '.EMAIL_NAME, $html_msg, $send_to, $body["email"]);
        $response["send_email"] = true;

        
        if($mail == false) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            $response["result"] = "COULD_NOT_SEND";
        } else {
            try{
                
                Connection::getInstance()->beginTransaction();
                $insert = Connection::getInstance()->prepare("INSERT INTO tenho_interesse (nome, email, telefone, mensagem) VALUES (:nome, :email, :telefone, :mensagem)");
                
                $insert->execute([
                    'nome' => $body['nome'],
                    'email' => $body['email'],
                    'telefone' => $body['telefone'],
                    'mensagem' => $body['mensagem'],
                ]);
               // var_dump($mail); exit();
                $lastInsertId = Connection::getInstance()->lastInsertId();
                
                $add_imovel = Connection::getInstance()->prepare("INSERT INTO tenho_interesse_imoveis (id_tenho_interesse, tabela, id_imovel) VALUES (:id_tenho_interesse, :tabela, :id_imovel)");
                foreach($imoveis as $imovel){
                    $add_imovel->execute([
                        'id_tenho_interesse' => $lastInsertId,
                        'tabela' => $imovel['tabela'],
                        'id_imovel' => $imovel['id_imovel']
                    ]);
                }
                
                Connection::getInstance()->commit();
                $response["result"] = true;
            }catch(Exception $e){
                Connection::getInstance()->rollBack();
                $response["result"] = false;   
            }
        }
    }
    echo json_encode($response);
}