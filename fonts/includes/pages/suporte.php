<?php
	// $suportes = Connection::getInstance()->query("SELECT * FROM suporte WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);

	 $internet = Connection::getInstance()->query("SELECT * FROM suporte WHERE ativo = 1 and categoria = 'sobre-minha-internet' ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
	 $pagamentos = Connection::getInstance()->query("SELECT * FROM suporte WHERE ativo = 1 and categoria = 'sobre-meu-pagamento' ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
	 $contratos = Connection::getInstance()->query("SELECT * FROM suporte WHERE ativo = 1 and categoria = 'sobre-meu-contrato' ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
	 $sobres = Connection::getInstance()->query("SELECT * FROM suporte WHERE ativo = 1 and categoria = 'sobre-a-bommtempo' ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
  ?>


<section class="container support">

	<div class="support-content">
		<h2 class="title-line">Suporte</h2>
		<div class="plans-title-description">
			<p>Aqui você encontrará soluções para <br>
resolver problemas simples e tirar dúvidas sobre diversos assuntos!</p>
		</div>
	
		<div class="container suport__table">

			<div class="support__item" >
				<a data-support="sobre-minha-internet" href="" title="" class="support__a js-support active">
					<figure>
						<?php include("icon-support.php"); ?>
					</figure>
					<h2 class="support__title">Sobre minha internet</h2>
				</a>
			</div>
			<div class="support__item">
				<a data-support="sobre-meu-pagamento" href="" title="" class="support__a js-support">
					<figure>
					<?php include("icon-support.php"); ?>
					</figure>
					<h2 class="support__title">Sobre meu pagamento</h2>
				</a>
			</div>
			<div class="support__item">
				<a data-support="sobre-meu-contrato" href="" title="" class="support__a js-support">
					<figure>
					<?php include("icon-support.php"); ?>
					</figure>
					<h2 class="support__title">Sobre meu contrato</h2>
				</a>
			</div>
			<div class="support__item">
				<a data-support="sobre-a-bommtempo" href="" title="" class="support__a js-support">
					<figure>
					<?php include("icon-support.php"); ?>
					</figure>
					<h2 class="support__title">Sobre a Bommtempo</h2>
				</a>
			</div>
		</div>
	</div>
		
</section>


<section  class="container support-container">
	<div class="content">


			<div class="support-cc active" id="sobre-minha-internet">
				<ul class="support-list">

					<?php	$i = 0;	
					
					foreach ($internet as $inter) {  ?>	
				

					<li class="sp-item js-accordion-parent active">
						<div class="sp-item__title js-accordion-bt <?php echo ($i == 0) ? 'opened' : 'closed'; ?>">
							<h3><?php echo $inter['pergunta']; ?></h3>
							<svg class="active-arrow" viewBox="0 0 72 51">
								<ellipse class="st0" cx="35.6" cy="26.5" rx="12.5" ry="12.5"/>
								<path class="st1" d="M40.1 25.2L35.6 30l-4.5-4.8"/>
								<path class="st2" d="M22.3 38c-6-7.1-5.7-17.4.7-24.2m24.2-.8c7.1 6.6 7.8 17.5 1.6 24.9m-33.2-3.1c-2.1-5.5-2.1-11.6.2-17.1m38.6 17.2c2.1-5.5 2.1-11.6-.2-17.1m-43 12.6c-.3-2.5-.2-5.1.2-7.6m48.4 7.7c.3-2.5.2-5.1-.2-7.6"/>
							</svg>
						</div>
						<div class="sp-item__description js-accordion">
							<p class="sp-item__text"><?php echo $inter['resposta']; ?></p>
							<div class="sp-item__container-links">
								<?php if($inter['link'] != ''){	?>
								<a class="sp-item__link go" target="_blank" href="<?php echo $inter['link']; ?>" title="Visitar Site">
									<span>Visitar Site</span>
									<svg width="20" height="20">
										<circle cx="10" cy="10" r="10"/>
										<path fill="none" stroke="#fff" stroke-width="2" d="M8.556 5.014l4.467 4.993L8.556 15"/>
									</svg>
								</a>
								<?php }		
								if(!empty($inter['documento'])){
									/*
									$j = 0;
									foreach ($internet->document as $document) {
										$j++;
										?>
										<a class="sp-item__link down" download href="<?php echo base_url() .'uploads/support_document/' . $document->file_name; ?>" title="Baixar documento">
											<span>Baixar documento <?php echo $j; ?></span>
											<svg width="20" height="20">
												<circle cx="10" cy="10" r="10"/>
												<path fill="#f6fbf4" fill-rule="evenodd" d="M12 6H8v5H6l3.995 3.994L13.989 11H12V6z"/>
											</svg>
										</a>
										<?php 
									} */
								}	?>
							</div>
						</div>
					</li>
					<?php
					$i++;
					}
				?>

				</ul>
			</div>


			<div class="support-cc" id="sobre-meu-pagamento">
				<ul class="support-list">
					<?php
					$i = 0;
					foreach ($pagamentos as $pagamento) {
						?>

				<li class="sp-item js-accordion-parent active">
					<div class="sp-item__title js-accordion-bt <?php echo ($i == 0) ? 'opened' : 'closed'; ?>">
						<h3><?php echo $pagamento['pergunta']; ?></h3>
							<svg class="active-arrow" viewBox="0 0 72 51">
								<ellipse class="st0" cx="35.6" cy="26.5" rx="12.5" ry="12.5"/>
								<path class="st1" d="M40.1 25.2L35.6 30l-4.5-4.8"/>
								<path class="st2" d="M22.3 38c-6-7.1-5.7-17.4.7-24.2m24.2-.8c7.1 6.6 7.8 17.5 1.6 24.9m-33.2-3.1c-2.1-5.5-2.1-11.6.2-17.1m38.6 17.2c2.1-5.5 2.1-11.6-.2-17.1m-43 12.6c-.3-2.5-.2-5.1.2-7.6m48.4 7.7c.3-2.5.2-5.1-.2-7.6"/>
							</svg>
					</div>
					<div class="sp-item__description js-accordion">
						<p class="sp-item__text"><?php echo $pagamento['resposta']; ?></p>
						<div class="sp-item__container-links">
							<?php if($pagamento['link'] != ''){	?>
							<a class="sp-item__link go" target="_blank" href="<?php echo $pagamento['link']; ?>" title="Visitar Site">
								<span>Visitar Site</span>
								<svg width="20" height="20">
									<circle cx="10" cy="10" r="10"/>
									<path fill="none" stroke="#fff" stroke-width="2" d="M8.556 5.014l4.467 4.993L8.556 15"/>
								</svg>
							</a>
							<?php }		
							if(!empty($pagamento['documento'])){

								/*
								$j = 0;
								foreach ($pagamento->document as $document) {
									$j++;

									?>
									<a class="sp-item__link down" download href="<?php echo base_url() .'uploads/support_document/' . $document->file_name; ?>" title="Baixar documento">
										<span>Baixar documento <?php echo $j; ?></span>
										<svg width="20" height="20">
											<circle cx="10" cy="10" r="10"/>
											<path fill="#f6fbf4" fill-rule="evenodd" d="M12 6H8v5H6l3.995 3.994L13.989 11H12V6z"/>
										</svg>
									</a>
								<?php }	*/
									}
									?>
								</div>
							</div>
						</li>
						<?php
						$i++;
					}
					?>
				</ul>
			</div>


			<div class="support-cc" id="sobre-meu-contrato">
				<ul class="support-list">
					<?php
					$i = 0;
				foreach ($contratos as $contrato) {
					?>

					<li class="sp-item js-accordion-parent active">
						<div class="sp-item__title js-accordion-bt <?php echo ($i == 0) ? 'opened' : 'closed'; ?>">
							<h3><?php echo $contrato['pergunta']; ?></h3>
							<svg class="active-arrow" viewBox="0 0 72 51">
								<ellipse class="st0" cx="35.6" cy="26.5" rx="12.5" ry="12.5"/>
								<path class="st1" d="M40.1 25.2L35.6 30l-4.5-4.8"/>
								<path class="st2" d="M22.3 38c-6-7.1-5.7-17.4.7-24.2m24.2-.8c7.1 6.6 7.8 17.5 1.6 24.9m-33.2-3.1c-2.1-5.5-2.1-11.6.2-17.1m38.6 17.2c2.1-5.5 2.1-11.6-.2-17.1m-43 12.6c-.3-2.5-.2-5.1.2-7.6m48.4 7.7c.3-2.5.2-5.1-.2-7.6"/>
							</svg>
						</div>
						<div class="sp-item__description js-accordion">
							<p class="sp-item__text"><?php echo $contrato['resposta']; ?></p>
							<div class="sp-item__container-links">
								<?php if($contrato['link'] != ''){	?>
								<a class="sp-item__link go" target="_blank" href="<?php echo $contrato['link']; ?>" title="Visitar Site">
									<span>Visitar Site</span>
									<svg width="20" height="20">
										<circle cx="10" cy="10" r="10"/>
										<path fill="none" stroke="#fff" stroke-width="2" d="M8.556 5.014l4.467 4.993L8.556 15"/>
									</svg>
								</a>
								<?php }		
								if(!empty($contrato['documento'])){
									/*
									$j = 0;
									foreach ($contrato->document as $document) {
										$j++;
										?>
										<a class="sp-item__link down" download href="<?php echo base_url() .'uploads/support_document/' . $document->file_name; ?>" title="Baixar documento">
											<span>Baixar documento <?php echo $j;?></span>
											<svg width="20" height="20">
												<circle cx="10" cy="10" r="10"/>
												<path fill="#f6fbf4" fill-rule="evenodd" d="M12 6H8v5H6l3.995 3.994L13.989 11H12V6z"/>
											</svg>
										</a>
									<?php } */
										}?>
									</div>
								</div>
							</li>
							<?php
							$i++;
					}
					?>
				</ul>
			</div>


			<div class="support-cc" id="sobre-a-bommtempo">
				<ul class="support-list">
					<?php
					$i = 0;
					foreach ($sobres as $sobre) {
						?>

						<li class="sp-item js-accordion-parent active">
							<div class="sp-item__title js-accordion-bt <?php echo ($i == 0) ? 'opened' : 'closed'; ?>">
								<h3><?php echo $sobre['pergunta']; ?></h3>
								<svg class="active-arrow" viewBox="0 0 72 51">
									<ellipse class="st0" cx="35.6" cy="26.5" rx="12.5" ry="12.5"/>
									<path class="st1" d="M40.1 25.2L35.6 30l-4.5-4.8"/>
									<path class="st2" d="M22.3 38c-6-7.1-5.7-17.4.7-24.2m24.2-.8c7.1 6.6 7.8 17.5 1.6 24.9m-33.2-3.1c-2.1-5.5-2.1-11.6.2-17.1m38.6 17.2c2.1-5.5 2.1-11.6-.2-17.1m-43 12.6c-.3-2.5-.2-5.1.2-7.6m48.4 7.7c.3-2.5.2-5.1-.2-7.6"/>
								</svg>
							</div>
							<div class="sp-item__description js-accordion">
								<p class="sp-item__text"><?php echo $sobre['resposta']; ?></p>
								<div class="sp-item__container-links">
									<?php if($sobre['link'] != ''){	?>
									<a class="sp-item__link go" target="_blank" href="<?php echo $sobre['link']; ?>" title="Visitar Site">
										<span>Visitar Site</span>
										<svg width="20" height="20">
											<circle cx="10" cy="10" r="10"/>
											<path fill="none" stroke="#fff" stroke-width="2" d="M8.556 5.014l4.467 4.993L8.556 15"/>
										</svg>
									</a>
									<?php }		
									if(!empty($sobre['documento'])){
										/*
										$j = 0;
										foreach ($sobre->document as $document) {
											$j++;

											?>
											<a class="sp-item__link down" download href="<?php echo base_url() .'uploads/support_document/' . $document->file_name; ?>" title="Baixar documento">
												<span>Baixar documento <?php echo $j; ?></span>
												<svg width="20" height="20">
													<circle cx="10" cy="10" r="10"/>
													<path fill="#f6fbf4" fill-rule="evenodd" d="M12 6H8v5H6l3.995 3.994L13.989 11H12V6z"/>
												</svg>
											</a>
										<?php }	 */
											} ?>
										</div>
									</div>
								</li>
								<?php
								$i++;
							}
							?>
						</ul>
			</div>
					

		</div>
	</section>