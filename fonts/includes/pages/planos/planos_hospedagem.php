<?php
    $itens = Connection::getInstance()->query("SELECT * FROM planos_hospedagem WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="c-plans" >		
	<div class="c-plans__inner hospedagem">
		<div class="h-title">
			<div class="h-title__content">
				<p>Espaço em Disco e banco de Dados Compatível</p>
			</div>
			<div class="h-title__content">
				<p>Suporte Técnico Especializado</p>
			</div>
			<div class="h-title__content">
				<p>Dados protegidos com Backup Periódico</p>
			</div>
		</div>
		<div class="list-plans">
			<div class="list-plans__tt">
				<ul class="description-plans">
					<li class="list-plans__title">Planos</li>
					<li class="list-plans__li flex __h-center __v-center">Email</li>
					<li class="list-plans__li flex __h-center __v-center">Banda mensal</li>
					<li class="list-plans__li flex __h-center __v-center">Espaço em disco</li>
					<li class="list-plans__li flex __h-center __v-center">Banco de dados</li>
					<li class="list-plans__li flex __h-center __v-center">FTP</li>
					<li class="list-plans__li flex __h-center __v-center">Subdomínios</li>
					<li class="list-plans__li flex __h-center __v-center">Anti-span</li>
					<li class="list-plans__li flex __h-center __v-center">Webmail</li>
					<li class="list-plans__li flex __h-center __v-center">Painel de admin</li>
					<li class="list-plans__li flex __h-center __v-center">Suporte técnico</li>
					<li class="list-plans__price">Valor</li>
					<li class="list-plans__li flex __h-center __v-center"></li>
				</ul>
			</div>

			<div class="container-list-plans">
				<div class="list-plans__cc js-list-host">

                <?php foreach ($itens as $item){ 
                     $price = explode('.', $item['valor']);
                    // var_dump($price);exit();   
                    ?>
					<div class="list-plans__item">
						<ul id="basic">
							<li class="list-plans__title"><?=$item['plano']?></li>
							<li class="list-plans__li flex __h-center __v-center"><?=$item['email']?></li>
							<li class="list-plans__li flex __h-center __v-center"><?=$item['banda_mensal']?></li>
							<li class="list-plans__li flex __h-center __v-center"><?=$item['espaco_disco']?></li>
							<li class="list-plans__li flex __h-center __v-center">
                                <?php if($item['banco_dados']){
                                    echo $item['banco_dados'];
                                }else{
                                ?>
								<span class="n"><svg viewBox="0 0 100 100"><path d="M99.45 18L81.87.41 49.74 32.55 18.13.93.55 18.52l31.61 31.61L.55 81.75l17.58 17.58 31.61-31.62 31.88 31.88L99.2 82 67.32 50.13 99.45 18z"/></svg></span>							
                                <?php } ?>    
                            </li>       
                            <li class="list-plans__li flex __h-center __v-center">
                                <?php if($item['ftp']){
                                    echo $item['ftp'];
                                }else{
                                ?>
								<span class="n"><svg viewBox="0 0 100 100"><path d="M99.45 18L81.87.41 49.74 32.55 18.13.93.55 18.52l31.61 31.61L.55 81.75l17.58 17.58 31.61-31.62 31.88 31.88L99.2 82 67.32 50.13 99.45 18z"/></svg></span>							
                                <?php } ?>    
                            </li> 
                            <li class="list-plans__li flex __h-center __v-center">
                                <?php if($item['subdominios']){
                                    echo $item['subdominios'];
                                }else{
                                ?>
								<span class="n"><svg viewBox="0 0 100 100"><path d="M99.45 18L81.87.41 49.74 32.55 18.13.93.55 18.52l31.61 31.61L.55 81.75l17.58 17.58 31.61-31.62 31.88 31.88L99.2 82 67.32 50.13 99.45 18z"/></svg></span>							
                                <?php } ?>    
                            </li> 
                            <li class="list-plans__li flex __h-center __v-center">
                                <?php if($item['anti_spam']){ ?>
                                    <span class="y"><svg viewBox="0 0 100 77.16"><path d="M36.72 77.16L0 40.44l14.01-14.01 22.71 22.71L85.86.01l14 14.01-63.14 63.14z"/></svg></span>
                                <?php }else{
                                ?>
								<span class="n"><svg viewBox="0 0 100 100"><path d="M99.45 18L81.87.41 49.74 32.55 18.13.93.55 18.52l31.61 31.61L.55 81.75l17.58 17.58 31.61-31.62 31.88 31.88L99.2 82 67.32 50.13 99.45 18z"/></svg></span>							
                                <?php } ?>    
                            </li> 
                            <li class="list-plans__li flex __h-center __v-center">
                                <?php if($item['webmail']){ ?>
                                    <span class="y"><svg viewBox="0 0 100 77.16"><path d="M36.72 77.16L0 40.44l14.01-14.01 22.71 22.71L85.86.01l14 14.01-63.14 63.14z"/></svg></span>
                                <?php }else{
                                ?>
								<span class="n"><svg viewBox="0 0 100 100"><path d="M99.45 18L81.87.41 49.74 32.55 18.13.93.55 18.52l31.61 31.61L.55 81.75l17.58 17.58 31.61-31.62 31.88 31.88L99.2 82 67.32 50.13 99.45 18z"/></svg></span>							
                                <?php } ?>    
                            </li> 
                            <li class="list-plans__li flex __h-center __v-center">
                                <?php if($item['painel_admin']){ ?>
                                    <span class="y"><svg viewBox="0 0 100 77.16"><path d="M36.72 77.16L0 40.44l14.01-14.01 22.71 22.71L85.86.01l14 14.01-63.14 63.14z"/></svg></span>
                                <?php }else{
                                ?>
								<span class="n"><svg viewBox="0 0 100 100"><path d="M99.45 18L81.87.41 49.74 32.55 18.13.93.55 18.52l31.61 31.61L.55 81.75l17.58 17.58 31.61-31.62 31.88 31.88L99.2 82 67.32 50.13 99.45 18z"/></svg></span>							
                                <?php } ?>    
                            </li> 

							<li class="list-plans__li flex __h-center __v-center"><?=$item['suporte_tecnico']?></li>
							<li class="list-plans__price">
                                <?php echo 'R$ ' . $price[0];
							    	echo ($price[1] < 1) ? '' : '<span>,'.$price[1].'</span>';
								?>
                            </li>
							<li class="list-plans__li flex __h-center __v-center">
								<div class="plan-sign">
									<a title="<?=$item['label_botao']?>" data-id="1" class="js-goform" data-text="Planos | Hospedagem ">
										<svg class="plan-sign__svg"><path d="M33.998 54C19.087 54 7 42.135 7 27.5S19.087 1 33.998 1M4.645 40.806C2.327 36.896 1 32.35 1 27.5c0-4.381 1.083-8.513 3-12.154"/></svg>
										<?=$item['label_botao']?>	<svg class="plan-sign__svg"><path d="M1.002 1C15.913 1 28 12.865 28 27.5S15.913 54 1.002 54m29.353-39.806C32.673 18.105 34 22.65 34 27.5c0 4.381-1.083 8.513-3 12.153"/></svg>
									</a>
								</div>
							</li>
						</ul>
					</div>
                <?php } ?>
			</div>
			</div>
		</div>
	</div>
</div>					
</div>
					
