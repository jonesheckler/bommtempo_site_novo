<?php
    $itens = Connection::getInstance()->query("SELECT * FROM planos_sistema_seguranca WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="c-plans">
	<div class="c-plans__inner sistema-de-seguranca">
		<div class="list-plans">

        <?php foreach ($itens as $item){ ?>
			<div class="list-plans__item">
				<ul id="basic">
					<li class="list-plans__title flex __h-center __v-center"><h2><?=$item['titulo_principal']?></h2></li>
					<li class="list-plans__li flex __h-center __v-center description">
						<p><?=$item['descricao']?></p>
					</li>
					<li class="list-plans__li">
						<div class="plan-sign">
							<a title="<?=$item['label_botao']?>" data-id="1" class="js-goform" data-text="Planos | Sistema de Segurança ">
								<svg class="plan-sign__svg"><path d="M33.998 54C19.087 54 7 42.135 7 27.5S19.087 1 33.998 1M4.645 40.806C2.327 36.896 1 32.35 1 27.5c0-4.381 1.083-8.513 3-12.154"/></svg>
								<?=$item['label_botao']?><svg class="plan-sign__svg"><path d="M1.002 1C15.913 1 28 12.865 28 27.5S15.913 54 1.002 54m29.353-39.806C32.673 18.105 34 22.65 34 27.5c0 4.381-1.083 8.513-3 12.153"/></svg>
							</a>
						</div>
					</li>
				</ul>
			</div>
						
        <?php } ?>                
		</div>
	</div>
</div>					
</div>
					

