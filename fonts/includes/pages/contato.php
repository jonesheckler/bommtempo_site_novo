<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$motivos = Connection::getInstance()->query("SELECT * FROM motivos WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
$cidades = Connection::getInstance()->query("SELECT * FROM cidades WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);

?>


<section id="contato" class="container contact">
	<div class="content">
		<div class="container-title">
			<!-- <svg class="container-title__svg" viewBox="0 0 429.2 341">
  <circle class="circle" cx="212.7" cy="172.5" r="143"/>
  <circle class="border" cx="212.7" cy="172.5" r="150"/>
  <path class="lines line-l-1" d="M158.2 0c-4.3 1.7-8.5 3.6-12.6 5.7s-8.2 4.2-12.2 6.5c-8 4.6-15.6 9.7-22.9 15.3-7.3 5.5-14.3 11.5-20.8 18-1.6 1.6-3.2 3.2-4.8 4.9l-4.6 5.1c-2.9 3.5-5.9 7-8.6 10.6-5.5 7.3-10.4 15-14.7 23l-1.6 3c-.5 1-1 2.1-1.5 3.1-1 2.1-2 4.1-2.9 6.2l-2.5 6.3c-.4 1.1-.9 2.1-1.2 3.2l-1.1 3.2c-2.9 8.6-5.2 17.4-6.9 26.4l-1 6.8-.5 3.4c-.2 1.1-.2 2.3-.3 3.4-.4 4.5-.9 9.1-.9 13.6l-.2 6.9c0 2.3.1 4.6.1 6.8s0 4.6.2 6.8l.5 6.8.3 3.4c.1 1.1.3 2.3.4 3.4l.9 6.8c.3 2.3.8 4.5 1.2 6.7s.8 4.5 1.4 6.7l1.6 6.6c.2 1.1.6 2.2.9 3.3l1 3.3 1 3.3c.3 1.1.8 2.1 1.2 3.2l2.4 6.4c3.5 8.4 7.6 16.6 12.3 24.4 4.8 7.8 10.2 15.1 16.3 22 6 6.9 12.6 13.2 19.7 19 7.1 5.8 14.6 11.1 22.6 15.7 4 2.3 8 4.5 12.2 6.5 4.1 2 8.4 3.8 12.7 5.4-8.9-2.7-17.4-6.3-25.6-10.7-8.2-4.4-16-9.4-23.4-15-14.9-11.3-27.7-25.2-37.8-40.9-5-7.9-9.4-16.2-13.1-24.8l-2.5-6.5c-.4-1.1-.9-2.2-1.2-3.3l-1.1-3.4-1.1-3.3c-.4-1.1-.7-2.2-1-3.4l-1.8-6.8c-.7-2.2-1-4.6-1.5-6.9s-1-4.6-1.3-6.9l-1-6.9c-.2-1.2-.3-2.3-.5-3.5l-.3-3.5-.6-7c-.2-2.3-.2-4.7-.3-7s-.2-4.7-.2-7l.2-7c0-4.7.6-9.3 1-14 .1-1.2.2-2.3.4-3.5l.5-3.5 1.1-6.9c1.9-9.1 4-18.3 7.2-27.1l1.2-3.3c.4-1.1.9-2.2 1.3-3.2l2.7-6.5 3-6.3c.5-1 1-2.1 1.5-3.1l1.7-3 1.7-3.1c.6-1 1.1-2 1.8-3l3.8-5.9c1.3-2 2.7-3.8 4-5.7.7-.9 1.3-1.9 2.1-2.8l2.2-2.7c2.8-3.7 5.9-7.2 9-10.7l4.8-5c1.6-1.7 3.3-3.2 5-4.8 6.8-6.3 14.1-12.2 21.7-17.6 7.6-5.3 15.5-10.2 23.7-14.5C141 6.7 149.5 3 158.2 0z"/>
  <path class="lines line-l-2" d="M76.9 30.5C59.3 49.4 45.1 71.2 35.4 94.9c-4.8 11.8-8.6 24.1-11.1 36.6s-3.9 25.2-4.1 38c-.2 12.7.8 25.5 2.9 38.1s5.5 24.9 9.9 36.9c8.9 23.9 22.4 46.2 39.4 65.7-18.5-18-33.2-40.1-43-64.3-4.9-12.1-8.5-24.7-11-37.5-2.4-12.8-3.4-25.9-3.3-39 .2-13.1 1.7-26.1 4.4-38.9 2.8-12.7 6.8-25.2 12.1-37.1 10.8-23.9 26.2-45.5 45.3-62.9z"/>
  <path class="lines line-l-3" d="M18.7 91c-4.2 13-7.6 26.1-9.9 39.4-1.1 6.7-2.1 13.3-2.6 20-.6 6.7-1 13.4-1 20.2-.1 6.7.1 13.5.6 20.2.4 6.7 1.3 13.4 2.3 20.1 2.1 13.3 5.2 26.5 9.2 39.6-5.9-12.3-10.2-25.4-13-38.9-1.5-6.7-2.4-13.5-3.1-20.4s-.9-13.7-.9-20.6c.1-6.9.5-13.8 1.3-20.6s1.9-13.6 3.5-20.3c3-13.4 7.5-26.5 13.6-38.7z"/>
  <path class="lines line-r-1" d="M296.6 11c8.2 4.3 16.1 9.2 23.7 14.5 7.6 5.4 14.9 11.2 21.7 17.6 1.7 1.6 3.4 3.2 5 4.8l4.8 5c3 3.5 6.2 7 9 10.7l2.2 2.7c.7.9 1.4 1.9 2.1 2.8 1.3 1.9 2.7 3.7 4 5.7l3.8 5.9 1.8 3 1.7 3.1 1.7 3c.6 1 1 2.1 1.6 3.1l3 6.3 2.7 6.5c.4 1.1.9 2.1 1.3 3.2l1.2 3.3c3.2 8.8 5.4 17.9 7.2 27.1l1.1 6.9.5 3.5c.2 1.1.2 2.3.4 3.5.4 4.7 1 9.3 1 14l.2 7c0 2.3-.1 4.7-.2 7s-.1 4.7-.3 7l-.6 7-.3 3.5c-.1 1.2-.3 2.3-.5 3.5l-1 6.9c-.3 2.3-.8 4.6-1.3 6.9-.5 2.3-.8 4.6-1.5 6.9l-1.8 6.8c-.3 1.1-.6 2.2-1 3.4l-1.1 3.3-1.1 3.4c-.3 1.1-.8 2.2-1.2 3.3l-2.5 6.5c-3.7 8.6-8 16.9-13.1 24.8-10.2 15.7-23 29.5-37.8 40.9-7.4 5.6-15.2 10.6-23.4 15s-16.7 8-25.6 10.7c4.3-1.5 8.6-3.3 12.7-5.4 4.2-2 8.2-4.2 12.2-6.5 7.9-4.6 15.5-9.9 22.6-15.7 7.1-5.8 13.7-12.1 19.7-19s11.5-14.2 16.3-22c4.7-7.8 8.8-15.9 12.3-24.4l2.4-6.4c.4-1.1.8-2.1 1.2-3.2l1-3.3 1-3.3c.3-1.1.7-2.2.9-3.3l1.6-6.6c.6-2.2 1-4.4 1.4-6.7s.9-4.5 1.2-6.7l.9-6.8c.1-1.1.3-2.3.4-3.4l.3-3.4.5-6.8c.2-2.3.2-4.6.2-6.8s.1-4.6.1-6.8l-.2-6.9c0-4.6-.5-9.1-.9-13.6-.1-1.1-.1-2.3-.3-3.4l-.5-3.4-1-6.8c-1.6-9-3.9-17.8-6.9-26.4l-1.1-3.2c-.3-1.1-.8-2.1-1.2-3.2l-2.5-6.3c-.9-2.1-1.9-4.1-2.9-6.2-.5-1-.9-2.1-1.5-3.1l-1.6-3c-4.3-8-9.2-15.7-14.7-23-2.7-3.6-5.7-7.2-8.6-10.6l-4.6-5.1c-1.6-1.7-3.2-3.3-4.8-4.9-6.5-6.5-13.5-12.5-20.8-18-7.3-5.6-15-10.7-22.9-15.3-4-2.3-8-4.5-12.2-6.5-4.1-2.1-8.3-4-12.6-5.7C280 3 288.5 6.7 296.6 11z"/>
  <path class="lines line-r-2" d="M352.5 30.5c17.6 18.9 31.8 40.7 41.4 64.4 4.8 11.8 8.6 24.1 11.1 36.6s3.9 25.2 4.1 38c.2 12.7-.8 25.5-2.9 38.1s-5.5 24.9-9.9 36.9c-8.9 23.9-22.4 46.2-39.4 65.7 18.5-18 33.2-40.1 43-64.3 4.9-12.1 8.5-24.7 11-37.5 2.4-12.8 3.4-25.9 3.3-39-.2-13.1-1.7-26.1-4.4-38.9-2.8-12.7-6.8-25.2-12.1-37.1-10.7-23.9-26.1-45.5-45.2-62.9z"/>
  <path class="lines line-r-3" d="M410.7 91c4.2 13 7.6 26.1 9.9 39.4 1.1 6.7 2.1 13.3 2.6 20 .6 6.7 1 13.4 1 20.2.1 6.7-.1 13.5-.6 20.2-.4 6.7-1.3 13.4-2.3 20.1-2.1 13.3-5.2 26.5-9.2 39.6 5.9-12.3 10.2-25.4 13-38.9 1.5-6.7 2.4-13.5 3.1-20.4s.9-13.7.9-20.6c-.1-6.9-.5-13.8-1.3-20.6s-1.9-13.6-3.5-20.3c-2.9-13.4-7.5-26.5-13.6-38.7z"/>
 </svg>-->			
<h1 class="title">Contato</h1>
		</div>
		<div class="content-form">
			<form class="form" method="post" class="js-form-validate" id="form-contato" enctype="multipart/form-data">
				<input type="hidden" name="ci_csrf_token" value="fb42085c07621ca8852366721e35868e">
				<input type="hidden" name="category" value="" />
				<input type="hidden" name="subcategory" value="" />

				<div class="form-title">
					<h2 class="title">Fale Conosco</h2>

					<div class="help-contact" style="display: none;">
						Você selecionou a opção <b></b>.
					</div>
					<div class="title-contact">
						Preencha o formulário abaixo e entraremos em contato com você.
					</div>

					<div class="content-input ct-mid">
						<label class="label-col">
							<input type="text" placeholder="Nome" id="nome-contato" name="name" class="input" required="required" value="">
						</label>
					</div>
					<div class="content-input ct-mid">
						<label class="label-col">
							<input type="email" placeholder="Email" id="email-contato" name="email" class="input" value="">
						</label>
					</div>
					
					<div class="content-input ct-mid">
						<label class="label-col">
							<input type="text" placeholder="Telefone" name="phone" class="input" required="required" value="" data-mask-type="phone">
						</label>

						<label class="label-col">
							<select name="type_contact" required="required" class="input select" data-parsley-errors-container="#parsley-contact-select">
								<option value="" selected>Método de contato preferido</option>
								<option value="1">Ligação</option>
								<option value="2">E-mail</option>
								<option value="3">WhatsApp</option>
							</select>
						</label>

						<label class="label-col">
							<select name="subject" required="required" id="assunto-contato"  class="input select" data-parsley-errors-container="#parsley-contact-select">
                                <option value="" selected>Motivo</option>
                                <?php foreach($motivos as $motivo){ ?>
                                    
								    <option value="<?=$motivo['id_motivo']?>"><?=$motivo['motivo']?></option>
                                <?php } ?>
                            
                                    
							</select>
						</label>
					</div>
					<div class="content-input ct-mid">
						<label class="label-col">
							<textarea name="message" placeholder="Mensagem" id="mensagem-contato" class="input textarea" value=""></textarea>
						</label>
					</div>
					<div class="content-input ct-mid">
						<label class="label-col">
							<select name="city" required="required" class="input select" data-parsley-errors-container="#parsley-contact-select">
								<option value="" selected>Cidade</option>
																	
                                    <?php foreach($cidades as $cidade){ ?>
                                        
                                        <option value="<?=$cidade['id_cidade']?>"><?=$cidade['titulo_principal']?></option>
                                    <?php } ?>
																		
									<option value="99999">Outra Cidade</option>
									
							</select>
						</label>
					</div>
					<div class="content-input js-upload" style="display: none;">
						<label class="label-col">
							<div class="upload">
								<div class="upload__inputFileOverlay">Anexe seu currículo</div>
								<input class="upload__input" type="file" name="attachment"/>
							</div>
						</label>
					</div>
					<div class="content-input">
						<label class="label-col">
							<input type="submit" value="Enviar" class="submit">
						</label>
					</div>
				</form>
			</div>
		</div>
		<div class="container container-bg">
			<figure class="bg_svg">
				<svg viewBox="0 0 67.6 67.8"><path d="M33.8 67.8c7.2 0 12.9-5.8 12.9-12.9 0-7.1-5.8-12.9-12.9-12.9-7.1 0-12.9 5.8-12.9 12.9-.1 7 5.7 12.8 12.9 12.9zm-.1 0h-.1.1zm.1-41.9c9.8 0 18.1 6.8 20.3 15.9h12.6c.6-2.6.9-5.2.9-8C67.6 15.1 52.5 0 33.8 0S0 15.1 0 33.8c0 2.7.3 5.4.9 8h12.6c2.2-9.1 10.4-15.9 20.3-15.9"/></svg>
			</figure>
		</div>
    </section>
    

<!--
    <script type="text/javascript">
    $('.telefone').mask('(00) 00000-0000');

    var form_contato = document.getElementById('form-contato');
        var pode_enviar_contato = true;
        form_contato.addEventListener("submit", function (ev) {
            ev.preventDefault();
            if (!pode_enviar_contato) {
                return;
            }
            pode_enviar_contato = false;
            var sending_contato = document.getElementById('sending-contato');
            sending_contato.innerText = "Enviando mensagem, por favor aguarde."
            var nome = document.getElementById("nome-contato").value;
            var email = document.getElementById("email-contato").value;
            var assunto = document.getElementById("assunto-contato").value;
            var telefone = document.getElementById("telefone-contato").value;

            var mensagem = document.getElementById("mensagem-contato").value;
            var data = 'nome=' + nome + 
            '&email=' + email + 
            '&telefone=' + telefone + 
            '&assunto=' + assunto +
            '&mensagem=' + mensagem; 
            var request = new XMLHttpRequest();
            request.open('POST', './contato/contato.php', true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            request.send(data);
            request.onreadystatechange = function () {
                if (request.readyState == XMLHttpRequest.DONE) {
                    sending_contato.innerText = "";
                    pode_enviar_contato = true;
                    try {
                        var response = JSON.parse(request.responseText);
                        if (response.result !== true && response.result !== 'true') {
                            if (response.result == 'MESSAGE_NOT_VALID') {
                                alert("Por favor nos escreva uma mensagem antes de enviar.")
                            } else if (response.result == 'COULD_NOT_SEND') {
                                alert(
                                    "Desculpe mas houve um erro ao enviar a mensagem, por favor tente novamente"
                                );
                            } else {
                                alert(
                                    "Desculpe mas houve um erro ao enviar a mensagem, por favor tente novamente"
                                );
                            }
                        } else {
                            alert(
                                "Sua mensagem foi enviada com sucesso, em breve entraremos em contato com você"
                            );
                            form_contato.reset();
                        }
                    } catch (e) {
                        alert("Desculpe mas houve um erro ao enviar a mensagem, por favor tente novamente")
                    }
                }
            }
        });

	</script> -->