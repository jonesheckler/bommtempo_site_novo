<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$sobre = Connection::getInstance()->query("SELECT * FROM sobre WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);

?>

<section id="sobre-nos" class="container about-us">
	<div  class="content">
		<h2 class="title-line">Sobre Nós</h2>
		<div class="container about-us__table">
			
				<?php foreach($sobre as $sob ){ ?>	
				<div class="about-us__content">
					<?php if($sob['imagem']){ ?>
						<figure class="selo">
							<img src="<?=$sob['imagem']?>" alt="<?=$sob['titulo']?>">
						</figure>
					<?php } ?>
					
					<h3><?=$sob['titulo']?></h3>
					<p><?=$sob['descricao']?></p>
				</div>

				<?php } ?>
				
			</div>
	</div>
</section>	


