<?php
$home_banners = Connection::getInstance()->query("SELECT * FROM home_banners WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
$botoes = Connection::getInstance()->query("SELECT * FROM botoes WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);




//var_dump($home_banners); exit();

?>



<main class="main home">
	<input type="hidden" id="page" data-page="#contato">
	<section class="container banner-top">
		<ul class="owl-carousel owl-theme banner">
            <?php
            foreach($home_banners as $banner){
            ?>
            <li class="banner__item" style="background-image: url(<?=$banner['banner']?>);">
                <div class="content">
                    <div class="banner__content">
                        <h1 class="banner__title">
                             <?=$banner['nome_1']?>    
                            <p><br>
                            <?=$banner['nome_2']?>  </p>				
                        </h1>
                    </div>
                </div>
            </li>
            
            <?php 
            } 
            ?>
        </ul>		
    
    
    
    
    
    <ul class="navigation-top">
			<?php
            foreach($botoes as $botao){
				
				if($botao['modal'] == 0){
				?>	

					<li class=" navigation-top__li" >
						<a href="<?=$botao['link']?>" target="_blank" class="navigation-top__tx" title="<?=$botao['nome_1']?>"><?=$botao['nome_1']?><img src="<?=$botao['banner']?>" class="icone"> </a>
					</li>
				<?php 
            	}else {
            	?>	
					<li class=" navigation-top__li status" >
						<a href="javascript:void(0)" class="navigation-top__tx js-open-modal"  title="<?=$botao['nome_1']?>" data-modal="<?=$botao['slug']?>"  ><?=$botao['nome_1']?><img src="<?=$botao['banner']?>" class="icone"></a>
					</li>

					
				<?php 
            	}
            	?>	

			<?php 
            } 
            ?>
		</ul>	
	
	</section>
    <div class="clear"></div>


	<div class="modal">
	<div class="modal__overlay"></div>


		<?php foreach($botoes as $botao){	
			if($botao['modal']){	
			?>
			<div id="<?=$botao['slug']?>" class="modal__content">
				<h2><?=$botao['nome_1']?></h2>
				<p>
					<div align="center"><?=$botao['nome_2']?></div>		</p>
			</div>
		<?php }} ?>	
	</div>




<?php
	include("planos.php");
	include("cidades.php");	
	include('sobre.php'); 
	include("contato.php"); 
?>
