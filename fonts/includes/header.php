<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">

<head>
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-62652006-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-62652006-3');
</script>


    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:locale" content="pt_BR" />
	<meta property="og:type" content="website" />
	
	<?php
	//include("includes/header_seo.php");
	$config = Connection::getInstance()->query("SELECT * FROM configuracoes WHERE id = 1")->fetch(PDO::FETCH_ASSOC);
	
    ?>
	<title><?=$config['titulo']?></title>
	<meta name="description" content="<?=$config['descricao']?>">
	<meta name="keywords" content="<?=$config["palavras"]?>" />

	<meta property="og:title" content="<?=$config['titulo']?>" />
	<meta property="og:site_name" content="<?=$config['titulo']?>" />
	<meta property="og:description" content="<?=$config['descricao']?>">


		<link rel="icon" href="https://www.bommtempo.com.br/uploads/favicon_icon/72a70e46023398b1d4595ad1d62e28c2.png" sizes="32x32">
		<!-- Touch icon for iOS 2.0+ and Android 2.1+: -->
		<link rel="apple-touch-icon-precomposed" href="https://www.bommtempo.com.br/uploads/favicon_icon/b9eff83578044074013a41a103fbfc8b.png">
		<!-- IE 10 Metro tile icon (Metro equivalent of apple-touch-icon): -->
		<meta name="msapplication-TileColor" content="#FFFFFF">
		<meta name="msapplication-TileImage" content="https://www.bommtempo.com.br/uploads/favicon_icon/2d9f9a2deb055819c8c643b0869f4cc7.png">



   



    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,300i,400,500,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">


    <link rel="stylesheet" href="css/style.css">
    <link href="img/favicon.png" rel="shortcut icon" type="image/x-icon" />
    <!--<link rel="stylesheet" href="css/stylemob.css?silvana20192" media="screen and (max-width: 700px)"> 
    
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="js/jquery.range.js"></script>
    <script type="text/javascript" src="js/carousel.js"></script>
    <script type="text/javascript" src="js/parallax.min.js"></script>
    
	<script src="js/flickity.pkgd.min.js"></script>
    <script type="text/javascript" src="js/site.js?silvana2018"></script>   
    <script type="text/javascript">
        function scrollMenu(menu) {
            $('body, html').animate({
                scrollTop: $('#' + menu).offset().top
            }, 1500);
        }
    </script>-->
</head>


<body>
    


<header class="header">
	<div class="content">

		<div id="nav-icon" class="menu-anchor">
			<span></span>
			<span></span>
			<span></span>
		</div>

		<a href="https://bommtempo.com.br" title="Bomm Tempo" class="logo-topo">
	<figure class="logo">
		<svg viewBox="0 0 483 105.6">
			<path class="logo__path1" d="M282.1 79.9h-5.6v-4.4h5.6v4.4zm0 25.7h-5.6V83.5h5.6v22.1zm4.5 0V83.5h4.3l.7 3.2c2.1-2.2 4.5-3.2 7.2-3.2 5.2 0 7.9 2.7 7.9 8.2v14h-5.6v-14c0-2.4-1.2-3.7-3.6-3.7-1.8 0-3.6.7-5.3 2.2v15.4h-5.6zm24.3-25.7h4.1l.8 3.6h5.6v4.4h-4.9v10.2c0 2.1.9 3.1 2.7 3.1h2.3v4.4h-5c-3.7 0-5.6-2-5.6-6V79.9zm23 3.6c6.7 0 10.1 3.4 10.1 10.3 0 .9-.1 1.8-.2 2.7h-14.7c0 3.1 2.3 4.7 6.9 4.7 2.2 0 4.5-.2 6.7-.6v4.4c-2 .4-4.3.6-7.1.6-8 0-12-3.8-12-11.3-.1-7.2 3.3-10.8 10.3-10.8m-4.8 9.1h9.4v-.2c0-3.1-1.5-4.6-4.6-4.6-3 0-4.6 1.6-4.8 4.8m18.3 13V83.5h4.3l.7 2.8c1.9-1.9 4-2.8 6.1-2.8V88c-2.1 0-3.9.9-5.6 2.6v15h-5.5zm13.6 0V83.5h4.3l.7 3.2c2.1-2.2 4.5-3.2 7.2-3.2 5.2 0 7.9 2.7 7.9 8.2v14h-5.6v-14c0-2.4-1.2-3.7-3.6-3.7-1.8 0-3.6.7-5.3 2.2v15.4H361zm34-22.1c6.7 0 10.1 3.4 10.1 10.3 0 .9-.1 1.8-.2 2.7h-14.7c0 3.1 2.3 4.7 6.9 4.7 2.2 0 4.5-.2 6.7-.6v4.4c-2 .4-4.3.6-7.1.6-8 0-12-3.8-12-11.3-.1-7.2 3.3-10.8 10.3-10.8m-4.8 9.1h9.4v-.2c0-3.1-1.5-4.6-4.6-4.6-3 0-4.6 1.6-4.8 4.8m18-12.7h4.1l.8 3.6h5.6v4.4h-5v10.2c0 2.1.9 3.1 2.7 3.1h2.3v4.4h-5c-3.7 0-5.6-2-5.6-6V79.9z"/>
			<path class="logo__path2" d="M79.8 98.5c4 0 7.2-3.2 7.2-7.2s-3.2-7.2-7.2-7.2-7.2 3.2-7.2 7.2 3.3 7.2 7.2 7.2zm0 0zm0 0zm0 0zm0 0zm0 0zm0 0zm0-98.5C107 0 129 22.1 129 49.3c8.7 1.7 15.2 9.3 15.2 18.5 0 2.7-.6 5.2-1.6 7.5h-33.3c.6-2.3.9-4.7.9-7.2 0-16.8-13.6-30.5-30.5-30.5S49.3 51.2 49.3 68.1c0 2.5.3 4.9.9 7.2H14.6C5.8 69.9 0 60.3 0 49.3c0-16.8 13.6-30.5 30.5-30.5 3.3 0 6.5.5 9.4 1.5C48.9 8 63.4 0 79.8 0m0 75.3c5.5 0 10.1 3.8 11.3 8.9h7c.3-1.4.5-2.9.5-4.4 0-10.4-8.4-18.8-18.8-18.8C69.4 60.9 61 69.3 61 79.7c0 1.5.2 3 .5 4.4h7c1.3-5 5.9-8.8 11.3-8.8m75.5-7.1c1.8.6 3.8.8 6.2.8 4.7 0 7-3.3 7-9.9 0-5.4-2-8.1-6.1-8.1-2.6 0-5 .5-7.2 1.6v15.6zm-7.8-34.9h7.8v13.5c2.3-1.6 4.9-2.3 7.8-2.3 8.8 0 13.1 4.9 13.1 14.8 0 10.7-5 16-15.1 16-4.2 0-8.8-.4-13.7-1.3V33.3zm39.9 26.5c0 6.4 2.5 9.7 7.5 9.7s7.5-3.2 7.5-9.7c0-6.3-2.5-9.4-7.5-9.4s-7.5 3.1-7.5 9.4m-7.8.1c0-10.3 5.1-15.5 15.3-15.5 10.2 0 15.3 5.2 15.3 15.5s-5.1 15.5-15.3 15.5c-10.1 0-15.2-5.2-15.3-15.5M215 75.3V44.5h6l.9 4.5c2.8-3 6.1-4.5 10-4.5 3.9 0 6.5 1.4 8 4.2 3.1-2.8 6.4-4.2 9.9-4.2 7.1 0 10.6 3.9 10.6 11.7v19.1h-7.8V55.9c0-3.4-1.5-5.1-4.6-5.1-2.3 0-4.5 1-6.7 3.1v21.4h-7.8V56c0-3.5-1.4-5.2-4.3-5.2-2.3 0-4.4 1-6.4 3.1v21.4H215zm51.7 0V44.5h6l.9 4.5c2.8-3 6.1-4.5 10-4.5 3.9 0 6.5 1.4 8 4.2 3.1-2.8 6.4-4.2 9.9-4.2 7.1 0 10.6 3.9 10.6 11.7v19.1h-7.8V55.9c0-3.4-1.5-5.1-4.6-5.1-2.3 0-4.5 1-6.7 3.1v21.4h-7.8V56c0-3.5-1.4-5.2-4.3-5.2-2.3 0-4.4 1-6.4 3.1v21.4h-7.8zm51.2-35.8h5.8l1.2 5h7.8v6.2h-6.9v14.2c0 2.9 1.2 4.3 3.7 4.3h3.2v6.2h-6.9c-5.2 0-7.8-2.8-7.8-8.3V39.5zm32 5c9.4 0 14 4.8 14 14.3 0 1.3-.1 2.5-.3 3.8h-20.5c0 4.4 3.2 6.5 9.6 6.5 3.1 0 6.2-.3 9.3-.9v6.2c-2.7.6-6 .9-9.9.9-11.2 0-16.8-5.3-16.8-15.8.1-10 4.9-15 14.6-15m-6.7 12.7h13.2V57c0-4.3-2.2-6.4-6.5-6.4-4.1 0-6.4 2.2-6.7 6.6m25.5 18.1V44.5h6l.9 4.5c2.8-3 6.1-4.5 10-4.5 3.9 0 6.5 1.4 8 4.2 3.1-2.8 6.4-4.2 9.9-4.2 7.1 0 10.6 3.9 10.6 11.7v19.1h-7.8V55.9c0-3.4-1.5-5.1-4.6-5.1-2.3 0-4.5 1-6.7 3.1v21.4h-7.8V56c0-3.5-1.4-5.2-4.3-5.2-2.3 0-4.4 1-6.4 3.1v21.4h-7.8zm59.5-8c1.9 1.1 4 1.6 6.3 1.6 4.6 0 7-3.3 7-9.8 0-5.5-2.6-8.3-7.7-8.3-2.2 0-4 .1-5.5.4v16.1zm-7.8-21.5c4.1-.8 8.4-1.3 13-1.3 10.5 0 15.7 4.9 15.7 14.7 0 10.8-4.9 16.2-14.6 16.2-2.1 0-4.2-.5-6.3-1.5v12.6h-7.8V45.8zm39.8 14c0 6.4 2.5 9.7 7.5 9.7s7.5-3.2 7.5-9.7c0-6.3-2.5-9.4-7.5-9.4s-7.5 3.1-7.5 9.4m-7.7.1c0-10.3 5.1-15.5 15.3-15.5 10.2 0 15.3 5.2 15.3 15.5s-5.1 15.5-15.3 15.5c-10.2 0-15.3-5.2-15.3-15.5"/>
		</svg>
	</figure>
</a>
		<nav class="nav">
			<ul class="menu">
				<li>
					<a class="menu__item js-scroller"
						data-id="#planos" href="<?php if (isset($_GET['param1']) && $_GET['param1'] == "suporte")  {echo "banner";} else {echo '';} ?>#planos"
												title="Planos">
						Planos
					</a>
				</li>
				<li>
					<a class="menu__item js-scroller"
						data-id="#cobertura" href="<?php if (isset($_GET['param1']) && $_GET['param1'] == "suporte")  {echo "banner";} else {echo '';} ?>#cobertura" 
												title="Cobertura">
						Cobertura
					</a>
				</li>
				<li>
					<a class="menu__item js-scroller"
						data-id="#sobre-nos" href="<?php if (isset($_GET['param1']) && $_GET['param1'] == "suporte")  {echo "banner";} else {echo '';} ?>#sobre-nos"
												title="Sobre Nós">
						Sobre Nós
					</a>
				</li>
				<li>
					<a class="menu__item " href="./suporte"
						
						title="Suporte">
						Suporte
					</a>
				</li>
				<li>
					<a class="menu__item js-scroller"
						data-id="#contato" href="<?php if (isset($_GET['param1']) && $_GET['param1'] == "suporte")  {echo "banner";} else {echo '';} ?>#contato"
												title="Contato">
						Contato
					</a>
				</li>
			
			</ul>
		</nav>
<!--
		<form class="header-search" method="post" action="https://www.bommtempo.com.br/busca/">
			<input type="hidden" name="ci_csrf_token" value="fb42085c07621ca8852366721e35868e">
			<input class="header-search__input" type="text" name="busca" placeholder="Pesquisar">
			<span class="header-search__button">
				<svg viewBox="0 0 112.6 112.6">
					<path d="M111.1 104l-31-31c6.2-7.6 9.9-17.4 9.9-28C90 20.2 69.8 0 45 0S0 20.2 0 45s20.2 45 45 45c10.6 0 20.4-3.7 28.1-9.9l31 31c1 1 2.3 1.5 3.5 1.5s2.6-.5 3.5-1.5c2-1.9 2-5.1 0-7.1M10 45c0-19.3 15.7-35 35-35s35 15.7 35 35-15.7 35-35 35-35-15.7-35-35" fill="#1d1d1b"/>
				</svg>
			</span>
					</form>-->

	</div>


<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
?>
    </header>
    




    