<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
class Connection{
   public static $instance;

   /**
    * @return PDO
    */
   public static function getInstance() { 
      if (!isset(self::$instance)) { 
         self::$instance = new PDO('mysql:host=localhost;dbname=bommtempo', 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); 
         self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
         self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
         self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING); 
      } 
         return self::$instance; 
   }
}


$subject = Connection::getInstance()->query("SELECT * FROM subject where id =".$_POST['subject'])->fetch();
$cidade = Connection::getInstance()->query("SELECT * FROM cidades where id =".$_POST['city'])->fetch();

require_once(__DIR__."/send_email.fnc.php");
header("Content-type: application/json;charset=utf-8");

$SEND_EMAIL = true; // se vai enviar email usando servidor de emails

if(strtoupper($_SERVER["REQUEST_METHOD"]) === "POST"){
    $response = [];
    if(false){
        $response["result"] = "EMAIL_NOT_VALID";
    }else{
        $columns = array("name", "email", "phone", "type_contact", "subject", "message", "city");
        //remoevs all unecessary data of $_POST
        foreach ($columns as $c) {
            if(!isset($_POST[$c])){
                $_POST[$c] = '';
            }
        }
        $filter_body = array_intersect_key($_POST, array_flip($columns));
        $filter_body = array_map("trim", $filter_body);
        $filter_body = array_map("strip_tags", $filter_body);
        $body = $filter_body;

        switch ($_POST["type_contact"]) {
            case '1':
            $metodo = 'Ligação';
            break;

            case '2':
            $metodo = 'E-mail';
            break;

            case '3':
            $metodo = 'WhatsApp';
            break;
            
            default:
            $metodo = '';
            break;
        }
        
        if($SEND_EMAIL){
            $html_msg = '    <html>
                            <head>
                                <title>Contato via site - '.EMAIL_NAME.'</title>
                            </head>

                            <body>
                                <p>'.$body["name"].' acaba de nos enviar uma mensagem através do nosso site. Seguem os dados de contato.</p>

                                <p>
                                    <b>Nome: </b> '.$body["name"].'<br />
                                    <b>Cidade: </b> '.$cidade["city"].'<br />
                                    <b>E-mail: </b> '.$body["email"].'<br />
                                    <b>Telefone: </b> '.$body["phone"].'<br />
                                    <b>Método: </b> '.$metodo.'<br />
                                    <b>Motivo: </b> '.$subject['subject'].'<br />
                                    <b>Mensagem: </b> <br />'.$body["message"].'<br />
                                    <hr>
                                    <em>Não responda a esse email, esse e-mail é automático.</em>
                                </p>
                            </body>
                        </html>';
            
            $mail = send_email("Contato via site ".EMAIL_NAME, $html_msg, $send_to, $body["email"]);
            $response["send_email"] = true;

           
            if($mail) {
                $response["result"] = "COULD_NOT_SEND";
            } else {
                 $insert = Connection::getInstance()->prepare("INSERT INTO contato (created_on, modified_on, name, email, city,  phone, subject, message, origin, type_contact) VALUES (now(), now(), :name, :email, :city,  :phone, :subject, :message, 'Contato', :type_contact)");
                if($insert->execute($body)){
                    $response["result"] = true;
                }else{
                    $response["result"] = false;
                }   
            }
        }else{
            $response["send_email"] = false;

            $insert = Connection::getInstance()->prepare("INSERT INTO mensagens_site (email, cidade, nome, telefone, mensagem) VALUES (:email, :cidade, :nome, :telefone, :mensagem)");
            if($insert->execute($body)){
                $response["result"] = true;
            }else{
                $response["result"] = false;
            } 
        }

    }

    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}";
    header("Location: ".$actual_link);
    exit;
    echo json_encode($response);
}