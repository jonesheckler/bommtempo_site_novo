-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 23, 2019 at 12:25 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bommtempo`
--

-- --------------------------------------------------------

--
-- Table structure for table `acessos`
--

CREATE TABLE `acessos` (
  `id` int(1) NOT NULL,
  `data` datetime DEFAULT NULL,
  `ip` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acessos`
--

INSERT INTO `acessos` (`id`, `data`, `ip`) VALUES
(91769, '2019-07-21 10:46:16', '::1'),
(91770, '2019-07-22 09:06:33', '::1'),
(91771, '2019-07-23 05:46:49', '::1'),
(91772, '2019-07-29 14:09:34', '::1'),
(91773, '2019-07-30 00:05:52', '::1'),
(91774, '2019-07-31 22:15:20', '::1'),
(91775, '2019-08-01 21:28:10', '::1'),
(91776, '2019-08-02 08:09:24', '::1'),
(91777, '2019-08-03 09:46:09', '::1'),
(91778, '2019-08-05 09:39:35', '::1'),
(91779, '2019-08-06 10:26:37', '::1'),
(91780, '2019-08-07 09:34:15', '::1'),
(91781, '2019-08-08 09:27:52', '::1'),
(91782, '2019-08-09 07:59:00', '::1'),
(91783, '2019-08-11 19:40:20', '::1'),
(91784, '2019-08-12 12:28:23', '::1'),
(91785, '2019-08-13 14:17:53', '::1'),
(91786, '2019-08-15 16:23:23', '::1'),
(91787, '2019-08-21 09:23:19', '::1'),
(91788, '2019-08-22 10:05:45', '::1'),
(91789, '2019-08-23 08:44:24', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `acessos_online`
--

CREATE TABLE `acessos_online` (
  `ip` varchar(20) NOT NULL,
  `time` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acessos_online`
--

INSERT INTO `acessos_online` (`ip`, `time`) VALUES
('::1', 1566563036);

-- --------------------------------------------------------

--
-- Table structure for table `botoes`
--

CREATE TABLE `botoes` (
  `id` int(11) NOT NULL,
  `nome_1` varchar(255) DEFAULT NULL,
  `nome_2` varchar(255) DEFAULT NULL,
  `banner` varchar(600) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `modal` tinyint(4) NOT NULL DEFAULT '0',
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `botoes`
--

INSERT INTO `botoes` (`id`, `nome_1`, `nome_2`, `banner`, `link`, `slug`, `modal`, `ativo`, `ordem`) VALUES
(1, 'Status', 'Conexão Normal!', 'uploads/botoes/1/f5eb8437823f54c813db.png', '', 'status', 1, 1, 1),
(2, 'Portal do Cliente', '', 'uploads/botoes/2/40a89914cfdbd7290af5.png', '', 'portal-do-cliente', 0, 1, 2),
(3, '2ª Via de Boleto', '', 'uploads/botoes/3/289f220ec6ea29948473.png', 'https://bloquetoexpresso.caixa.gov.br/bloquetoexpresso/index.jsp', '2a-via-de-boleto', 0, 1, 3),
(4, 'Teste de Velocidade', '', 'uploads/botoes/4/acf46f033be592456d39.png', 'https://www.minhaconexao.com.br/hosts/bommtempo.php', 'teste-de-velocidade', 0, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cidades`
--

CREATE TABLE `cidades` (
  `id_cidade` int(11) NOT NULL,
  `titulo_principal` varchar(255) NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cidades`
--

INSERT INTO `cidades` (`id_cidade`, `titulo_principal`, `ordem`, `ativo`, `created_at`) VALUES
(1, 'Alto Feliz ', 1, 1, '2019-08-09 13:34:43'),
(2, 'Bom Princípio', 2, 1, '2019-08-09 13:34:55'),
(3, 'Caxias do Sul (Vila Cristina)', 3, 1, '2019-08-09 13:35:51'),
(4, 'Feliz', 4, 1, '2019-08-09 13:36:01'),
(5, 'Harmonia', 5, 1, '2019-08-09 13:36:15'),
(6, 'Ivoti', 6, 1, '2019-08-09 13:36:25'),
(7, 'Linha Nova', 7, 1, '2019-08-09 13:42:22'),
(8, 'Nova Petrópolis', 10, 1, '2019-08-09 13:42:43'),
(9, 'Pareci Novo', 11, 1, '2019-08-09 13:42:51'),
(10, 'Picada Café', 12, 1, '2019-08-09 13:43:01'),
(11, 'Presidente Lucena', 15, 1, '2019-08-09 13:43:13'),
(12, 'São José do Hortêncio', 16, 1, '2019-08-09 13:43:25'),
(13, 'São Sebastião do Caí', 17, 1, '2019-08-09 13:43:37'),
(14, 'São Vendelino', 18, 1, '2019-08-09 13:43:44'),
(15, 'Tupandi ', 19, 1, '2019-08-09 13:43:49'),
(16, 'Vale Real ', 20, 1, '2019-08-09 13:43:56');

-- --------------------------------------------------------

--
-- Table structure for table `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(1) NOT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `descricao` varchar(155) DEFAULT NULL,
  `palavras` varchar(175) NOT NULL,
  `adwords` text NOT NULL,
  `contato` varchar(255) NOT NULL,
  `contato_cc` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `titulo`, `descricao`, `palavras`, `adwords`, `contato`, `contato_cc`) VALUES
(1, 'Bomm Tempo', 'Bomm Tempo', 'bomm tempo, bom tempo, internet, fibra, via radio, internet via radio', '', 'jones.heckler@gmail.com', 'jones.heckler@gmail.com;mw10@mw10.com.br');

-- --------------------------------------------------------

--
-- Table structure for table `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `rua` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `mapa` text,
  `rua2` varchar(255) DEFAULT NULL,
  `numero2` varchar(255) DEFAULT NULL,
  `bairro2` varchar(255) DEFAULT NULL,
  `cidade2` varchar(255) DEFAULT NULL,
  `uf2` varchar(255) DEFAULT NULL,
  `complemento2` varchar(255) DEFAULT NULL,
  `mapa2` text,
  `horario` text,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contato`
--

INSERT INTO `contato` (`id`, `rua`, `cidade`, `uf`, `cep`, `bairro`, `numero`, `complemento`, `mapa`, `rua2`, `numero2`, `bairro2`, `cidade2`, `uf2`, `complemento2`, `mapa2`, `horario`, `facebook`, `instagram`, `youtube`, `latitude`, `longitude`) VALUES
(1, 'R. Tiradentes', 'Feliz', 'RS', NULL, 'Centro', '260', '', 'https://www.google.com.br/maps/place/R.+Tiradentes,+260,+Feliz+-+RS,+95770-000/@-29.4522387,-51.307151,17z/data=!3m1!4b1!4m5!3m4!1s0x951eab1d800ab7e7:0x2161a979641ca836!8m2!3d-29.4522387!4d-51.3049623?hl=pt-BR', 'R. Treze de Maio', '935', 'Centro', 'São Sebastião do Caí', 'RS', '', '', 'Seg à Sexta das 8h30m às 12h  -  13h20m ás 18h\r\nSábados das 8h ás 12h', 'https://www.facebook.com/BommtempoInternet/', 'https://www.instagram.com/bommtempo_internet/', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `nome_1` varchar(255) DEFAULT NULL,
  `nome_2` varchar(255) DEFAULT NULL,
  `banner` varchar(600) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `destaque` tinyint(4) NOT NULL DEFAULT '0',
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `nome_1`, `nome_2`, `banner`, `link`, `destaque`, `ativo`, `ordem`) VALUES
(17, '', '', 'uploads/home_banners/17/8207421fe166c84afa1b.jpg', NULL, 0, 1, 2),
(18, '', '', 'uploads/home_banners/18/2bae2c629260abd7441c.jpg', NULL, 0, 1, 1),
(19, '', '', 'uploads/home_banners/19/e3489e42d1b3e0c21e53.jpg', NULL, 0, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `internet_tecnologia`
--

CREATE TABLE `internet_tecnologia` (
  `id_internet_tecnologia` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internet_tecnologia`
--

INSERT INTO `internet_tecnologia` (`id_internet_tecnologia`, `nome`) VALUES
(1, 'Fibra Óptica'),
(2, 'Via Rádio');

-- --------------------------------------------------------

--
-- Table structure for table `mensagens_site`
--

CREATE TABLE `mensagens_site` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(100) NOT NULL DEFAULT 'Não informado',
  `cidade` varchar(255) NOT NULL,
  `metodo` varchar(255) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `assunto` varchar(255) NOT NULL DEFAULT 'Sem assunto',
  `mensagem` text NOT NULL,
  `enviado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mensagens_site`
--

INSERT INTO `mensagens_site` (`id`, `nome`, `email`, `telefone`, `cidade`, `metodo`, `motivo`, `assunto`, `mensagem`, `enviado_em`) VALUES
(73, 'Jones Heckler', 'jones.heckler@gmail.com', '(54) 99177-7547', 'Feliz', 'Ligação', 'Motivo Jones', 'Sem assunto', 'teste', '2019-08-21 14:20:23'),
(74, 'Jones Heckler', 'jones.heckler@gmail.com', '(54) 99177-7547', 'Bom Princípio', 'Ligação', 'Orçamento', 'Sem assunto', 'ddas', '2019-08-22 14:19:08'),
(75, 'Jones Heckler', 'jones.heckler@gmail.com', '(54) 99177-7547', 'Bom Princípio', 'WhatsApp', 'Suporte Técnico', 'Sem assunto', 'teste', '2019-08-23 11:54:26'),
(76, 'Jones Heckler', 'jones.heckler@gmail.com', '(54) 99177-7547', 'Bom Princípio', 'Ligação', 'Motivo Jones', 'Sem assunto', 'dsadsa', '2019-08-23 12:10:51'),
(77, 'Jones Heckler', 'jones.heckler@gmail.com', '(54) 99177-7547', 'Bom Princípio', 'E-mail', 'Trabalhe Conosco', 'Sem assunto', 'sdfsdf', '2019-08-23 12:20:17');

-- --------------------------------------------------------

--
-- Table structure for table `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `_table` varchar(255) NOT NULL,
  `codigo` longtext NOT NULL,
  `icone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modulos`
--

INSERT INTO `modulos` (`id`, `nome`, `_table`, `codigo`, `icone`) VALUES
(1, 'Configurações', '', '', ''),
(2, 'Quem Somos', '', '', ''),
(3, 'Cidades', '', '', ''),
(4, 'Planos', '', '', ''),
(5, 'Mensagens via Site', '', '', ''),
(6, 'Banners', '', '', ''),
(8, 'Contato', '', '', ''),
(11, 'Botões', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `motivos`
--

CREATE TABLE `motivos` (
  `id_motivo` int(11) NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `contatos` text NOT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motivos`
--

INSERT INTO `motivos` (`id_motivo`, `motivo`, `contatos`, `ordem`, `ativo`, `created_at`) VALUES
(1, 'Orçamento', 'atendimento@bomtempo.com.br; comercial@bomtempo.com.br', 1, 1, '2019-08-09 17:37:54'),
(2, 'Suporte Técnico', 'comercial@bomtempo.com.br; atendimento@bomtempo.com.br; suporte@bomtempo.com.br', 2, 1, '2019-08-09 17:37:15'),
(3, 'Cancelamento', 'atendimento@bomtempo.com.br; comercial@bomtempo.com.br', 3, 1, '2019-08-21 12:38:11'),
(4, 'Solicitação Comercial', 'atendimento@bomtempo.com.br; comercial@bomtempo.com.br', 4, 1, '2019-08-21 12:45:19'),
(5, 'Solicitação Financeira', 'atendimento@bomtempo.com.br; comercial@bomtempo.com.br', 5, 1, '2019-08-21 12:45:44'),
(6, 'Quero ser fornecedor', 'compras@bomtempo.com.br; ronaldo@bomtempo.com.br', 6, 1, '2019-08-21 12:47:57'),
(7, 'Outros', 'atendimento@bomtempo.com.br; comercial@bomtempo.com.br', 10, 1, '2019-08-21 12:46:27'),
(8, 'Trabalhe Conosco', 'tais@bomtempo.com.br; rh@bomtempo.com.br', 8, 1, '2019-08-23 12:14:58'),
(9, 'Quero falar com a direção', 'jose@bomtempo.com.br; dainane@bomtempo.com.br; tais@bomtempo.com.br', 7, 1, '2019-08-21 12:47:37'),
(10, 'Motivo Jones', 'jones.heckler@gmail.com;mw10@mw10.com.br', 13, 0, '2019-08-23 12:23:06'),
(11, 'Quero Bommtempo na minha cidade', 'atendimento@bomtempo.com.br; comercial@bomtempo.com.br', 9, 1, '2019-08-23 12:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `painel_administrativo`
--

CREATE TABLE `painel_administrativo` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `imagem_login` varchar(255) DEFAULT NULL,
  `imagem_logo` varchar(255) DEFAULT NULL,
  `cor_primaria` varchar(255) DEFAULT NULL,
  `cor_secundaria` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `painel_administrativo`
--

INSERT INTO `painel_administrativo` (`id`, `titulo`, `subtitulo`, `imagem_login`, `imagem_logo`, `cor_primaria`, `cor_secundaria`) VALUES
(1, 'MW10', 'MW10', 'uploads/painel_administrativo/imagem_login/computer-768608-1920-jpg.jpg', 'uploads/painel_administrativo/imagem_logo/screen-shot-2019-07-11-at-13-40-25-png.png', '#f04f23', '#d913b6');

-- --------------------------------------------------------

--
-- Table structure for table `planos_hospedagem`
--

CREATE TABLE `planos_hospedagem` (
  `id_plano_hospedagem` int(11) NOT NULL,
  `plano` varchar(225) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `banda_mensal` varchar(255) NOT NULL,
  `espaco_disco` varchar(255) DEFAULT NULL,
  `banco_dados` int(11) NOT NULL DEFAULT '0',
  `ftp` int(11) NOT NULL DEFAULT '0',
  `subdominios` int(11) NOT NULL DEFAULT '0',
  `anti_spam` tinyint(4) NOT NULL DEFAULT '1',
  `webmail` tinyint(4) NOT NULL DEFAULT '1',
  `painel_admin` tinyint(4) NOT NULL DEFAULT '1',
  `suporte_tecnico` varchar(255) DEFAULT NULL,
  `valor` decimal(8,2) DEFAULT NULL,
  `label_botao` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `id_plano_servico` int(11) NOT NULL DEFAULT '3',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `planos_hospedagem`
--

INSERT INTO `planos_hospedagem` (`id_plano_hospedagem`, `plano`, `email`, `banda_mensal`, `espaco_disco`, `banco_dados`, `ftp`, `subdominios`, `anti_spam`, `webmail`, `painel_admin`, `suporte_tecnico`, `valor`, `label_botao`, `ordem`, `ativo`, `id_plano_servico`, `created_at`) VALUES
(12, 'Basic', '10 contas', '20 GB', '250 MB', 0, 1, 0, 1, 1, 1, 'Horário comercial', '19.90', 'Assinar', 1, 1, 3, '2019-08-09 01:08:14'),
(13, 'Lite', '20 contas', '50 GB', '500 MB', 1, 2, 1, 1, 1, 1, 'Horário Comercial', '29.90', 'Assinar', 2, 1, 3, '2019-08-09 01:32:16'),
(14, 'Premium', '35 contas', 'ilimitado', '1 GB', 1, 4, 3, 1, 1, 1, 'Horário Comercial', '39.90', 'Assinar', 3, 1, 3, '2019-08-09 01:33:37'),
(15, 'Platinum', '50 contas', 'ilimitado', '2 GB', 2, 4, 3, 1, 1, 1, 'Horário comercial', '49.90', 'Assinar', 4, 1, 3, '2019-08-09 01:38:49'),
(16, 'Gold', '80 contas', 'ilimitado', '4 GB', 2, 6, 4, 1, 1, 1, '24 horas', '69.90', 'Assinar', 5, 1, 3, '2019-08-09 01:40:16');

-- --------------------------------------------------------

--
-- Table structure for table `planos_internet_empresa`
--

CREATE TABLE `planos_internet_empresa` (
  `id_plano_empresa` int(11) NOT NULL,
  `titulo_principal` varchar(255) NOT NULL,
  `titulo_secundario` varchar(255) NOT NULL,
  `descricao` text,
  `label_botao` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `id_plano_servico` int(11) NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `planos_internet_empresa`
--

INSERT INTO `planos_internet_empresa` (`id_plano_empresa`, `titulo_principal`, `titulo_secundario`, `descricao`, `label_botao`, `ordem`, `ativo`, `id_plano_servico`, `created_at`) VALUES
(2, '15', 'Mbps', 'Ideal para as necessidades básicas!', 'Assinar', 3, 1, 2, '2019-08-09 00:31:42'),
(3, '35', 'Mbps', 'Velocidade ideal para todos os momentos!', 'Assinar', 2, 1, 2, '2019-08-09 00:32:13'),
(4, '50', 'Mbps', 'Ultravelocidade em diversos dispositivos!', 'Assinar', 1, 1, 2, '2019-08-09 00:32:39'),
(5, 'Novidade', 'Planos EPP', 'Soluções ideais para todo tipo de negócio.', 'Saiba mais sobre nossos planos', 4, 1, 2, '2019-08-09 00:33:36');

-- --------------------------------------------------------

--
-- Table structure for table `planos_internet_voce`
--

CREATE TABLE `planos_internet_voce` (
  `id_plano_voce` int(11) NOT NULL,
  `id_tecnologia` int(11) NOT NULL,
  `titulo_principal` varchar(255) NOT NULL,
  `titulo_secundario` varchar(255) NOT NULL,
  `descricao` text,
  `valor` decimal(8,2) DEFAULT NULL,
  `label_botao` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `id_plano_servico` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `planos_internet_voce`
--

INSERT INTO `planos_internet_voce` (`id_plano_voce`, `id_tecnologia`, `titulo_principal`, `titulo_secundario`, `descricao`, `valor`, `label_botao`, `ordem`, `ativo`, `id_plano_servico`, `created_at`) VALUES
(11, 1, '10', 'Mbps', 'Máximo Down 100%  e  Máximo Up 30% ', '90.00', 'Assinar', 5, 1, 1, '2019-08-09 12:33:12'),
(12, 1, '25', 'Mbps', 'Máximo Down 100%  e  Máximo Up 30% ', '110.00', 'Assinar', 4, 1, 1, '2019-08-09 12:33:05'),
(13, 1, '50', 'Mbps', 'Máximo Down 100%  e  Máximo Up 30% ', '120.00', 'Assinar', 3, 1, 1, '2019-08-09 12:32:59'),
(14, 1, '100', 'Mbps', 'Máximo Down 100%  e  Máximo Up 30% ', '150.00', 'Assinar', 2, 1, 1, '2019-08-09 12:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `planos_servicos`
--

CREATE TABLE `planos_servicos` (
  `id_plano_servico` int(11) NOT NULL,
  `nome` varchar(225) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `planos_servicos`
--

INSERT INTO `planos_servicos` (`id_plano_servico`, `nome`, `slug`, `ativo`) VALUES
(1, 'Internet para Você', 'internet-para-voce', 1),
(2, 'Internet para sua empresa', 'internet-para-sua-empresa', 1),
(3, 'Hospedagem', 'hospedagem', 1),
(4, 'Sistema de Segurança', 'sistema-de-seguranca', 1);

-- --------------------------------------------------------

--
-- Table structure for table `planos_sistema_seguranca`
--

CREATE TABLE `planos_sistema_seguranca` (
  `id_plano_sistema_seguranca` int(11) NOT NULL,
  `titulo_principal` varchar(255) NOT NULL,
  `descricao` text,
  `label_botao` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `id_plano_servico` int(11) NOT NULL DEFAULT '4',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `planos_sistema_seguranca`
--

INSERT INTO `planos_sistema_seguranca` (`id_plano_sistema_seguranca`, `titulo_principal`, `descricao`, `label_botao`, `ordem`, `ativo`, `id_plano_servico`, `created_at`) VALUES
(1, 'Alarmes', 'Escolha a melhor solução para proteger seus bens e sua família. Oferecemos diversas opções de alarmes para você escolher a alternativa que melhor atende suas necessidades.', 'Solicite uma visita técnica', 2, 1, 4, '2019-08-09 12:31:56'),
(2, 'Videomonitoramento', 'Câmeras de Segurança para você ou sua empresa, com acesso em tempo real. Sistema moderno e inteligente de monitoramento de imagens, controle de acesso e vídeo verificação em tempo real.', 'Solicite uma visita técnica', 1, 1, 4, '2019-08-09 12:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `sobre`
--

CREATE TABLE `sobre` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `imagem` varchar(600) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sobre`
--

INSERT INTO `sobre` (`id`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`) VALUES
(2, 'Conectando pessoas', '<p>Há mais de 20 anos atendemos cada cliente de forma única e inovadora, oferecendo experiências marcantes.<br />\r\nAlém da melhor Internet da região do Vale do Caí a Bommtempo conta com as melhores soluções em Sistemas de Segurança para sua casa, empresa ou condomínio. </p>\r\n\r\n<p>Vem para o lado Bomm da Internet!</p>\r\n', NULL, 1, 1),
(3, 'Responsabilidade ambiental', '<p>Somos a única empresa de Internet a conquistar o SELO SOLAR, por produzir a própria energia e ainda devolver o excedente para a rede pública. Esse é nosso jeito de cuidar do mundo!</p>', 'uploads/sobre/3/b1f3d4404b516e4aac40.png', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `suporte`
--

CREATE TABLE `suporte` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoria` enum('sobre-minha-internet','sobre-meu-pagamento','sobre-meu-contrato','sobre-a-bommtempo') NOT NULL,
  `pergunta` varchar(255) NOT NULL,
  `resposta` longtext NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `documento` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suporte`
--

INSERT INTO `suporte` (`id`, `categoria`, `pergunta`, `resposta`, `link`, `documento`, `ordem`, `ativo`) VALUES
(1, 'sobre-minha-internet', 'Minha conexão está lenta. O que devo fazer?', '<p style=\"margin-bottom: 0cm; line-height: 100%;\">\nSe\nsua conexão parecer lenta, feche todas as abas de Internet e faça o\nteste da velocidade em um navegador através do site: <a href=\"http://www.minhaconexao.com.br/hosts/bommtempo.php\" target=\"_blank\">Minha conexão!</a></p><p style=\"margin-bottom: 0cm; line-height: 100%;\"><br></p><p align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%;\"><b>O ideal é realizar o teste conectado via WiFi. Caso nesse teste\napresente alguma inconformidade na velocidade de conexão, o teste\ndeve ser realizado novamente em um computador/notebook que se conecte\na internet através do cabo que sai da ONT (no caso de fibra) ou da\nLAN da fonte PoE ( no caso de rádio). \n</b></p><p align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%;\">\n<b>Se\no teste pelo cabo no computador/notebook apresentar a velocidade\ncontratada, existe grande possibilidade de existir algum problema\nrelacionado ao roteador WiFi. </b>\n</p><p style=\"margin-bottom: 0cm; line-height: 100%;\"><br></p><p style=\"margin-bottom: 0cm; line-height: 100%\">-\nCaso apareça a mensagem “Não\nRespondendo” observe\nse há muitos programas abertos ao mesmo tempo. Feche os programas e\nreinicie o computador. Se o\nproblema persistir, procure\num técnico de sua confiança para fazer uma verificação no\nseu computador/notebook.</p>', '', NULL, 2, 1),
(2, 'sobre-minha-internet', 'Estou sem conexão WI-FI. O que devo fazer?', '<p align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%;\">\nObserve\nse seu roteador está ligado e com as luzes acesas. Caso esteja\nligado e todos os dispositivos que conectam via Wi-Fi estejam sem\nacesso, desligue o roteador e ligue novamente. No caso de somente um\ndispositivo estar sem acesso à internet, verifique se as\nconfigurações do dispositivo estão corretas (Wireless ativado).</p>\n<p align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br>\n\n</p>\n<p align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Verifique\nse as quedas ocorrem em todos os dispositivos que utilizam a internet\nou somente em algum específico. No caso de ocorrer somente em um\ndispositivo pode ser um problema específico do mesmo como: distância\ndo roteador, problemas no software do equipamento, entre outros.</p>', '', NULL, 2, 1),
(3, 'sobre-minha-internet', 'Meu acesso está caindo. O que devo fazer?', '<p style=\"margin-bottom: 0cm; line-height: 100%;\">\n<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">Observe\n</span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">se\nseu aparelho </span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">desconectou\nda rede </span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">WI-FI</span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">.\n</span>Se<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">\nnão est</span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">iver</span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">\nconectado, reinicie seu roteador e seu dispositivo. </span>\n</p>\n<p style=\"margin-bottom: 0cm; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100%;\">\n<br>\n\n</p>\n<p style=\"margin-bottom: 0cm; line-height: 100%\"><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">Caso\nesteja conectado, entre em contato com o </span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">Suporte\nTécnico </span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">telefônico</span><span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">\nSEM reiniciar os equipamentos da internet.&nbsp;</span>\n</p>', '', NULL, 3, 1),
(12, 'sobre-minha-internet', 'Qual é o melhor roteador para a minha casa ou empresa?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nPara\nsaber qual é o melhor roteador para atender a sua casa seria\nnecessário passar por uma avaliação técnica. </p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Atualmente, a\nempresa trabalha com roteadores das marcas TP-link, Mikrotik e UniFi.\nA escolha do roteador tem grande influencia sobre a qualidade do\nserviço prestado, sempre que você tiver dúvidas ligue para a nossa\nequipe, poderemos te auxiliar na escolha do melhor equipamento.</p>', '', NULL, 4, 1),
(13, 'sobre-minha-internet', 'Posso dividir internet com o meu vizinho?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nCompartilhar\ninternet com os vizinhos de forma clandestina é ilegal. </p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">De acordo\ncom a ANATEL (<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">Agência\nNacional de Telecomunicações), </span>a\nInternet contratada deve ser usada <span style=\"font-variant-numeric: normal; font-variant-east-asian: normal;\">em\nlocais que não ultrapassam os limites da sua residência</span>.\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Violando esta norma, você corre o risco de ser multado em até R$\n10.000,00 (dez mil reais).&nbsp;\n</p>', '', NULL, 6, 1),
(14, 'sobre-minha-internet', 'Como faço para mudar a senha do meu roteador?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<font color=\"#000000\"><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">Para\nisso você pode entrar em contato conosco e solicitar\numa visita da nossa equipe técnica que pode ir até a sua residência\npara fazer essa alteração.</font></font></font></p>', '', NULL, 5, 1),
(15, 'sobre-minha-internet', 'Quantos Mbps são recomendados por dispositivos?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nSegundo\navaliações técnicas em diversas situações\nde uso, para cada dispositivo que estará conectado em uma rede de\nWiFi\né necessário cerca de 2Mbps. </p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Essa recomendação foi baseada em uma\nmédia de uso e pode variar também conforme o dispositivo e a\natividade que será desempenhada por eles.</p>', '', NULL, 7, 1),
(16, 'sobre-meu-pagamento', 'Quais são as formas de pagamento disponíveis para pagar a minha mensalidade?', '<p class=\"western\" align=\"left\" style=\"margin-bottom: 0cm; line-height: 100%\"><b>Pagamento\nLoja:</b></p><p class=\"western\" align=\"left\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nNessa\nopção você faz o pagamento da sua mensalidade direto em uma das\nlojas da Bommtempo Internet. Essa forma de pagamento dispensa o uso\nde qualquer tipo de carnê e evita o desperdício de papel impresso.\nVocê recebe um comprovante dos pagamentos que fez e pronto, é o\nmétodo mais simples de pagamento da sua fatura. \n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%;\">\n<br>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<b>Débito\nem Conta:</b></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" style=\"margin-bottom: 0cm; line-height: 100%\">O\ndébito em conta ou débito automático é a solução dos problemas\npara quem acaba esquecendo-se de pagar uma conta ou não tem tempo a\nperder em filas de Banco ou de lotérica. Sem contar na segurança\nnas transações, que são feitas online, sem a necessidade de sacar\ndinheiro vivo. Os pagamentos tem os seus valores debitados na data do\nvencimento, direto da sua conta corrente. E o melhor é que esse\nserviço não possui nenhum custo adicional para você. Você só\nprecisa possuir uma conta corrente em uma das agencias conveniadas\nconosco. Os bancos são: Bancos do Brasil, Banrisul e Sicredi. \n</p><p class=\"western\" style=\"margin-bottom: 0cm; line-height: 100%;\">\n<br>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<b>Boleto\nBancário:</b></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br>Esse\né o formato de pagamento que você recebe mensalmente por e-mail um\ntítulo de cobrança que pode ser pago em qualquer instituição ou\nestabelecimento conveniado, até a data de vencimento. Esse pagamento\npode ser efetuado em casas lotéricas, Correios, caixas eletrônicos\ne até mesmo por meio da internet pelo acesso por home banking ou\naplicativos de smartphones e tablets. Para isso você utilizará o\ncódigo de barras do boleto e efetuará o pagamento em locais que o\naceitem ou então pode imprimir o boleto que você receberá por\ne-mail todo mês para pagá-lo no local de sua preferência.&nbsp;</p>', '', NULL, 1, 1),
(17, 'sobre-meu-pagamento', 'Não possuo e-mail. Como posso pagar o meu boleto?', '<p class=\"western\" align=\"left\" style=\"margin-bottom: 0cm; line-height: 100%\">Caso\nnão utilize e-mails você pode pagar sua conta em uma agência\nlotérica, portanto apenas o código de identificação que é 10 +\n883866 + CPF do Titular da conta e pronto. </p><p class=\"western\" align=\"left\" style=\"margin-bottom: 0cm; line-height: 100%\">Assim fica ainda mais\nfácil pagar as suas mensalidades.&nbsp;<br></p>', '', NULL, 2, 1),
(18, 'sobre-meu-pagamento', 'Como posso alterar minha forma de pagamento?', '<p class=\"western\" align=\"left\" style=\"margin-bottom: 0cm; line-height: 100%\">Para\nhabilitar uma forma de pagamento diferente da que você tem hoje,\nsolicite a nossa equipe por telefone ou através do e-mail:\n<a href=\"mailto:atendimento@bomtempo.com.br\">atendimento@bomtempo.com.br</a>.<br></p><p>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<br>\n\n</p>', '', NULL, 3, 1),
(19, 'sobre-meu-pagamento', 'Como posso gerar a 2ª via atualizada do meu boleto?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVocê\npode fazer a emissão de nova 2ª via atualizada acessando o site:&nbsp;<a href=\"https://si-web.sicredi.com.br/boletoweb/BoletoWeb.servicos.Index.task\" target=\"_blank\">Boleto Sicredi</a></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nE\ninserindo os dados:</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n→ CNPJ\ndo Beneficiário (Bommtempo) <b>02.591.052-0001-05</b></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n→ CPF\nou CNPJ do Titular da conta de Internet que você quer pagar.</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n→ Nosso\nNúmero, que é o número do seu boleto. (*esse é o único nº que\nmuda a cada mês)</p><p>\n\n\n\n\n\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nLembre-se:\nboletos vencidos podem ser pagos numa agência do Sicredi, em horário\nbancário.</p>', '', NULL, 4, 1),
(20, 'sobre-meu-pagamento', 'Tenho débito em conta mas no dia do pagamento não debitou o valor da mensalidade, como posso quitar minha pendência?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Você\npode entrar em contato conosco para que possamos encontrar a melhor forma de quitar a sua pendência, a fim de evitar\nbloqueios ligando para nosso suporte telefônico 24h:</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVIVO\n- 51 9 97579912</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVIVO\n- 51 9 99419728</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nCLARO\n- 51 9 93461002</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nTIM\n- 51 9 82980674</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nFIXO\n- (51) 36374500</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nOu\nenvie-nos um e-mail para: atendimento@bomtempo.com.br</p><p>\n\n\n\n\n\n\n\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<br>\n\n</p>', '', NULL, 5, 1),
(21, 'sobre-meu-pagamento', 'Quais são os bancos conveniados para fazer débito direto na minha conta bancária?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Possuímos\nconvênio com os bancos: Sicredi, Banrisul e Banco do Brasil.<br></p><p>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nPara\nhabilitar será necessário o preenchimento/assinatura de um termo de\nautorização de débito. </p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Solicite\na nossa equipe por telefone ou através do e-mail:\n<a href=\"mailto:atendimento@bomtempo.com.br\">atendimento@bomtempo.com.br</a>.</p>', '', NULL, 6, 1),
(22, 'sobre-meu-pagamento', 'Paguei o boleto errado do meu carnê, efetuei o pagamento do mês seguinte ao invés do boleto do mês atual, como faço para substituir?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Nesse\ncaso é necessário que você nos comunique. Assim quitaremos a\nmensalidade vencida e lhe encaminhemos um boleto atualizado do mês\nque você pagou por engano.<br></p>', '', NULL, 7, 1),
(23, 'sobre-meu-pagamento', 'Por que recebi duas notas fiscais diferentes da mesma mensalidade?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><span style=\"font-size: 11pt; font-family: Arial, serif;\">Essa\nnota fiscal e esse recibo numerado que você recebeu se refere ao\nserviço de acesso à internet prestado no período de uso mensal.</span><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<br>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<font color=\"#000000\"><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">Ao\nse contratar um serviço de acesso à internet, há a necessidade de\nse contratar não apenas o provimento de serviço de conexão à\ninternet, mas também um prestador de serviços de comunicação\nmultimídia que lhe dê suporte. Ambos os serviços podem ser\nprestados pela mesma empresa, ou por empresas distintas.</font></font></font></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><font color=\"#000000\"><font face=\"Arial, serif\"><font style=\"font-size: 11pt\"><br></font></font></font></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<font color=\"#000000\"><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">Fornecemos\no serviço de acesso à internet pelo provedor IMAF Internet Eireli -\nEPP, onde é emitido o recibo numerado (Nota de débito) equivalente\nao valor do serviço prestado e o fornecimento do serviço de\ncomunicação multimídia é feito pela empresa Bom Tempo Serviços\nde Comunicação Multimídia Eireli EPP, onde é emitida a NF Mod 21\nequivalente ao valor do serviço prestado.</font></font></font></p>', '', NULL, 8, 1),
(24, 'sobre-meu-contrato', 'Preciso trocar a titularidade do meu contrato, quais documentos eu necessito?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Para\ntrocar a titularidade do contrato de Internet é necessário que o\nantigo e o novo titular assinem um termo consentindo com a alteração\ndo contrato. </p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Também é necessário que o novo titular traga e/ou\nenvie um documento de identificação que pode ser um RG, CPF, CNH,\nou qualquer outro documento que sirva para completar o cadastro do\nnovo titular.&nbsp;<br></p>', '', NULL, 2, 1),
(27, 'sobre-meu-contrato', 'Já tenho um contrato em meu nome, posso contratar mais um ponto em outro endereço?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nClaro,\nvocê pode ter mais de um ponto de internet em seu nome, e terá\ndireito a um desconto na contratação do seu novo ponto de internet.\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Ligue\npara:</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nFIXO\n- (51) 36374500</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVIVO\n- 51 9 97579912</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVIVO\n- 51 9 99419728</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nCLARO\n- 51 9 93461002</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nTIM\n- 51 9 82980674</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Ou\nenvie-nos um e-mail para: comercial@bomtempo.com.br</p>', '', NULL, 3, 1),
(28, 'sobre-minha-internet', 'Qual é a diferença entre um plano doméstico o e um plano Empresarial?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Planos\nDomésticos são indicados para consumo residencial, são planos que\npossuem valores mais acessíveis e sua utilização está relacionada\nao lazer, estudos, busca por informações, entre outros.<br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<br>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nJá\nos Planos Empresariais são serviços indicados a corporações e/ou\nempresas que necessitam do acesso à internet para trabalhar. \n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<br>\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nA\nprincipal diferença entre as duas modalidades de planos é\nprioridade no atendimento que têm os clientes empresariais. Sempre\nque houver alguma dificuldade no acesso ou qualquer outra situação,\nos clientes empresariais terão prioridade na fila de atendimento. \n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nOutra\nvantagem dos planos empresariais é a possibilidade de instalarmos um\nlink de backup via-rádio, que assumirá a conexão se houver algum\nrompimento na fibra óptica, garantindo assim ainda mais segurança\nem todos os processos que envolvem a internet*.</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nAlém\nde tudo isso, os planos empresariais também contam com um endereço\nde IP Fixo, que é necessário para acesso à sistemas de\nmonitoramento, servidores, liberação de portas, etc.</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nOutro\ndetalhe importante é que o contrato do plano empresarial é feito no\nCNPJ do cliente, e do plano doméstico é no CPF.</p><p>\n\n\n\n\n\n\n\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n*Os\nbackups necessitam de avaliação técnica para serem\ndisponibilizados.</p>', '', NULL, 8, 1),
(29, 'sobre-minha-internet', 'Qual é a diferença entre um plano Dedicado e um plano Compartilhado?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">No\ncompartilhado o seu computador é conectado diretamente a um provedor\nde internet “comunitário”, ou seja, dividindo a conexão com\nvários usuários. Dessa forma, a velocidade da internet pode\noscilar, de acordo com o número de pessoas que estão conectadas\nnaquele momento. As vantagens de adquirir esse tipo de serviço é o\nbaixo custo e a viabilidade e rapidez de instalação.<br></p><p>\n\n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">No\ndedicado o seu computador é ligado diretamente ao provedor, sem\ninterrupções no caminho, ou seja, com um link somente seu. A\ninternet dedicada geralmente é utilizada por empresas que necessitam\nde uma boa estabilidade de conexão e alta performance. As vantagens\nde adquirir esse tipo de serviço são: estabilidade de sinal,\ngarantia da velocidade contratada.</p>', '', NULL, 9, 1),
(30, 'sobre-minha-internet', 'Tenho um estabelecimento comercial, como funciona o HotSpot?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<font color=\"#000000\"><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">O\nServiço funciona em um local onde uma rede Wi-Fi está disponível\npara seus clientes. Estes pontos são oferecidos </font></font><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">geralmente\nem</font></font><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">\nbares, cafeterias, restaurantes e outros estabelecimentos. </font></font></font>\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\n<font color=\"#000000\"><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">A\nBommtempo Internet </font></font><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">oferece</font></font><font face=\"Arial, serif\"><font style=\"font-size: 11pt\">\num sistema que te auxilia a administrar as conexões e ter um\nhistórico da frequência de utilização dos seus clientes. Tudo\nisso com planos dedicados a esse tipo de estabelecimento. Se informe\ncom um de nossos consultores e solicite uma avaliação técnica.</font></font></font></p>', '', NULL, 10, 1),
(31, 'sobre-meu-pagamento', 'No meu navegador aparece uma mensagem de \"Irregularidade no Cadastro\", o que pode estar errado?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Entre\nem contato conosco para que possamos te auxiliar e identificar a\nirregularidade. </p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Ligue para:</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\"><br></p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nFIXO\n- (51) 36374500</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVIVO\n- 51 9 97579912</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nVIVO\n- 51 9 99419728</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nCLARO\n- 51 9 93461002</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nTIM\n- 51 9 82980674</p><p>\n\n\n\n\n\n\n</p><p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">\nOu\nenvie-nos um e-mail para: atendimento@bomtempo.com.br</p>', '', NULL, 9, 1),
(32, 'sobre-meu-pagamento', 'Meu acesso bloqueou por falta de pagamento e paguei hoje por boleto, quanto tempo demora para o acesso voltar?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">Nesse\nformato de pagamento, o boleto só é reconhecido no dia seguinte,\nquando o banco encaminha até nossa equipe financeira um arquivo de\nretorno contendo a informação da liquidação. E somente depois\ndesse retorno ser recebido e do sistema identificar o pagamento que a\nsua conexão é liberada, à menos que tenha o recurso de\nauto-desbloqueio para utilizar nessa fatura.<br></p>', '', NULL, 10, 1),
(33, 'sobre-minha-internet', 'Como funciona o Auto-desbloqueio?', '<p class=\"western\" align=\"justify\" style=\"margin-bottom: 0cm; line-height: 100%\">O\nAuto-desbloqueio é um recurso criado para liberar o acesso do\ncliente que foi bloqueado por falta de pagamento. Esse recurso pode\nser utilizado apenas 1x à cada fatura e é recomendado que seja\nusado assim que o pagamento tenha sido feito para que no dia seguinte\no cliente volte a acessar novamente.&nbsp;<br></p>', '', NULL, 11, 1),
(34, 'sobre-meu-contrato', 'Contrato de prestação de serviços – Bommtempo Internet', '<p>Você pode fazer o download do&nbsp;contrato de prestação de serviços da Bommtempo Internet, no formato PDF.<br>Se tiver dúvidas sobre algum item deste documento, entre em contato conosco.</p>', '', NULL, 1, 0),
(35, 'sobre-a-bommtempo', 'Para que existimos?', '<p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">Para\noferecer a melhor experiência em comunicação através da internet\ne encantar as pessoas com um atendimento exclusivo e carinhoso.</p>', '', NULL, 1, 1),
(36, 'sobre-a-bommtempo', 'Para que lutamos?', '<p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">Para\nconquistar o maior índice de clientes satisfeitos com serviços de\ninternet do estado.<br></p>', '', NULL, 2, 1),
(37, 'sobre-a-bommtempo', 'O jeito Bommtempo de ser!', '<p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>Bomm\né acreditar nas pessoas</b><br></p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">Todas\nas conquistas vêm através das pessoas. Buscamos profissionais\ntalentosos, apaixonados e dispostos a viver em um ambiente de\ncolaboração, união e amizade.</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>Bomm\npara nós é Bomm para você </b>\n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">Atender\nde forma encantadora, com dedicação e excelência. Surpreender o\ncliente é a nossa principal motivação e a nossa meta é todos\nsaírem ganhando.  \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>Bomm\né fazer mais com menos</b></p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">“A\ncriatividade é a inteligência se divertindo”. O pouco pode ser\ntudo para os criativos.</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>Bomm\né ser simples</b></p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">Simplicidade\né a chave para grandes resultados. Simplificar não é ser\nsuperficial, é lidar com as situações de forma objetiva e clara.\nNão precisa ser complexo para ser eficiente.</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>Bomm\né vestir a camisa</b></p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">Para\num time ser vencedor é necessário entrar em campo com espirito de\ncapitão. Jogar com comprometimento, responsabilidade e\ntransparência. Servindo como exemplo de respeito e integridade.  \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>Bomm\né pensar fora da caixa</b></p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">“Em\ncabeças quadradas, novas ideias não circulam”. Perspicácia para\nver no problema a solução é o caminho para evoluirmos sempre. O\nque está bom sempre pode ficar melhor, por isso, inovação sempre\nserá bem-vinda aqui!</p>', '', NULL, 3, 1),
(38, 'sobre-a-bommtempo', 'Como tudo começou?', '<p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>02\nde junho de 1998</b> – A Bommtempo é fundada trabalhando\ninicialmente com o conserto e manutenção de equipamentos\neletrônicos (computadores e impressoras), além da instalação de\nsoftwares em geral, tais como Windows, antivírus, entre outros. \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>1999</b>\n– Começa a implantação da venda de internet, na modalidade\ndiscada, que se utilizava da linha de telefone fixo para funcionar.\nOs primeiros clientes foram amigos e empresas próximos da família\ndo sócio fundador. \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>2000</b> – Com o destaque do negócio, iniciou sua ampliação com a\nconstrução da sua sede. \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>2001\n</b>– Tem início a implantação da Internet Banda Larga via\nRádio, atendendo inicialmente os municípios de Feliz, Bom\nPrincípio, Vale Real e Alto Feliz. A implantação da internet via\nrádio necessitava da construção de torres, nas quais são\ncolocadas antenas retransmissoras do sinal. Até este momento,\ncontávamos com 4 torres – uma em cada município – 400 clientes\ne 5 colaboradores. \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>2010</b>\n– O objetivo de ampliar a estrutura para todos os municípios da\nregião do Vale do Caí é atingido. Até aí, já contávamos com 30\ncolaboradores, 20 torres de retransmissão e mais de 2.000 clientes.</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>2012</b>\n– A dedicação para o serviço de internet se torna cada vez maior\ne é desativado o setor de manutenção de computadores e\nimpressoras. Aqui, começa um grande passo: implantação da rede de\nFibra Óptica na cidade de Feliz, já estendendo, neste mesmo ano,\npara os municípios vizinhos.</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\">\n<b>2014</b> – Já somos 43 profissionais e cerca de 6 mil clientes atendidos. \n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>2017</b>-\nInauguração da sua primeira filial na cidade de São Sebastião do\nCaí, lançamento de uma nova identidade: logomarca totalmente\nrepaginada que transmite foco no relacionamento com os clientes e\ninvestimento em ações de marketing.</p><p>\n\n\n\n\n\n\n\n</p><p class=\"western\" style=\"margin-bottom: 0.28cm; line-height: 108%\"><b>2018</b>\n-  Inauguração de uma nova sede na cidade de Feliz. Aniversário de\n20 anos de fundação.</p>', '', NULL, 4, 1),
(39, 'sobre-minha-internet', 'Para quê serve o AnyDesk? ', '<p><font color=\"#4a4a4a\" face=\"Roboto, Helvetica, Arial, sans-serif\"><span style=\"font-size: 18px;\">O AnyDesk é um software que serve para conectar dois computadores em tempo real. Baixe aqui o seu</span></font><span style=\"color: rgb(74, 74, 74); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 18px;\">&nbsp;</span><font color=\"#4a4a4a\" face=\"Roboto, Helvetica, Arial, sans-serif\"><span style=\"font-size: 18px;\"><a href=\"https://anydesk.pt/download\" target=\"_blank\">AnyDesk</a>!</span></font><br></p>', '', NULL, 1, 1),
(41, 'sobre-meu-contrato', 'Contrato de Telefonia Digital - Bommtempo Internet ', '<p><span style=\"color: rgb(0, 70, 71); font-family: &quot;Sansation Regular&quot;; font-size: 14px; background-color: rgb(246, 251, 244);\">Você pode fazer o download do&nbsp;contrato de de telefonia digital da Bommtempo Internet, no formato PDF.</span><br style=\"box-sizing: inherit; color: rgb(0, 70, 71); font-family: &quot;Sansation Regular&quot;; font-size: 14px; background-color: rgb(246, 251, 244);\"><span style=\"color: rgb(0, 70, 71); font-family: &quot;Sansation Regular&quot;; font-size: 14px; background-color: rgb(246, 251, 244);\">Se tiver dúvidas sobre algum item deste documento, entre em contato conosco.</span><br></p>', '', NULL, 4, 0),
(42, 'sobre-meu-contrato', 'Contratos referentes aos equipamentos em comodato, prestação de Serviços de Telecomunicações e Serviços de valor adicionado.', '<p>Caso tenha alguma dúvida, estamos a disposição.<br></p><p>Entre em contato pelo e-mail: atendimento@bomtempo.com.br, pelo fone (51) 3637 4500 ou ainda pelo whatApp (51) 9 9757 9912.&nbsp;</p>', '', NULL, 1, 1),
(43, 'sobre-meu-pagamento', 'teste', '<p>teste</p>\r\n', NULL, NULL, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `telefones`
--

CREATE TABLE `telefones` (
  `id` int(11) NOT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `imagemtelefone` varchar(600) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `telefones`
--

INSERT INTO `telefones` (`id`, `telefone`, `imagemtelefone`, `icon`, `link`, `ativo`, `ordem`) VALUES
(4, '(51) 99757-9912', NULL, 'fab fa-whatsapp', 'https://wa.me/5551997579912?text=Oi', 1, 1),
(5, '(51) 98298-0674', NULL, 'fas fa-mobile-alt', 'tel:++5551982980674', 1, 2),
(6, '(51) 3637-4500', NULL, 'fa fa-phone', 'tel:++555136374500', 1, 3),
(7, '(51) 99941-9728', NULL, 'fas fa-mobile-alt', 'tel:++5551999419728', 1, 4),
(8, '(51) 99346-1002', NULL, 'fas fa-mobile-alt', 'tel:++5551993461002', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(1) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `permissoes` varchar(255) NOT NULL,
  `ultimo_acesso` datetime DEFAULT NULL,
  `ativo` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `email`, `permissoes`, `ultimo_acesso`, `ativo`) VALUES
(1, 'Admin', 'admin', 'f0d5299cdda6b67fa0e98a4000c823b0', 'mw10@mw10.com.br', '1,2,3,4,5,6,8,9,10,', '2019-08-22 14:27:20', 1),
(2, 'bommtempo', 'bomtempo', 'c62d929e7b7e7b6165923a5dfc60cb56', 'bommtempo@bomtempo.com.br', '1,2,3,4,5,6,8,9,10,11,', '2019-07-21 10:50:32', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acessos`
--
ALTER TABLE `acessos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `botoes`
--
ALTER TABLE `botoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cidades`
--
ALTER TABLE `cidades`
  ADD PRIMARY KEY (`id_cidade`) USING BTREE;

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internet_tecnologia`
--
ALTER TABLE `internet_tecnologia`
  ADD PRIMARY KEY (`id_internet_tecnologia`) USING BTREE;

--
-- Indexes for table `mensagens_site`
--
ALTER TABLE `mensagens_site`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motivos`
--
ALTER TABLE `motivos`
  ADD PRIMARY KEY (`id_motivo`) USING BTREE;

--
-- Indexes for table `painel_administrativo`
--
ALTER TABLE `painel_administrativo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `planos_hospedagem`
--
ALTER TABLE `planos_hospedagem`
  ADD PRIMARY KEY (`id_plano_hospedagem`) USING BTREE;

--
-- Indexes for table `planos_internet_empresa`
--
ALTER TABLE `planos_internet_empresa`
  ADD PRIMARY KEY (`id_plano_empresa`) USING BTREE;

--
-- Indexes for table `planos_internet_voce`
--
ALTER TABLE `planos_internet_voce`
  ADD PRIMARY KEY (`id_plano_voce`) USING BTREE;

--
-- Indexes for table `planos_servicos`
--
ALTER TABLE `planos_servicos`
  ADD PRIMARY KEY (`id_plano_servico`);

--
-- Indexes for table `planos_sistema_seguranca`
--
ALTER TABLE `planos_sistema_seguranca`
  ADD PRIMARY KEY (`id_plano_sistema_seguranca`) USING BTREE;

--
-- Indexes for table `sobre`
--
ALTER TABLE `sobre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suporte`
--
ALTER TABLE `suporte`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telefones`
--
ALTER TABLE `telefones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acessos`
--
ALTER TABLE `acessos`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91790;

--
-- AUTO_INCREMENT for table `botoes`
--
ALTER TABLE `botoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cidades`
--
ALTER TABLE `cidades`
  MODIFY `id_cidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `internet_tecnologia`
--
ALTER TABLE `internet_tecnologia`
  MODIFY `id_internet_tecnologia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mensagens_site`
--
ALTER TABLE `mensagens_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `motivos`
--
ALTER TABLE `motivos`
  MODIFY `id_motivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `painel_administrativo`
--
ALTER TABLE `painel_administrativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `planos_hospedagem`
--
ALTER TABLE `planos_hospedagem`
  MODIFY `id_plano_hospedagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `planos_internet_empresa`
--
ALTER TABLE `planos_internet_empresa`
  MODIFY `id_plano_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `planos_internet_voce`
--
ALTER TABLE `planos_internet_voce`
  MODIFY `id_plano_voce` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `planos_servicos`
--
ALTER TABLE `planos_servicos`
  MODIFY `id_plano_servico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `planos_sistema_seguranca`
--
ALTER TABLE `planos_sistema_seguranca`
  MODIFY `id_plano_sistema_seguranca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sobre`
--
ALTER TABLE `sobre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suporte`
--
ALTER TABLE `suporte`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `telefones`
--
ALTER TABLE `telefones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
