<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$motivos = Connection::getInstance()->query("SELECT * FROM motivos WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
$cidades = Connection::getInstance()->query("SELECT * FROM cidades WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);

?>


<section id="contato" class="container contact">
	<div class="content">
		<div class="container-title">
<h1 class="title">Contato</h1>
		</div>
		<div class="content-form">
			<form class="form" method="post" class="js-form-validate" id="form-contato" enctype="multipart/form-data">
				<input type="hidden" name="ci_csrf_token" value="fb42085c07621ca8852366721e35868e">
				<input type="hidden" name="category" value="" />
				<input type="hidden" name="subcategory" value="" />

				<div class="form-title">
					<h2 class="title">Fale Conosco</h2>

					<div class="help-contact" style="display: none;">
						Você selecionou a opção <b></b>.
					</div>
					<div class="title-contact">
						Preencha o formulário abaixo e entraremos em contato com você.
					</div>

					<div class="content-input ct-mid">
						<label class="label-col">
							<input type="text" placeholder="Nome" id="nome-contato" name="name" class="input" required="required" value="">
						</label>
					</div>
					<div class="content-input ct-mid">
						<label class="label-col">
							<input type="email" placeholder="Email" id="email-contato" name="email" class="input" value="">
						</label>
					</div>
					
					<div class="content-input ct-mid">
						<label class="label-col">
							<input type="text" placeholder="Telefone" name="phone" class="input" required="required" value="" data-mask-type="phone">
						</label>

						<label class="label-col">
							<select name="type_contact" required="required" class="input select" data-parsley-errors-container="#parsley-contact-select">
								<option value="" selected>Método de contato preferido</option>
								<option value="1">Ligação</option>
								<option value="2">E-mail</option>
								<option value="3">WhatsApp</option>
							</select>
						</label>

						<label class="label-col">
							<select name="subject" required="required" id="assunto-contato"  class="input select" data-parsley-errors-container="#parsley-contact-select">
                                <option value="" selected>Motivo</option>
                                <?php foreach($motivos as $motivo){ ?>
                                    
								    <option value="<?=$motivo['id_motivo']?>"><?=$motivo['motivo']?></option>
                                <?php } ?>
                            
                                    
							</select>
						</label>
					</div>
					<div class="content-input ct-mid">
						<label class="label-col">
							<textarea name="message" placeholder="Mensagem" id="mensagem-contato" class="input textarea" value=""></textarea>
						</label>
					</div>
					<div class="content-input ct-mid">
						<label class="label-col">
							<select name="city" required="required" class="input select" data-parsley-errors-container="#parsley-contact-select">
								<option value="" selected>Cidade</option>
																	
                                    <?php foreach($cidades as $cidade){ ?>
                                        
                                        <option value="<?=$cidade['id_cidade']?>"><?=$cidade['titulo_principal']?></option>
                                    <?php } ?>
																		
									<option value="99999">Outra Cidade</option>
									
							</select>
						</label>
					</div>
					<div class="content-input js-upload" style="display: none;">
						<label class="label-col">
							<div class="upload">
								<div class="upload__inputFileOverlay">Anexe seu currículo</div>
								<input class="upload__input" type="file" name="attachment"/>
							</div>
						</label>
					</div>
					<div class="content-input">
						<label class="label-col">
							<input type="submit" value="Enviar" class="submit">
						</label>
					</div>
				</form>
			</div>
		</div>
		<div class="container container-bg">
			<figure class="bg_svg">
				<svg viewBox="0 0 67.6 67.8"><path d="M33.8 67.8c7.2 0 12.9-5.8 12.9-12.9 0-7.1-5.8-12.9-12.9-12.9-7.1 0-12.9 5.8-12.9 12.9-.1 7 5.7 12.8 12.9 12.9zm-.1 0h-.1.1zm.1-41.9c9.8 0 18.1 6.8 20.3 15.9h12.6c.6-2.6.9-5.2.9-8C67.6 15.1 52.5 0 33.8 0S0 15.1 0 33.8c0 2.7.3 5.4.9 8h12.6c2.2-9.1 10.4-15.9 20.3-15.9"/></svg>
			</figure>
		</div>
    </section>
    

<!--
    <script type="text/javascript">
    $('.telefone').mask('(00) 00000-0000');

    var form_contato = document.getElementById('form-contato');
        var pode_enviar_contato = true;
        form_contato.addEventListener("submit", function (ev) {
            ev.preventDefault();
            if (!pode_enviar_contato) {
                return;
            }
            pode_enviar_contato = false;
            var sending_contato = document.getElementById('sending-contato');
            sending_contato.innerText = "Enviando mensagem, por favor aguarde."
            var nome = document.getElementById("nome-contato").value;
            var email = document.getElementById("email-contato").value;
            var assunto = document.getElementById("assunto-contato").value;
            var telefone = document.getElementById("telefone-contato").value;

            var mensagem = document.getElementById("mensagem-contato").value;
            var data = 'nome=' + nome + 
            '&email=' + email + 
            '&telefone=' + telefone + 
            '&assunto=' + assunto +
            '&mensagem=' + mensagem; 
            var request = new XMLHttpRequest();
            request.open('POST', './contato/contato.php', true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            request.send(data);
            request.onreadystatechange = function () {
                if (request.readyState == XMLHttpRequest.DONE) {
                    sending_contato.innerText = "";
                    pode_enviar_contato = true;
                    try {
                        var response = JSON.parse(request.responseText);
                        if (response.result !== true && response.result !== 'true') {
                            if (response.result == 'MESSAGE_NOT_VALID') {
                                alert("Por favor nos escreva uma mensagem antes de enviar.")
                            } else if (response.result == 'COULD_NOT_SEND') {
                                alert(
                                    "Desculpe mas houve um erro ao enviar a mensagem, por favor tente novamente"
                                );
                            } else {
                                alert(
                                    "Desculpe mas houve um erro ao enviar a mensagem, por favor tente novamente"
                                );
                            }
                        } else {
                            alert(
                                "Sua mensagem foi enviada com sucesso, em breve entraremos em contato com você"
                            );
                            form_contato.reset();
                        }
                    } catch (e) {
                        alert("Desculpe mas houve um erro ao enviar a mensagem, por favor tente novamente")
                    }
                }
            }
        });

	</script> -->