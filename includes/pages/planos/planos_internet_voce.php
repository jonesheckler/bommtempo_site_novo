<?php
    $itens = Connection::getInstance()->query("SELECT * FROM planos_internet_voce WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
?>

    <div class="c-plans__inner fibra-optica">
    <div class="c-plans__title">
        <h2>Fibra Óptica</h2>
    </div>

        <ul class="c-plans__list">

        <?php foreach ($itens as $item){ ?>

            <li class="c-plans__item novosplanos">
                    <div class="c-plans__border">
                        <div class="flex __h-center __v-center type-plane novosplanosint">
                            <h3>
                                <span><?=$item['titulo_principal']?></span><?=$item['titulo_secundario']?>								
                                </h3>
                        </div>
                        <div class="flex __h-center __v-center description novosplanosint">
                            <p><?=$item['descricao']?> </p>
                        </div>
                        <div class="flex __h-center __v-center price"><h4>
                            R$ <?=number_format($item['valor'], 0)?>													
                            </h4>
                    </div>
                </div>
                <div class="plan-sign">
                    <a onclick="abreModal()" title="Assinar" >
                        <svg class="plan-sign__svg"><path d="M33.998 54C19.087 54 7 42.135 7 27.5S19.087 1 33.998 1M4.645 40.806C2.327 36.896 1 32.35 1 27.5c0-4.381 1.083-8.513 3-12.154"/></svg>
                        <?=$item['label_botao']?>							<svg class="plan-sign__svg"><path d="M1.002 1C15.913 1 28 12.865 28 27.5S15.913 54 1.002 54m29.353-39.806C32.673 18.105 34 22.65 34 27.5c0 4.381-1.083 8.513-3 12.153"/></svg>
                    </a>
                </div>
            </li>

        <?php } ?>
    </ul>
    </div>
</div>