<svg class="" width="123" height="108">
	<defs>
		<filter id="a" width="110" height="109" x="6" y="0" filterUnits="userSpaceOnUse">
			<feOffset in="SourceAlpha" result="offset"/>
			<feGaussianBlur result="blur" stdDeviation="3.873"/>
			<feFlood flood-color="#004647" flood-opacity=".5" result="flood"/>
			<feComposite in2="blur" operator="in" result="composite"/>
			<feBlend in="SourceGraphic" result="blend"/>
		</filter>
	</defs>
	<path d="M1.862 44.715a59.387 59.387 0 0 0 .11 20.785" fill="none" stroke="#004647" stroke-width="1px" fill-rule="evenodd"/>
	<path d="M122.108 45.182A57.623 57.623 0 0 1 122 65.811M95.212 22.075a47.5 47.5 0 0 1 .544 64.257M27.007 22.013a46.56 46.56 0 0 0-.553 63.976m83.99-55.106a55.154 55.154 0 0 1 1.529 44.817m-98.82-44.692a59.578 59.578 0 0 0-1.418 45" fill="none" stroke="#004647" stroke-width="1px" fill-rule="evenodd" data-name="linha"/>
	<circle cx="60.703" cy="54.016" r="40.016" fill="#004647" filter="url(#a)"/>
</svg>