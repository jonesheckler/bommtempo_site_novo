<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$noticias = Connection::getInstance()->query("SELECT * FROM noticias WHERE ativo = 1 ORDER BY data desc")->fetchAll();

?>


<section class="container blog">
	<div class="content">
		<div class="container-title">
			<h1 class="title-new">Blog</h1>
		</div>
		<?php foreach ($noticias as $n) { 
			$data = explode('-', $n["data"]);?>
			<div class="blog__item">
				<div class="col img"> <img src="<?=$n["imagem"]?>"></div>
				<div class="col">
					<span><?=$data[2]?> de <?=mes_extenso($data[1])?> de <?=$data[0]?></span>
					<h2><?=$n["titulo"]?></h2>
					<p><?=$n["resumo"]?></p>
					<a href='./noticia/<?=$n["slug"]?>'>Saiba mais</a>
					
				</div>
			</div>
		<?php } ?>
	</div>
</section>
    