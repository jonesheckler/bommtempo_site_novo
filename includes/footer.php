<!--
<ul class="navigation-top chat">
	<li>
		<a href="javascript:openChat()" title="Precisa de ajuda?" class="navigation-top__tx help">
			Fale conosco!
		</a>
	</li>
</ul>
-->
 
<div class="modal-contato " id="modal" >
	    <div class="container-title"><h2 class="title"> Assine </h2></div>
	    <div class="modal-body">
			<div class="texto">
				Entre em contato via WhatsApp <br>
				<a class="whats" href="https://wa.me/5551997579912" target="_blank"><i class="fab fa-whatsapp"></i> 99757-9912</a>
			</div>
			<hr>
			<form id="form-modal">
				<input type="hidden" name="subject" value="1" />
				<div class="content">
					<div class="texto">
						Ou digite seu nome e número de telefone que entraremos em contato da forma que preferir. 
					</div>
					<div class="col">
						<input type="text" name="name" placeholder="Nome completo" required>
					</div>
					<div class="col2">
						<input type="text" name="phone" data-mask-type="phone" placeholder="Telefone" required>
					</div>
					<div class="col2">
						<input type="text" name="email" placeholder="Email" required>
					</div>
					<div class="texto">
						Quero que o contato seja feito via <br><br>
						<input type="radio" name="type_contact" id="cb_whatsapp" value="3" required> <label for="cb_whatsapp">WhatsApp</label>
						<input type="radio" name="type_contact" id="cb_ligacao" value="1" required> <label for="cb_ligacao">Ligação</label>
						<input type="radio" name="type_contact" id="type_contact" value="4" required> <label for="cb_indiferente">Indiferente</label>
					</div>
					<div class="rodape">
						<button type="submit" >Enviar</button>
					</div>
				</div>
			</form>
		</div>
		<a class="modal-close" data-target="modal"><i class="fa fa-times"></i></a>
	  </div>
	  <div class="modal-close" data-target="modal"></div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<a href="https://wa.me/5551997579912?text=Dúvida" style="    position:fixed;width:60px;height:60px;bottom:40px;right:40px;background-color:#25d366;color:#FFF;border-radius:50px;text-align:center;font-size:30px;box-shadow: 1px 1px 2px #888;
    z-index:1000;" target="_blank">
    <i style="margin-top:16px" class="fa fa-whatsapp"></i>
    </a>
<!--
<script type="text/javascript" src="https://bom-tempo.mktzap.com.br/webchat/qxxkfhno2lyrqbpmpn2q"></script>
-->
<script type="text/javascript">
    function openChat (){
        var dados = {
            "name": "Nome do usuário",
            "email": "user@email.com",
            "custom": [
                {
                    "name": "Telefone",
                    "data": ""
                },
                {
                    "name": "Cidade",
                    "data": ""
                }
            ]
        };
        sendWidgetData(dados);
    }

    function abreModal() {
    	$("#modal").toggleClass("open");
    }

   
       
</script>


<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$dadoscontatos = Connection::getInstance()->query("SELECT * FROM contato WHERE id = 1")->fetch(PDO::FETCH_ASSOC);
$telefones = Connection::getInstance()->query("SELECT * FROM telefones WHERE ativo = 1 ORDER BY ordem")->fetchAll(PDO::FETCH_ASSOC);
//var_dump($dadoscontatos);
?>



		<footer class="footer">
			<div class="content">
				<figure class="footer__logo">
					<svg viewBox="0 0 259.2 177.1">
						<path fill="#52af32" d="M143.5 177.1c7.2 0 12.9-5.8 12.9-12.9 0-7.1-5.8-12.9-12.9-12.9-7.1 0-12.9 5.8-12.9 12.9-.1 7 5.7 12.8 12.9 12.9zm-.1 0zm0 0zm0 0zm0 0zm0 0zm0 0h-.1.1zM143.3 0c48.9 0 88.5 39.6 88.5 88.5 15.6 3 27.4 16.7 27.4 33.2 0 4.8-1 9.4-2.8 13.5h-59.9c1-4.2 1.5-8.5 1.5-12.9 0-30.2-24.5-54.8-54.8-54.8-30.2 0-54.8 24.5-54.8 54.8 0 4.5.5 8.8 1.5 12.9H26.2C10.5 125.6 0 108.3 0 88.5c0-30.2 24.5-54.8 54.8-54.8 5.9 0 11.6.9 16.9 2.7C87.8 14.4 113.9 0 143.3 0m.2 135.2c9.8 0 18.1 6.8 20.3 15.9h12.6c.6-2.6.9-5.2.9-8 0-18.7-15.1-33.8-33.8-33.8s-33.8 15.1-33.8 33.8c0 2.7.3 5.4.9 8h12.6c2.2-9.1 10.4-15.9 20.3-15.9"/>
					</svg>
				</figure>
				<div class="footer__address">
					<ul class="address">
						<li>
							<p>Atendimento comercial</p>
							<p><a href="<?=$dadoscontatos['mapa']?>" target="_blank">
							<?=$dadoscontatos['rua']?>, <?=$dadoscontatos['numero']?>, <?=$dadoscontatos['bairro']?> - <?=$dadoscontatos['cidade']?> | <?=$dadoscontatos['uf']?></a></p>
							
							<p style="white-space:pre-wrap;"><?=$dadoscontatos['horario']?></p>	
							
						</li>
						<li>
							<p>Atendimento comercial</p>
							<p><a href="<?=$dadoscontatos['mapa2']?>" target="_blank">
							<?=$dadoscontatos['rua2']?>, <?=$dadoscontatos['numero2']?>, <?=$dadoscontatos['bairro2']?> - <?=$dadoscontatos['cidade2']?> | <?=$dadoscontatos['uf2']?></a></p>					
					
							<p style="white-space:pre-wrap;"><?=$dadoscontatos['horario2']?></p>					
						</li>
					</ul>
				</div>				<div class="footer__phone">
					<ul class="phones">
						<?php foreach($telefones as $telefone){ ?>

							<li class="phones__li">
								<a class="phones__a" href="<?=$telefone['link']?>" title="Telefone"><i class="<?=$telefone['icon']?>" style="font-size: 20px;color: #51af31; margin-right: 5px;"></i> <?=$telefone['telefone']?></a>
							</li>
						<?php } ?>	
					
					</ul>
				</div>
				
				<!--<div id="logo-wt-container"> </div> -->
				<div class="redessociais">
					<li class="redessociais_linha">
						<a href="<?=$dadoscontatos['instagram']?>" title="Instagram Bomm Tempo" target="_blank">
						<i class="fab fa-instagram" style="color: #fff;"></i>
						</a>
					</li>
					<li class="redessociais_linha">
						<a href="<?=$dadoscontatos['facebook']?>" title="Facebook Bomm Tempo" target="_blank">
						<i class="fab fa-facebook-f" style="color: #fff;"></i>
						</a>
					</li>
				</div>

			</div>
		</footer>


		<script src="js/global.min.js" type="text/javascript" ></script>
    
</body>

</html>