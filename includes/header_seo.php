<?php
$titulo = null;
$imagem = "img/avatar.png";
$descricao = $config["descricao"];

error_reporting(E_ALL);
ini_set('display_errors', 1);
$_GET['param1'] = isset($_GET['param1']) ? $_GET['param1'] : null;
switch ($_GET['param1']) {
    case 'noticias':
        $titulo = "Notícias";
        break;
    case 'aluga':
        $titulo = "Aluga";
        $_SESSION['busca']['tipo-cadastro'] = '1';
        break;
    case 'aluga-venda':
        $titulo = "Aluga";
        $_SESSION['busca']['tipo-cadastro'] = '1';
        break;
    case 'venda':
        $titulo = "Venda";
        $_SESSION['busca']['tipo-cadastro'] = '2';
        break;
    case 'busca-venda':
        $titulo = "Venda";
        $_SESSION['busca']['tipo-cadastro'] = '2';
        break;
    case 'busca':
        $titulo = "Encontre seu imóvel";
        break;
    case 'anuncie-seu-imovel': 
        $titulo = "Anuncie Seu Imóvel";
        break;
    case 'classificados':
        $titulo = "Classificados";
        break;
    case 'favoritos':
            $titulo = "Favoritos";
        break;
    case 'sobre':
        $titulo = "Sobre a Ideal";
        $descricao = limita_caracteres(strip_tags(Connection::getInstance()->query("SELECT descricao FROM quem_somos LIMIT 1")->fetchColumn()), 300);
        break;
    case 'noticia': 
        $noticia = Connection::getInstance()->prepare("SELECT titulo, imagem, texto FROM noticias WHERE slug = ? AND ativo = 1 LIMIT 1");
        $noticia->execute(array($_GET['param2']));
        $noticia = $noticia->fetch(PDO::FETCH_ASSOC);
        $titulo = $noticia['titulo'];
        
        $imagem = file_exists($noticia['imagem']) ? $noticia['imagem'] : $imagem;

        $descricao = limita_caracteres(strip_tags($noticia['texto']), 250);

        break;
    case 'imoveis':
        $_GET['param2'] = isset($_GET['param2']) ? $_GET['param2'] : null;
        $_GET['param3'] = isset($_GET['param3']) ? $_GET['param3'] : null;
        if($_GET['param2'] === 'classificados'){
            $imovel = Connection::getInstance()->prepare("SELECT condominio, descricao, capa FROM classificados WHERE id_classificados = ? LIMIT 1");
            $imovel->execute(array($_GET['param3']));
            $imovel = $imovel->fetch(PDO::FETCH_ASSOC);
            if(!$imovel){
                echo '<script>location.href = "'.$base_url.'/404"</script>';
                die;
            }
            $titulo = $imovel['condominio'] ? $imovel['condominio'] . ' | Classificados' : null;
            
            if($titulo){
                $descricao = limita_caracteres(strip_tags($imovel['descricao']), 300);
                $imagem = $imovel['capa'];
            }
        }elseif($_GET['param2'] === 'venda'){
            $pesquisa = [
                "fields" =>  ["Codigo", "Categoria", "Bairro", "Cidade", "Vagas", "Caracteristicas", "Cidade","InfraEstrutura", "ValorVenda", "Dormitorios", "Suites", "Vagas", "AreaTotal", "AreaPrivativa", "FotoDestaque", "Descricao", "DescricaoEmpreendimento", "DescricaoWeb", "Latitude", "Longitude", ["Foto" => ["Foto","FotoPequena","Destaque"]]],
            ];
            
            $url =  $VISTASOFT_URL.'/imoveis/detalhes?key='.$VISTASOFT_KEY.'&imovel='.$_GET['param3'].'&pesquisa='.json_encode($pesquisa);
            
            $ch = curl_init($url);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER , array( 'Accept: application/json' ) );
            $imovel = curl_exec( $ch );
             
            $imovel = json_decode( $imovel, true );
            if(count($imovel) < 3){
                echo '<script>location.href = "'.$base_url.'/404"</script>';
                die;
            }
            $titulo = $imovel['Categoria'].', '.$imovel['Bairro'].' - Cód. '.$imovel['Codigo']. ' | ' . ' Venda';
            $imagem = $imovel['FotoDestaque'];
            $descricao = ' ';
        }else{
            $imovel = Connection::getInstance()->prepare("SELECT CODIGOIMOVEL, EDIFICIO, TIPOIMOVEL, TIPOVENDA, BAIRRO, DESCRICAOVENDA, DESCRICAOALUGUEL FROM IMB_IMOVEL WHERE PUBLICAR = '1' AND EXCLUIDO = '0' AND CODIGOIMOVEL = :codigo");
            $imovel->bindValue(":codigo", $_GET['param2']);
            $imovel->execute();
            $imovel = $imovel->fetch(PDO::FETCH_ASSOC);
            if(!$imovel){
                echo '<script>location.href = "'.$base_url.'/404"</script>';
                die;
            }
            $titulo = !empty($imovel['EDIFICIO']) ? $imovel['EDIFICIO'] . ', ' . $imovel['BAIRRO'] : null;
            $titulo = $titulo ? $titulo :  'Cód. '.$imovel['CODIGOIMOVEL'];
            if($titulo){
                if($imovel['TIPOVENDA'] == 'S'){
                    $descricao = limita_caracteres(strip_tags($imovel['DESCRICAOVENDA']), 300);
                }else{
                    $descricao = limita_caracteres(strip_tags($imovel['DESCRICAOALUGUEL']), 300);
                }
            }
        }
        break;
    default:
        # code...
        break;
}

$titulo = $titulo ? $titulo . ' - ' : '';
$titulo .= $config["titulo"];
?>

<title><?=$titulo?></title>
<meta name="description" content="<?=$descricao?>">
<meta name="keywords" content="<?=$config["palavras"]?>" />

<meta property="og:title" content="<?=$titulo?>" />
<meta property="og:site_name" content="<?=$config['titulo']?>" />
<meta property="og:url" content="<?=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" />
<meta property="og:image" content="<?=$imagem?>" />
<meta property="og:description" content="<?=$descricao?>">
<base href="<?=$base_url?>">