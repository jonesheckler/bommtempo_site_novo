<?php

require_once(__DIR__.'/phpmailer/PHPMailerAutoload.php');

define('EMAIL_HOST', 'smtp.gmail.com');
define('EMAIL_USERNAME', 'bomtempo.formulario@gmail.com');
define('EMAIL_PASSWORD', 'B0mmtemP0');
define('EMAIL_SMTP_PORT', 465);
define('EMAIL_SMTP_AUTH', TRUE);
define('EMAIL_SMTP_SECURE', 'ssl');
define('EMAIL_NAME', 'Bommtempo');

/*

define('EMAIL_HOST', 'mailserver.bomtempo.com.br');
define('EMAIL_USERNAME', 'monitoramento@bomtempo.com.br');
define('EMAIL_PASSWORD', 'B0mmtemp0$');
define('EMAIL_SMTP_PORT', 587);
define('EMAIL_SMTP_AUTH', false);
define('EMAIL_SMTP_SECURE', false);
define('EMAIL_NAME', 'Bommtempo');



/* pararealizar o envio fazer send_email('assunto', 'msg bonita', array(email@email.com))->send
essa função retorna o objeto do php mailer, entao se tem mais alguma alteração para fazer no php mailerl vc pode fazer dessa maneira
$mail = send_email(....);
$mail->CharSet = 'iso'; <-- exemplo
$mail->send();
*/
function send_email($assunto, $html_msg, $send_to, $reply_to = null, $file = null){
    if(!is_array($send_to)){
        if(!filter_var($send_to, FILTER_VALIDATE_EMAIL)){
            throw new Exception('Você deve informar 1 e-mail válido na função send_mail');
        }
        $send_to = array($send_to);
    }
    $mail = new PHPMailer;
            
    // $mail->SMTPDebug = 3;                               // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = EMAIL_HOST;  // Specify main and backup SMTP servers
    $mail->SMTPAuth = EMAIL_SMTP_AUTH;                               // Enable SMTP authentication
    if(EMAIL_SMTP_SECURE){
        $mail->SMTPSecure = EMAIL_SMTP_SECURE;
    }
    $mail->Username = EMAIL_USERNAME;                 // SMTP username
    $mail->Password = EMAIL_PASSWORD;                           // SMTP password
    $mail->Port = EMAIL_SMTP_PORT;                                    // TCP port to connect to
    $mail->CharSet = 'UTF-8';
    $mail->SMTPDebug = 1; 

 
/*
    if ($file != null){
         $mail->AddAttachment($file['tmp_name'],
                            $file['name']);
    }
  */  
    if($reply_to){
        $mail->AddReplyTo($reply_to);
    }
    $mail->setFrom(EMAIL_USERNAME, EMAIL_NAME);


    foreach($send_to as $cc){
        if(filter_var($cc, FILTER_VALIDATE_EMAIL)){
            $mail->addAddress($cc);
        }
    }
    
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $assunto;
    $mail->Body    = $html_msg;
    $mail->AltBody = strip_tags($html_msg);

    return $mail;
}