<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
// error_reporting(0);
require_once(__DIR__."/../manager/class/Connection.class.php");
require_once(__DIR__."/send_email.fnc.php");
header("Content-type: application/json;charset=utf-8");



//$EMAILS = Connection::getInstance()->query("SELECT contato, contato_cc FROM configuracoes LIMIT 1")->fetch();
//$email_principal = array($EMAILS['contato']);
//$email_cc = array_filter(explode(';', $EMAILS['contato_cc']));
// $send_to = array_merge($email_principal, $email_cc);
//$send_to = "jones.heckler@gmail.com";

//var_dump($_POST); exit();


$motivo = Connection::getInstance()->query("SELECT * FROM motivos where id_motivo =".$_POST['subject'])->fetch();
if (isset($_POST['city'])){
     $cidade = Connection::getInstance()->query("SELECT * FROM cidades where id_cidade =".$_POST['city'])->fetch();
 }
 else
 {
    $cidade = null;
 }

$motivo['contatos'] = preg_replace('/\s+/', '', $motivo['contatos']); // remover espaços em branco
$send_to = array_filter(explode(';', $motivo['contatos']));

// $send_to = 'bruno.zanonato@hotmail.com';
//var_dump($send_to); exit();

$SEND_EMAIL = true; // se vai enviar email usando servidor de emails

if(strtoupper($_SERVER["REQUEST_METHOD"]) === "POST"){



    $response = [];
        // if(empty($_POST["message"])){
        //     $response["result"] = "MESSAGE_NOT_VALID";
        // }
        // else{
            $columns = array("name", "email", "phone", "type_contact", "subject", "message", "city");
        //remoevs all unecessary data of $_POST
        foreach ($columns as $c) {
            if(!isset($_POST[$c])){
                $_POST[$c] = '';
            }
        }
        $filter_body = array_intersect_key($_POST, array_flip($columns));
        $filter_body = array_map("trim", $filter_body);
        $filter_body = array_map("strip_tags", $filter_body);
        $body = $filter_body;


        
    //var_dump($body); exit();

        switch ($_POST["type_contact"]) {
            case '1':
            $metodo = 'Ligação';
            break;

            case '2':
            $metodo = 'E-mail';
            break;

            case '3':
            $metodo = 'WhatsApp';
            break;

            case '4':
            $metodo = 'Indiferente';
            break;
            
            default:
            $metodo = '';
            break;
        }
        
        if($SEND_EMAIL){
            $html_msg = '    <html>
                            <head>
                                <title>Contato via site - Bomm Tempo</title>
                            </head>

                            <body>
                                <p>'.$body["name"].' acaba de nos enviar uma mensagem através do nosso site. Seguem os dados de contato.</p>

                                <p>
                                    <b>Nome: </b> '.$body["name"].'<br />';
            if ($cidade != null)
            {
                $html_msg .= '<b>Cidade: </b> '. $cidade["titulo_principal"].'<br />';
            }
                                    
             $html_msg .= '         <b>E-mail: </b> '.$body["email"].'<br />
                                    <b>Telefone: </b> '.$body["phone"].'<br />
                                    <b>Método: </b> '.$metodo.'<br />
                                    <b>Motivo: </b> '.$motivo['motivo'].'<br />
                                    <b>Mensagem: </b> <br />'.$body["message"].'<br />
                                    <hr>
                                    <em>Não responda a esse email, esse e-mail é automático.</em>
                                </p>
                            </body>
                        </html>';

            // var_dump($html_msg); exit();            
            
            $mail = send_email("Contato via site Bomm Tempo", $html_msg, $send_to, $body["email"], isset($_FILES["attachment"]) ? $_FILES["attachment"] : null);
            $response["send_email"] = true;
            if(!$mail->send()) {
                //var_dump($mail->ErrorInfo);
                 $response["error"] = $mail->ErrorInfo;
                $response["result"] = "COULD_NOT_SEND";
            } else {
                $insert = Connection::getInstance()->prepare("INSERT INTO mensagens_site (nome, email, telefone, motivo, cidade, metodo, mensagem) VALUES ( ?, ?, ?, ?, ?, ?, ?)");   
                if($insert->execute([$body['name'], $body["email"], $body["phone"], $motivo['motivo'], $cidade["titulo_principal"], $metodo, $body["message"]])){
                  
                    $response["result"] = true;
                }else{
                    $response["result"] = false;
                }   
            }
        }else{
            $response["send_email"] = false;

            $insert = Connection::getInstance()->prepare("INSERT INTO mensagens_site (nome, email, telefone, motivo, cidade, metodo, mensagem) VALUES ( ?, ?, ?, ?, ?, ?, ?)");   
            if($insert->execute([$body['name'], $body["email"], $body["phone"], $motivo['motivo'], $cidade["titulo_principal"], $metodo, $body["message"]])){
                $response["result"] = true;
            }else{
                $response["result"] = false;
            } 
        // }

    }
    echo json_encode($response);
}