<?php
//error_reporting(E_ALL);
// ini_set('display_errors', 1);

require_once('manager/class/Connection.class.php');
require_once('manager/class/functions.php');
contador();
error_reporting(0);

function pretty_number($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));
    
    // is this a number?
    if(!is_numeric($n)) return 0;
    
    // now filter it;
    if($n>=1000000000000){
        $valor = round(($n/1000000000000),1);
        $palavra = $valor > 1 ? 'trilhões' : 'trilhão';
        return $valor.' '.$palavra;
    }else if($n>=1000000000){
        $valor = round(($n/1000000000),1);
        $palavra = $valor > 1 ? 'bilhões' : 'bilhão';
        return $valor.' '.$palavra;
    }else if($n>=1000000){
        $valor = round(($n/1000000),1);
        $palavra = $valor > 1 ? 'milhões' : 'milhão';
        return $valor.' '.$palavra;
    }else if($n>=1000) return round(($n/1000),1).' mil';
    
    return number_format($n);
}

//filtragem de inofmracoes
if(!isset($_SESSION['orderby'])){
    $_SESSION['orderby'] = "DESC";
}
if(!isset($_SESSION['favoritos'])){
    $_SESSION['favoritos'] = array();
}

$config = Connection::getInstance()->query("SELECT * FROM configuracoes WHERE id = 1")->fetch();
$contato = Connection::getInstance()->query("SELECT * FROM contato WHERE id = 1")->fetch();


$_GET['param1'] = (isset($_GET['param1'])) ? $_GET['param1'] : "banner";
//$_GET['param2'] = (isset($_GET['param2'])) ? $_GET['param2'] : null;




include("includes/header.php");

$file = "includes/pages/".$_GET['param1'].".php";


if(file_exists($file)){
    include($file);
}else{
    include("includes/pages/404.php");
}

include("includes/footer.php"); 
?>